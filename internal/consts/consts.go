package consts

const (
	// DatabaseType specifies the type of the database being used (e.g., "postgres").
	DatabaseType = "postgres"
	// AppName represents the name of the application (e.g., "subscription").
	AppName = "subscription"
	// AcceptedVersions represents the accepted API versions for the application (e.g., "v1.0").
	AcceptedVersions = "v1.0"
	// ContextEndPoints represents context endpoints for the application
	ContextEndPoints = "context-endpoints"
)

const (
	// ContextAcceptedVersions is the HTTP header key for accepted API versions.
	ContextAcceptedVersions = "Accept-Version"
	// ContextSystemAcceptedVersions is the HTTP header key for system-accepted API versions.
	ContextSystemAcceptedVersions = "System-Accept-Versions"
	// ContextAcceptedVersionIndex is the HTTP header key for the index of the accepted API version.
	ContextAcceptedVersionIndex = "Accepted-Version-index"
)

// SuccessfullyCheckedout is a constant representing a success message for checking out a subscription plan.
const SuccessfullyCheckedout = "Successfully checked out subscription plan"

// SuccessfullyCancelled is a constant representing a success message for cancellation of  a subscription plan.
const SuccessfullyCancelled = "Successfully cancelled  subscription plan"

// SuccessfullyCheckedout is a constant representing a success message for checking out a subscription plan.
const SuccessfullyRenewed = "Successfully Renewed existing Subscription"

// Required implies the field is a must
const Required = "required"

// SubscriptionID of the plan
const SubscriptionID = "subscription_id"

// PaymentGatewayID of the member
const PaymentGatewayID = "payment_gateway_id"

// DummyPaymentGatewayURL  a dummy url to redirect to payment gateway
const DummyPaymentGatewayURL = "https://dummy-payment-gateway.com/pay"

const (
	// Page represents the page parameter for pagination.
	Page = "page"
	// Limit represents the limit parameter for pagination.
	Limit = "limit"
)

const (
	// DefaultLimit specifies the default limit for pagination.
	DefaultLimit = 10
	// DefaultPage specifies the default page for pagination.
	DefaultPage = 1
	// MaxAllowedLimit specifies the maximum allowed limit for pagination.
	MaxAllowedLimit = 25
)

// Context setting values
const (
	// ContextErrorResponses represents the context key for error responses.
	ContextErrorResponses = "context-error-response"
	// ContextLocallizationLanguage represents the context key for localization language.
	ContextLocallizationLanguage = "lan"
)

// headers
const (
	// HeaderLocallizationLanguage represents the header for localization language.
	HeaderLocallizationLanguage = "Accept-Language"
	SamplePartnerID             = "6f8a2d51-84df-4a1f-9d4b-e79a2c5a9c85"
)

// CacheErrorData represents the cache name for error data.
const CacheErrorData = "CACHE_ERROR_DATA"

// ExpiryTime represents the expiry time for cache data in seconds.
const ExpiryTime = 180

// KeyNames
const (
	// SubscriptionPlan represents a subscription plan key.
	SubscriptionPlan = "subscription_plan"
)

// The success and error response messages
const (
	EndpointErr = "Error occured while loading endpoints from service"
	ContextErr  = "Error occured while loading error from service"
)

// KeyNames
const (
	ValidationErr      = "validation_error"
	ForbiddenErr       = "forbidden"
	UnauthorisedErr    = "unauthorized"
	NotFound           = "not found"
	InternalServerErr  = "internal_server_error"
	Errors             = "errors"
	AllError           = "AllError"
	Registration       = "registration"
	ErrorCode          = "errorCode"
	Invalid            = "invalid"
	Success            = "Successfully switched products between subscriptions"
	SuccessfullyListed = "Subscription plans listed successfully"
	CheckStatus        = "check_status"
	NotSubscribedYet   = "not_subscribed"
	NotEnabled         = "notenabled"
)

// success response code
const (
	StatusOk = 200
)

// validation errors
const (
	MemberID              = "member_id"
	CurrentSubscriptionID = "current_subscription_id"
	NewSubscriptionID     = "new_subscription_id"
	ProductReferenceID    = "product_reference_id"
)

// consts used in utilities
const (
	LimitVal = 25
	InputOne = 10
	InputTwo = 64
)

// Cache Keys
const (
	CacheErrorKey     = "ERROR_CACHE_KEY_LABEL"
	CacheEndpointsKey = "endpoints"
)

// consts used in app
const (
	LogMaxAge    = 7
	LogMaxSize   = (1024 * 1024 * 10)
	LogMaxBackup = 5
)

const (
	// IntervalUnitDay represents the time interval unit in days.
	IntervalUnitDay = "DAY"
	// IntervalUnitWeek represents the time interval unit in weeks.
	IntervalUnitWeek = "WEEK"
	// IntervalUnitMonth represents the interval unit in months.
	IntervalUnitMonth = "MONTH"
	// IntervalUnitYear represents the interval unit in years.
	IntervalUnitYear = "YEAR"

	// PaymentOptionText represents the payment option text.
	PaymentOptionText = "SubscriptionText"

	// SubscriptionDurationID is the identifier for a subscription duration.
	SubscriptionDurationID = "duration"
	// SubscriptionDurationName represents the name of a subscription duration.
	SubscriptionDurationName = "name"
	// SubscriptionDurationIntervalCount represents the interval count of a subscription duration.
	SubscriptionDurationIntervalCount = "interval_count"
	// SubscriptionDurationIntervalUnit represents the interval unit of a subscription duration.
	SubscriptionDurationIntervalUnit = "interval_unit"
	// SubscriptionPaymentText represents the payment option text.
	SubscriptionPaymentText = "payment_option_text"
	// SubscriptionValue represents the value of a subscription.
	SubscriptionValue = "value"
	Inactive          = "inactive"
	Subscribed        = "alreadysubscribed"
	SubscribedOnce    = "oncesubscribed"
	CurrentStatus     = "current_status"

	// SubscriptionPlanName represents the name of a subscription plan.
	SubscriptionPlanName = "name"
	// SubscriptionPlanAmount represents the amount of a subscription plan.
	SubscriptionPlanAmount = "amount"
	// SubscriptionPlanCurrencyID represents the identifier for the currency in a subscription plan.
	SubscriptionPlanCurrencyID = "currency_id"
	// SubscriptionPlanCurrency represents the identifier for the currency in a subscription plan.
	SubscriptionPlanCurrency = "currency"
	// SubscriptionPlanSubscriptionDuration represents the subscription duration in a subscription plan.
	SubscriptionPlanSubscriptionDuration = "subscription_duration_id"
	// SubscriptionPlanArtistCount represents the artist count in a subscription plan.
	SubscriptionPlanArtistCount = "artist_count"
	// SubscriptionPlanTrackCount represents the track count in a subscription plan.
	SubscriptionPlanTrackCount = "tracks_count"
	// SubscriptionPlanProductCount represents the product count in a subscription plan.
	SubscriptionPlanProductCount = "product_count"
	// SubscriptionPlanMaxTracksProduct represents the maximum tracks per product in a subscription plan.
	SubscriptionPlanMaxTracksProduct = "max_tracks_per_product"
	// SubscriptionPlanMaxArtistsProduct represents the maximum artists per product in a subscription plan.
	SubscriptionPlanMaxArtistsProduct = "max_artist_per_product"
	// PartnerID represents the identifier for a partner in the system.
	PartnerID = "partner_id"
	// SubscriptionPlanProductTypes represents the product types in a subscription plan.
	SubscriptionPlanProductTypes = "product_type"
	// SubscriptionPlanTRackQuality represents the track quality in a subscription plan.
	SubscriptionPlanTRackQuality = "track_quality"
	// SubscriptionPlanRenewableDuration represents whether the subscription plan is renewable within certain duration.
	SubscriptionPlanRenewableDuration = "can_renewable_within"
)

const (
	// DaysInWeek represents the number of days in a week.
	DaysInWeek = 7
	// DaysInMonth represents the number of days in a month.
	DaysInMonth = 30
	// DaysInYear represents the number of days in a year.
	DaysInYear = 365
)

// KeyNames
const (
	// MemberIDErr represents a member ID error key.
	MemberIDErr = "member_id"
	// SubscriptionDuration represents a subscription duration key.
	SubscriptionDuration = "subscription_duration"
)

const (
	// ValidationErrorRequired represents a required validation error.
	ValidationErrorRequired = "required"
	// ValidationErrorMinValue represents a minimum value validation error.
	ValidationErrorMinValue = "min"
	// ValidationErrorInvalid represents an invalid validation error.
	ValidationErrorInvalid = "invalid"
	// ValidationErrorFormat represents a format validation error.
	ValidationErrorFormat = "format"
)
