package entities

import (
	"database/sql"
	"strconv"
	"time"

	"github.com/google/uuid"
)

// CheckoutSubscription represents the data structure for a subscription checkout request.
type CheckoutSubscription struct {
	SubscriptionID   string `json:"subscription_id"`    // SubscriptionID is the unique identifier for the subscription.
	PaymentGatewayID int    `json:"payment_gateway_id"` // PaymentGatewayID is the ID of the payment gateway to use for the subscription payment.
	CustomName       string `json:"custom_name"`        //CustomName is the alternative name for the subscribed plan
}

// SubscriptionRenewal represents the data structure for a subscription renewal request.
type SubscriptionRenewal struct {
	SubscriptionID   string `json:"subscription_id"`    // SubscriptionID is the unique identifier for the subscription.
	PaymentGatewayID int    `json:"payment_gateway_id"` // PaymentGatewayID is the ID of the payment gateway to use for the subscription payment.

}

// CancelSubscription structure
type CancelSubscription struct {
	SubscriptionID string `json:"subscription_id"` // SubscriptionID is the unique identifier for the subscription.
}

// SwitchSubscriptions represents the SwitchSubscriptions entity.
type SwitchSubscriptions struct {
	CurrentSubscriptionID string `json:"current_subscription_id"`
	NewSubscriptionID     string `json:"new_subscription_id"`
	ProductReferenceID    string `json:"product_reference_id"`
}

// ErrorResponse represents an error response.
type ErrorResponse struct {
	Message   string                 `json:"message"`
	ErrorCode interface{}            `json:"errorCode"`
	Errors    map[string]interface{} `json:"errors"`
}

// ListAllSubscriptions represents the ListAllSubscriptions entity.
type ListAllSubscriptions struct {
	Name                string              `json:"name"`
	SubscriptionDetails SubscriptionDetails `json:"subscription_details"`
	UsageDetails        UsageDetails        `json:"usage_details"`
}

// SubscriptionDetails represents subscription entity
type SubscriptionDetails struct {
	Status         string `json:"status"`
	ExpirationDate string `json:"expiration_date"`
}

// UsageDetails represents usage details entity
type UsageDetails struct {
	ArtistsAdded  ArtistsAdded  `json:"artists_added"`
	TracksAdded   TracksAdded   `json:"tracks_added"`
	ProductsAdded ProductsAdded `json:"products_added"`
}

// ArtistsAdded represents artists entity
type ArtistsAdded struct {
	Added int `json:"added"`
	Total int `json:"total"`
}

// TracksAdded represents tracks entity
type TracksAdded struct {
	Added int `json:"added"`
	Total int `json:"total"`
}

// ProductsAdded represents products entity
type ProductsAdded struct {
	Added int `json:"added"`
	Total int `json:"total"`
}

// MetaData represents metadata for an entity.
type MetaData struct {
	Total       int64 `json:"total"`
	PerPage     int32 `json:"per_page"`
	CurrentPage int32 `json:"current_page"`
	Next        int32 `json:"next"`
	Prev        int32 `json:"prev"`
}

// SuccessResponse represents success data for an entity.
type SuccessResponse struct {
	Code     int                    `json:"code"`
	Message  string                 `json:"message"`
	Metadata interface{}            `json:"metadata"`
	Data     []ListAllSubscriptions `json:"data"`
}

// Subscription represents the subscription entity.
type Subscription struct{}

// SubscriptionDuration represents the duration of a subscription.
type SubscriptionDuration struct {
	ID                int64   `json:"id,omitempty"`
	Name              string  `json:"name"`
	Value             int     `json:"value,omitempty"`
	PaymentOptionText string  `json:"payment_option_text,omitempty"`
	IntervalUnit      string  `json:"interval_unit"`
	IntervalCount     float64 `json:"interval_count"`
}

// ReqParams represents the request parameters.
type ReqParams struct {
	Page       int32  `form:"page"`
	Limit      int32  `form:"limit"`
	SearchTerm string `form:"search_term"`
}

// SubscriptionPlan represents a subscription plan.
type SubscriptionPlan struct {
	ID                       string                       `json:"id"`
	Name                     string                       `json:"name"`
	CurrencyISO              string                       `json:"currency_iso"`
	Currency                 CurrencyResponse             `json:"currency"`
	Amount                   float64                      `json:"amount"`
	IsActive                 bool                         `json:"is_active"`
	PartnerID                uuid.UUID                    `json:"partner_id"`
	CurrencyID               int64                        `json:"currency_id"`
	SubscriptionDurationID   int64                        `json:"subscription_duration_id"`
	ArtistCount              int                          `json:"artist_count"`
	TrackCount               int                          `json:"track_count"`
	MaxTracksPerProduct      int                          `json:"max_tracks_per_product"`
	MaxArtistsPerProduct     int                          `json:"max_artists_per_product"`
	IsFreeSubscription       bool                         `json:"is_free_subscription"`
	IsCancellationEnabled    bool                         `json:"is_cancellation_enabled"`
	CanRenewableWithin       int64                        `json:"can_renewable_within"`
	ProductCount             int16                        `json:"product_count"`
	PlanInfoText             string                       `json:"plan_info_text"`
	IsDefault                bool                         `json:"is_default"`
	IsOneTimeSubscription    bool                         `json:"is_one_time_subscription"`
	SubscriptionLimitPerYear int16                        `json:"subscription_limit_per_year"`
	TaxPercentage            float64                      `json:"tax_percentage"`
	SKU                      string                       `json:"sku"`
	Info                     string                       `json:"info"`
	Status                   bool                         `json:"status"`
	SubscriptionCount        int                          `json:"subscription_count"`
	Duration                 SubscriptionDurationResponse `json:"duration"`
	ProductTypeResponse      []ProductTypeResponse        `json:"product_type_response"`
	TrackQualityResponse     []TrackQualityResponse       `json:"track_quality_response"`
	DisplayColor             string                       `json:"display_color"`
	FirstExpiryWarning       int16                        `json:"first_expiry_warning"`
	SecondExpiryWarning      int16                        `json:"second_expiry_warning"`
	ThirdExpiryWarning       int16                        `json:"third_expiry_warning"`
	Position                 int                          `json:"position"`
	SubscriptionFor          int16                        `json:"subscription_for"`
	IsDeleted                bool                         `json:"is_deleted"`
	CreatedOn                time.Time                    `json:"created_on"`
	UpdatedBy                uuid.UUID                    `json:"updated_by"`
	UpdatedOn                time.Time                    `json:"updated_on"`
	DeletedBy                uuid.UUID                    `json:"deleted_by"`
	DeletedOn                time.Time                    `json:"deleted_on"`
	ProductTypes             []int64                      `json:"product_type"`
	TrackQuality             []int64                      `json:"track_quality"`
	SubscriptionPlanDetails  SubscriptionPlanDetails      `json:"details"`
}

// SubscriptionPlanDetails represents details of a subscription plan.
type SubscriptionPlanDetails struct {
	ArtistCount          int `json:"artist_count"`
	TrackCount           int `json:"tracks_count"`
	MaxTracksPerProduct  int `json:"max_tracks_per_product"`
	MaxArtistsPerProduct int `json:"max_artist_per_product"`
	ProductCount         int `json:"product_count"`
}

// SubscriptionPlanProductType represents a product type for a subscription plan.
type SubscriptionPlanProductType struct {
	ID             int64  `json:"id"`
	SubscriptionID string `json:"subscription_id"`
	ProductTypeID  int    `json:"product_type_id"`
}

// SubscriptionPlanTrackQuality represents track quality for a subscription plan.
type SubscriptionPlanTrackQuality struct {
	ID                 int64  `json:"id"`
	SubscriptionID     string `json:"subscription_id"`
	TrackFileQualityID int64  `json:"track_file_quality_id"`
}

// SubscriptionStore represents a store for a subscription.
type SubscriptionStore struct {
	ID             int64  `json:"id"`
	PartnerStoreID int64  `json:"partner_store_id"`
	SubscriptionID string `json:"subscription_id"`
}

// SubscriptionCountry represents a country for a subscription.
type SubscriptionCountry struct {
	ID             int64  `json:"id"`
	SubscriptionID string `json:"subscription_id"`
	CountryID      int64  `json:"country_id"`
}

// Entity represents a generic entity.
type Entity struct {
	TableName  string
	ColumnName string
	Value      string
}

// SubscriptionDurationResponse represents a response for a subscription duration.
type SubscriptionDurationResponse struct {
	ID            int64   `json:"id"`
	IntervalCount float64 `json:"interval_count"`
	IntervalUnit  string  `json:"interval_unit"`
}

// CurrencyResponse represents a response for a currency.
type CurrencyResponse struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
	Code string `json:"iso"`
}

// ProductTypeResponse represents a response for a product type.
type ProductTypeResponse struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

// TrackQualityResponse represents a response for track quality.
type TrackQualityResponse struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

// DetailsResponse represents a response with details.
type DetailsResponse struct {
	ArtistCount         int `json:"artist_count"`
	TracksCount         int `json:"tracks_count"`
	ProductCount        int `json:"product_count"`
	MaxTracksPerProduct int `json:"max_tracks_per_product"`
	MaxArtistPerProduct int `json:"max_artist_per_product"`
}

// SuccessResponse represents a success response.
type SuccessResp struct {
	Code    string      `json:"code"`
	Message string      `json:"message"`
	Meta    MetaData    `json:"meta_data"`
	Data    interface{} `json:"data"`
}

// Country represents a country entity.
type Country struct {
	ID                 int64     `json:"id"`
	Name               string    `json:"name"`
	ISO                string    `json:"iso"`
	ISOThreeCode       string    `json:"iso_three_code"`
	CurrencyID         *int      `json:"currency_id"`
	PostcodeRegex      string    `json:"postcode_regex"`
	LanguageIdentifier string    `json:"language_identifier"`
	UpdatedOn          time.Time `json:"updated_on"`
	UpdatedBy          uuid.UUID `json:"updated_by"`
}

// SubscriptionCountryResponse represents a response for a subscription country.
type SubscriptionCountryResponse struct {
	Code   string `json:"code"`
	Name   string `json:"name"`
	Status bool   `json:"status"`
}

// Store represents a store entity.
type Store struct {
	ID                       string `json:"id"`
	Name                     string `json:"name"`
	ParentID                 string `json:"parent_id"`
	RemotePath               string `json:"remote_path"`
	IsActive                 bool   `json:"is_active"`
	IsLive                   bool   `json:"is_live"`
	UseGenericPackage        bool   `json:"use_generic_package"`
	VariableName             string `json:"variable_name"`
	ReportingType            int16  `json:"reporting_type"`
	FeatureFMName            string `json:"featurefm_name"`
	IsFeatureFMEnabled       bool   `json:"is_featurefm_enabled"`
	ProcessDurationDays      int    `json:"process_duration_days"`
	DDEXVersionID            int    `json:"ddex_version_id"`
	CRSPermissionID          int    `json:"crs_permission_id"`
	IsDolbyEnabled           bool   `json:"is_dolby_enabled"`
	DolbyCodecID             string `json:"dolby_codec_id"`
	IsStereoRequiredForDolby bool   `json:"is_stereo_required_for_dolby"`
	IsSonyEnabled            bool   `json:"is_sony_enabled"`
	PreferredProviderProgram bool   `json:"preferred_provider_program"`
	IncludeFeaturedArtist    bool   `json:"include_featured_artist"`
	IsPriceCodeEnabled       bool   `json:"is_price_code_enabled"`
	IsPreSaveEnabled         bool   `json:"is_pre_save_enabled"`
	IsDefault                bool   `json:"is_default"`
	Logo                     string `json:"logo"`
	IsLocked                 bool   `json:"is_locked"`
}

// PartnerStore represents a partner store entity.
type PartnerStore struct {
	ID                       int64     `json:"id"`
	PartnerID                uuid.UUID `json:"partner_id"`
	StoreID                  uuid.UUID `json:"store_id"`
	IsStore                  bool      `json:"is_store"`
	Position                 int16     `json:"position"`
	CustomName               string    `json:"custom_name"`
	ProcessingDuration       int16     `json:"processing_duration"`
	ReviewProcessingDuration int16     `json:"review_processing_duration"`
	IsActive                 bool      `json:"is_active"`
	UpdatedBy                uuid.UUID `json:"updated_by"`
	UpdatedOn                time.Time `json:"updated_on"`
}

// SubscriptionStoreResponse represents a response for a subscription store.
type SubscriptionStoreResponse struct {
	ID                  string        `json:"id"`
	SubscriptionStoreID NullableInt64 `json:"subscription_store_id"`
	Name                string        `json:"name"`
	Logo                string        `json:"logo"`
	Status              bool          `json:"status"`
}

// SubscriptionStoresMetadataResponse represents metadata for subscription stores.
type SubscriptionStoresMetadataResponse struct {
	Code     string                      `json:"code"`
	Message  string                      `json:"message"`
	Metadata MetaData                    `json:"meta_data"`
	Data     []SubscriptionStoreResponse `json:"data"`
}

// NullableInt64 represents an optional int64 value that can be null.
type NullableInt64 struct {
	sql.NullInt64
}

// MarshalJSON customizes JSON marshaling for NullableInt64.
func (ni NullableInt64) MarshalJSON() ([]byte, error) {
	if !ni.Valid {
		return []byte("null"), nil
	}
	return []byte(strconv.FormatInt(ni.Int64, 10)), nil
}
