// nolint
package usecases_test

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"subscription/internal/consts"
	"subscription/internal/entities"
	"subscription/internal/repo/mock"
	"subscription/internal/usecases"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestHandleSubscriptionCheckout(t *testing.T) {
	// Initialize your logger client
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock for the SubscriptionRepoImply interface
	subRepoMock := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create an instance of the use case that you want to test
	subscriptionUC := usecases.NewSubscriptionUseCases(subRepoMock)
	// Convert context.Background() to *gin.Context for testing or specific use cases.
	ginCtx := createTestGinContext()
	memberID := uuid.New()
	checkoutData := entities.CheckoutSubscription{
		SubscriptionID:   "fefaac13-7391-4358-8dad-868849e54e35",
		PaymentGatewayID: 123,
	}

	// Scenario 1: SubscriptionID Is Missing
	t.Run("SubscriptionIDMissing", func(t *testing.T) {
		// Modify checkoutData to have an empty SubscriptionID
		test := entities.CheckoutSubscription{
			SubscriptionID:   "",
			PaymentGatewayID: 123,
		}

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionCheckout(ginCtx, memberID, test)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		expectedFieldsMap := map[string][]string{
			consts.SubscriptionID: {consts.Required},
		}
		if !reflect.DeepEqual(fieldsMap, expectedFieldsMap) {
			t.Errorf("Expected fieldsMap: %v for missing SubscriptionID, got: %v", expectedFieldsMap, fieldsMap)
		}
	})

	// Scenario 2: Subscription Is Free
	t.Run("SubscriptionIsFree", func(t *testing.T) {
		// Set the expectations for the mock function IsFreeSubscription
		expectedSubscriptionID := "fefaac13-7391-4358-8dad-868849e54e35"
		subRepoMock.EXPECT().IsFreeSubscription(ginCtx, expectedSubscriptionID).Return(true, nil)
		subRepoMock.EXPECT().HandleSubscriptionCheckout(ginCtx, memberID, checkoutData).Return(nil)

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionCheckout(ginCtx, memberID, checkoutData)

		// Validate that there are no errors and fieldsMap is empty
		require.Nil(t, err)
		if len(fieldsMap) != 0 {
			t.Errorf("Expected an empty fieldsMap for a free subscription, got: %v", fieldsMap)
		}
	})

	// Scenario 3: PaymentGatewayID Is Missing
	t.Run("PaymentGatewayIDMissing", func(t *testing.T) {
		// Reset the checkoutData
		checkoutData.SubscriptionID = "fefaac13-7391-4358-8dad-868849e54e35"
		checkoutData.PaymentGatewayID = 0

		// Set the expectations for the mock function IsFreeSubscription
		subRepoMock.EXPECT().IsFreeSubscription(ginCtx, checkoutData.SubscriptionID).Return(false, nil)

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionCheckout(ginCtx, memberID, checkoutData)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		expectedFieldsMap := map[string][]string{
			consts.PaymentGatewayID: {consts.Required},
		}
		if !reflect.DeepEqual(fieldsMap, expectedFieldsMap) {
			t.Errorf("Expected fieldsMap: %v, got fieldsMap: %v", expectedFieldsMap, fieldsMap)
		}
	})

	// Scenario 3: Valid Data
	t.Run("ValidData", func(t *testing.T) {
		renewalData := entities.SubscriptionRenewal{
			SubscriptionID:   "fefaac13-7391-4358-8dad-868849e54e35",
			PaymentGatewayID: 123,
		}

		// Configure the repository mock to return nil (no error)
		subRepoMock.EXPECT().HandleSubscriptionRenewal(ginCtx, memberID, renewalData).Return(nil)

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionRenewal(ginCtx, memberID, renewalData)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		require.Empty(t, fieldsMap)

	})
}

// Functio to create a gin instance for testing purpose
func createTestGinContext() *gin.Context {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)

	// You can set headers, query params, etc., on the gin context if needed.
	// For example:
	c.Request, _ = http.NewRequest(http.MethodGet, "/", nil)
	c.Request.Header.Set("headerKey", "headerValue")
	c.Request.URL.RawQuery = "key=value"

	return c
}
