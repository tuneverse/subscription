// nolint
package usecases_test

import (
	"reflect"
	"subscription/internal/consts"
	"subscription/internal/entities"
	"subscription/internal/repo/mock"
	"subscription/internal/usecases"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestHandleSubscriptionRenewal(t *testing.T) {
	// Initialize your logger client
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock for the SubscriptionRepoImply interface
	subRepoMock := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create an instance of the use case that you want to test
	subscriptionUC := usecases.NewSubscriptionUseCases(subRepoMock)

	// Convert context.Background() to *gin.Context for testing or specific use cases.
	ginCtx := createTestGinContext()
	memberID := uuid.New()

	renewalData := entities.SubscriptionRenewal{
		SubscriptionID:   "fefaac13-7391-4358-8dad-868849e54e35",
		PaymentGatewayID: 123,
	}

	// Scenario 1: SubscriptionID Is Missing
	t.Run("SubscriptionIDMissing", func(t *testing.T) {
		// Modify renewData to have an empty SubscriptionID
		renewalData := entities.SubscriptionRenewal{
			SubscriptionID:   "",
			PaymentGatewayID: 123,
		}

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionRenewal(ginCtx, memberID, renewalData)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		expectedFieldsMap := map[string][]string{
			consts.SubscriptionID: {consts.Required},
		}
		if !reflect.DeepEqual(fieldsMap, expectedFieldsMap) {
			t.Errorf("Expected fieldsMap: %v for missing SubscriptionID, got: %v", expectedFieldsMap, fieldsMap)
		}
	})

	// Scenario 2: PaymentGatewayID Is Missing
	t.Run("PaymentGatewayIDMissing", func(t *testing.T) {
		renewalData.SubscriptionID = "fefaac13-7391-4358-8dad-868849e54e35"
		renewalData.PaymentGatewayID = 0

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionRenewal(ginCtx, memberID, renewalData)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		expectedFieldsMap := map[string][]string{
			consts.PaymentGatewayID: {consts.Required},
		}
		if !reflect.DeepEqual(fieldsMap, expectedFieldsMap) {
			t.Errorf("Expected fieldsMap: %v, got fieldsMap: %v", expectedFieldsMap, fieldsMap)
		}
	})

	// Scenario 3: Valid Data
	t.Run("ValidData", func(t *testing.T) {
		renewalData := entities.SubscriptionRenewal{
			SubscriptionID:   "fefaac13-7391-4358-8dad-868849e54e35",
			PaymentGatewayID: 123,
		}

		// Configure the repository mock to return nil (no error)
		subRepoMock.EXPECT().HandleSubscriptionRenewal(ginCtx, memberID, renewalData).Return(nil)

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionRenewal(ginCtx, memberID, renewalData)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		require.Empty(t, fieldsMap)

	})
}
