// nolint
package usecases_test

import (
	"errors"
	"reflect"
	"subscription/internal/consts"
	"subscription/internal/entities"
	"subscription/internal/repo/mock"

	"subscription/internal/usecases"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

func TestHandleSubscriptionCancellation(t *testing.T) {
	// Initialize your logger client
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	logger.InitLogger(clientOpt)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// Create a mock for the SubscriptionRepoImply interface
	subRepoMock := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create an instance of the use case that you want to test
	subscriptionUC := usecases.NewSubscriptionUseCases(subRepoMock)

	// Convert context.Background() to *gin.Context for testing or specific use cases.
	ginCtx := createTestGinContext()
	memberID := uuid.New()

	// Scenario 1: SubscriptionID Is Missing
	t.Run("SubscriptionIDMissing", func(t *testing.T) {
		// Modify cancelData to have an empty SubscriptionID
		cancelData := entities.CancelSubscription{
			SubscriptionID: "",
		}

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionCancellation(ginCtx, memberID, cancelData)

		// Check for an error and validate fieldsMap
		require.Nil(t, err)
		expectedFieldsMap := map[string][]string{
			consts.SubscriptionID: {consts.Required},
		}
		if !reflect.DeepEqual(fieldsMap, expectedFieldsMap) {
			t.Errorf("Expected fieldsMap: %v for missing SubscriptionID, got: %v", expectedFieldsMap, fieldsMap)
		}
	})

	// Scenario 2: SubscriptionID Is Provided, Successful Cancellation
	t.Run("SuccessfulCancellation", func(t *testing.T) {
		// Modify cancelData to have a valid SubscriptionID
		cancelData := entities.CancelSubscription{
			SubscriptionID: "valid_subscription_id",
		}

		// Mock the repository to return nil error for successful cancellation
		subRepoMock.EXPECT().HandleSubscriptionCancellation(ginCtx, memberID, cancelData).Return(nil)

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionCancellation(ginCtx, memberID, cancelData)

		// Check for no error and empty fieldsMap
		require.Nil(t, err)
		require.Empty(t, fieldsMap)
	})

	// Scenario 3: SubscriptionID Is Provided, Cancellation Error
	t.Run("CancellationError", func(t *testing.T) {
		// Modify cancelData to have a valid SubscriptionID
		cancelData := entities.CancelSubscription{
			SubscriptionID: "29c908bb-27b9-4b38-9d40-29d0cab30e5c",
		}

		// Mock the repository to return an error for cancellation
		mockError := errors.New("cancellation error")
		subRepoMock.EXPECT().HandleSubscriptionCancellation(ginCtx, memberID, cancelData).Return(mockError)

		// Call the function you want to test
		fieldsMap, err := subscriptionUC.HandleSubscriptionCancellation(ginCtx, memberID, cancelData)

		// Check for an error and empty fieldsMap
		require.Error(t, err)
		require.Empty(t, fieldsMap)
	})
}
