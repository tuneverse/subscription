package usecases

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"subscription/internal/consts"
	"subscription/internal/entities"
	"subscription/internal/repo"
	"subscription/utilities"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

// SubscriptionUseCases is a struct that represents the use cases related to subscriptions.
type SubscriptionUseCases struct {
	repo repo.SubscriptionRepoImply
}

// SubscriptionUseCaseImply is an interface defining the methods for handling subscription-related operations.
type SubscriptionUseCaseImply interface {
	// HandleSubscriptionCheckout processes a new subscription checkout for a member.
	// It takes the member's ID and checkout data as input and returns any relevant
	// messages or errors in a map.
	HandleSubscriptionCheckout(ctx *gin.Context, memberID uuid.UUID, checkoutData entities.CheckoutSubscription) (map[string][]string, error)

	// HandleSubscriptionRenewal processes a subscription renewal for a member.
	// It takes the member's ID and renewal data as input and returns any relevant
	// messages or errors in a map.
	HandleSubscriptionRenewal(ctx *gin.Context, memberID uuid.UUID, checkoutData entities.SubscriptionRenewal) (map[string][]string, error)

	// HandleSubscriptionCancellation processes a request to cancel a subscription
	// for a member. It takes the member's ID and cancellation data as input and
	// returns any relevant messages or errors in a map.
	HandleSubscriptionCancellation(ctx *gin.Context, memberID uuid.UUID, checkoutData entities.CancelSubscription) (map[string][]string, error)
	SubscriptionProductSwitch(context.Context, uuid.UUID, entities.SwitchSubscriptions) (map[string][]string, error)
	IsMemberExists(context.Context, uuid.UUID) (bool, error)
	ViewAllSubscriptions(context.Context, uuid.UUID, entities.ReqParams) ([]entities.ListAllSubscriptions, models.MetaData, map[string][]string, error)
	GetSubscriptionRecordCount(context.Context, uuid.UUID) (int64, error)
	GetSubscription() []entities.Subscription
	CreateSubscriptionDuration(context.Context, entities.SubscriptionDuration) error
	ValidateSubscriptionDuration(context.Context, interface{}, string) (map[string][]string, error)
	GetAllSubscriptionDuration(context.Context, entities.ReqParams) ([]entities.SubscriptionDuration, error)
	GetTotalSubscriptionDurationCount(ctx context.Context) (int64, error)
	EditSubscriptionDuration(context.Context, string, map[string]interface{}) error
	DeleteSubscriptionDuration(context.Context, string) error
	ValidateSubscriptionPlan(context.Context, interface{}, string) (map[string][]string, error)
	CreateSubscriptionPlan(context.Context, entities.SubscriptionPlan) error
	EditSubscriptionPlanStore(ctx context.Context, partnerID string, planID string, stores []string) error
	EditSubscriptionPlanCountry(ctx context.Context, planID string, country []string) error
	GetAllSubscriptionPlans(ctx context.Context, reqData entities.ReqParams, searchFilter string, searchValue string, partnerID string) ([]entities.SubscriptionPlan, error)
	EditSubscriptionPlan(ctx context.Context, planIDStr string, data map[string]interface{}) error
	GetCurrencyByID(ctx context.Context, currencyID int64) (entities.CurrencyResponse, error)
	GetSubscriptionStores(ctx context.Context, planID string, page, perPage int) ([]entities.SubscriptionStoreResponse, int, error)
	GetSubscriptionCountries(ctx context.Context, planID string, page, perPage int) ([]entities.SubscriptionCountryResponse, int, error)
	GetSubscriptionDurationByID(ctx context.Context, durationID int64) (entities.SubscriptionDurationResponse, error)
	DeleteSubscriptionPlan(ctx context.Context, planID string, partnerID string) (string, error)
	GetSubscriptionPlanByID(ctx context.Context, planID string) (*entities.SubscriptionPlan, error)
	validateSubscriptionPlanForPatch(ctx context.Context, sd interface{}) (map[string][]string, error)
	validateSubscriptionPlanForPost(ctx context.Context, sd interface{}) (map[string][]string, error)
}

// NewSubscriptionUseCases creates a new SubscriptionUseCases instance.
func NewSubscriptionUseCases(subscriptionRepo repo.SubscriptionRepoImply) SubscriptionUseCaseImply {
	return &SubscriptionUseCases{
		repo: subscriptionRepo,
	}
}

// HandleSubscriptionCheckout handles the checkout of a subscription for a member.
// It takes the
//   - context
//   - memberID
//   - checkoutData
//     as parameters.
//
// It returns a map with subscription details and any encountered errors.
func (subscription *SubscriptionUseCases) HandleSubscriptionCheckout(ctx *gin.Context, memberID uuid.UUID, checkoutData entities.CheckoutSubscription) (map[string][]string, error) {
	fieldsMap := map[string][]string{}
	// Create a map to store the result
	resultMap := make(map[string][]string)
	subExists, isActive, err := subscription.repo.CheckSubscriptionExistenceAndStatus(ctx, checkoutData.SubscriptionID)
	if !subExists {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Invalid)
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: Invalid subscription plan")
		return fieldsMap, nil
	}
	if !isActive {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Inactive)
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: Inactive subscription plan")
		return fieldsMap, nil
	}
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: %s", err.Error())
	}
	// Check if SubscriptionID is empty
	if checkoutData.SubscriptionID == "" || len(checkoutData.SubscriptionID) == 0 {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: Missing required fields")
		return fieldsMap, nil
	}
	currentStatus, err := subscription.repo.GetSubscriptionStatusName(ctx, checkoutData.SubscriptionID)
	if err != nil {
		fmt.Println("Error:", err)
	}

	if currentStatus == "active" || currentStatus == "on_hold" || currentStatus == "processing" {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.CurrentStatus)
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: Check the current status of the plan")
		return fieldsMap, nil
	}
	// If the subscription is free, skip the PaymentGatewayID check
	isFree, err := subscription.repo.IsFreeSubscription(ctx, checkoutData.SubscriptionID)

	if err != nil {
		if err.Error() == "subscription does not exist" {
			logger.Log().WithContext(ctx).Errorf("Invalid Subscription Id : %s", err.Error())
			return nil, err
		} else {
			logger.Log().WithContext(ctx).Errorf("Failed to check if the subscription is free: %s", err.Error())
			return nil, err
		}
	}

	// Continue with the rest of the code, including PaymentGatewayID check, if it's not free
	if !isFree {
		if checkoutData.PaymentGatewayID == 0 {
			utils.AppendValuesToMap(fieldsMap, consts.PaymentGatewayID, consts.Required)
			logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: Missing required fields")
		}
		gatewayExists, _ := subscription.repo.CheckIfPayoutGatewayExists(ctx, checkoutData.PaymentGatewayID)
		if !gatewayExists {
			utils.AppendValuesToMap(fieldsMap, consts.PaymentGatewayID, consts.Invalid)
			logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan:Invalid gateway")
		}
		return fieldsMap, nil

	}
	subscribedFree, err := subscription.repo.CheckIfMemberSubscribedToFreePlan(ctx, memberID, checkoutData.SubscriptionID)
	if subscribedFree {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Subscribed)
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: subscribed this free plan already once")
		return fieldsMap, nil

	}
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan:error checking if member subscribed free plan")
	}
	subscribedOnetime, err := subscription.repo.HasSubscribedToOneTimePlan(ctx, memberID, checkoutData.SubscriptionID)
	if subscribedOnetime {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.SubscribedOnce)
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan: This is is one time plan")
		return fieldsMap, nil
	}
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to checkout this plan:Error checking This is is one time plan")
	}
	// Return the 'fieldsMap' with the processed data and a nil error, indicating success.
	if len(fieldsMap) > 0 {
		return fieldsMap, nil
	}

	// Handle the subscription checkout by calling the HandleSubscriptionCheckout method from the repository.
	err = subscription.repo.HandleSubscriptionCheckout(ctx, memberID, checkoutData)

	// Check if there was an error during the checkout process.
	if err != nil {
		// If an error occurred, log the error message and return the error.
		// Convert the error to a string and store it in the map
		resultMap["error"] = []string{err.Error()}
		logger.Log().WithContext(ctx).Errorf("Failed to checkout Subscription: %s", err.Error())
		return nil, err
	}

	return fieldsMap, nil
}

// HandleSubscriptionRenewal handles the checkout of a subscription for a member.
// It takes the
//   - context
//   - memberID
//   - checkoutData
//     as parameters.
//
// It returns a map with subscription details and any encountered errors.
func (subscription *SubscriptionUseCases) HandleSubscriptionRenewal(ctx *gin.Context, memberID uuid.UUID, checkoutData entities.SubscriptionRenewal) (map[string][]string, error) {
	fieldsMap := map[string][]string{}
	// Create a map to store the result
	resultMap := make(map[string][]string)
	// Check if SubscriptionID is empty
	subExists, isActive, err := subscription.repo.CheckSubscriptionExistenceAndStatus(ctx, checkoutData.SubscriptionID)
	if !subExists {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Invalid)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Invalid subscription plan")
		return fieldsMap, nil
	}
	if !isActive {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Inactive)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Inactive subscription plan")
		return fieldsMap, nil
	}
	yesSubscribed, err := subscription.repo.IsMemberSubscribedToPlan(ctx, memberID, checkoutData.SubscriptionID)
	if err != nil {
		fmt.Println("Error:", err)
	}
	if !yesSubscribed {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.NotSubscribedYet)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Not yet subscribed this plan")
		return fieldsMap, nil
	}
	if len(checkoutData.SubscriptionID) == 0 {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Missing required fields")
		return fieldsMap, nil
	}
	currentStatus, err := subscription.repo.GetSubscriptionStatusName(ctx, checkoutData.SubscriptionID)
	if err != nil {
		fmt.Println("Error:", err)
	}

	if currentStatus != "active" && currentStatus != "on_hold" {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.CheckStatus)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Check the current status of the plan")
		return fieldsMap, nil
	}
	subscribedFree, err := subscription.repo.CheckIfMemberSubscribedToFreePlan(ctx, memberID, checkoutData.SubscriptionID)
	if subscribedFree {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Subscribed)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: cannot renew free plan")
		return fieldsMap, nil

	}
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan:error checking if member subscribed free plan")
	}
	//Check if PaymentGatewayID is empty
	if checkoutData.PaymentGatewayID == 0 {
		utils.AppendValuesToMap(fieldsMap, consts.PaymentGatewayID, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Missing required fields")
	}
	gatewayExists, _ := subscription.repo.CheckIfPayoutGatewayExists(ctx, checkoutData.PaymentGatewayID)
	if !gatewayExists {
		utils.AppendValuesToMap(fieldsMap, consts.PaymentGatewayID, consts.Invalid)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan:Invalid gateway")
		return fieldsMap, nil
	}

	// Return the 'fieldsMap' with the processed data and a nil error, indicating success.
	if len(fieldsMap) > 0 {
		return fieldsMap, nil
	}

	// Handle the subscription checkout by calling the HandleSubscriptionCheckout method from the repository.
	err = subscription.repo.HandleSubscriptionRenewal(ctx, memberID, checkoutData)

	// Check if there was an error during the checkout process.
	if err != nil {
		// If an error occurred, log the error message and return the error.
		// Convert the error to a string and store it in the map
		resultMap["error"] = []string{err.Error()}
		logger.Log().WithContext(ctx).Errorf("Failed to renew Subscription: %s", err.Error())
		//ctx.JSON(http.StatusInternalServerError, resultMap)
		return nil, err
	}

	return fieldsMap, nil
}

// HandleSubscriptionCancellation handles the checkout of a subscription for a member.
// It takes the
//   - context
//   - memberID
//   - checkoutData
//     as parameters.
//
// It returns a map with subscription details and any encountered errors.
func (subscription *SubscriptionUseCases) HandleSubscriptionCancellation(ctx *gin.Context, memberID uuid.UUID, checkoutData entities.CancelSubscription) (map[string][]string, error) {
	fieldsMap := map[string][]string{}
	// Create a map to store the result
	resultMap := make(map[string][]string)
	// Check if SubscriptionID is empty
	subExists, isActive, err := subscription.repo.CheckSubscriptionExistenceAndStatus(ctx, checkoutData.SubscriptionID)
	if !subExists {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Invalid)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Invalid subscription plan")
		return fieldsMap, nil
	}
	if !isActive {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Inactive)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Inactive subscription plan")
		return fieldsMap, nil
	}
	yesSubscribed, err := subscription.repo.IsMemberSubscribedToPlan(ctx, memberID, checkoutData.SubscriptionID)
	if !yesSubscribed {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.NotSubscribedYet)
		logger.Log().WithContext(ctx).Errorf("Failed to renew this plan: Not yet subscribed this plan")
		return fieldsMap, nil
	}
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to check subscription status: %v", err)
		return fieldsMap, err
	}
	enabled, err := subscription.repo.CheckCancellationEnabled(ctx, checkoutData.SubscriptionID)
	if !enabled {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.NotEnabled)
		logger.Log().WithContext(ctx).Errorf("Failed to cancel this subscription: This is not cancellation enabled")
		return fieldsMap, nil
	}
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Failed to check subscription status: %v", err)
		return fieldsMap, err
	}
	currentStatus, err := subscription.repo.GetSubscriptionStatusName(ctx, checkoutData.SubscriptionID)

	if currentStatus != "active" && currentStatus != "on_hold" {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.CheckStatus)
		logger.Log().WithContext(ctx).Errorf("Failed to cancel this plan: Check the current status of the plan")
		return fieldsMap, nil
	}
	if err != nil {
		return nil, err

	}
	if checkoutData.SubscriptionID == "" {
		utils.AppendValuesToMap(fieldsMap, consts.SubscriptionID, consts.Required)
		logger.Log().WithContext(ctx).Errorf("Failed to cancel this subscription: Missing required fields")
		return fieldsMap, nil
	}

	// Return the 'fieldsMap' with the processed data and a nil error, indicating success.
	if len(fieldsMap) > 0 {
		return fieldsMap, nil
	}

	// Handle the subscription checkout by calling the HandleSubscriptionCheckout method from the repository.
	err = subscription.repo.HandleSubscriptionCancellation(ctx, memberID, checkoutData)

	// Check if there was an error during the checkout process.
	if err != nil {
		resultMap["error"] = []string{err.Error()}
		// If an error occurred, log the error message and return the error.
		logger.Log().WithContext(ctx).Errorf("Failed to cancel Subscription: %s", err.Error())
		// ctx.JSON(http.StatusInternalServerError, resultMap)
		return nil, err
	}

	return fieldsMap, nil
}

// SubscriptionProductSwitch function
func (subscription *SubscriptionUseCases) SubscriptionProductSwitch(ctx context.Context, memberID uuid.UUID, data entities.SwitchSubscriptions) (map[string][]string, error) {

	validationErrors := make(map[string][]string)

	if utilities.IsEmpty(data.CurrentSubscriptionID) {
		utils.AppendValuesToMap(validationErrors, consts.CurrentSubscriptionID, consts.Required)
	}

	if utilities.IsEmpty(data.NewSubscriptionID) {
		utils.AppendValuesToMap(validationErrors, consts.NewSubscriptionID, consts.Required)
	}

	if utilities.IsEmpty(data.ProductReferenceID) {
		utils.AppendValuesToMap(validationErrors, consts.ProductReferenceID, consts.Required)
	}

	if len(validationErrors) > 0 {
		return validationErrors, nil
	}

	if _, err := subscription.repo.SubscriptionProductSwitch(ctx, memberID, data); err != nil {
		logger.Log().WithContext(ctx).Errorf("Switch products between subscriptions failed err=%s", err.Error())
		return nil, err
	}
	return nil, nil
}

// IsMemberExists function
func (subscription *SubscriptionUseCases) IsMemberExists(ctx context.Context, memberID uuid.UUID) (bool, error) {

	_, err := subscription.repo.IsMemberExists(ctx, memberID)
	if err != nil {
		return false, err
	}

	return true, nil
}

// ViewAllSubscriptions function
func (subscription *SubscriptionUseCases) ViewAllSubscriptions(ctx context.Context, memberID uuid.UUID, reqParam entities.ReqParams) ([]entities.ListAllSubscriptions, models.MetaData, map[string][]string, error) {

	//call GetSubscriptionRecordCount function in repo
	recordCount, err := subscription.repo.GetSubscriptionRecordCount(ctx, memberID)

	if err != nil {
		return []entities.ListAllSubscriptions{}, models.MetaData{}, nil, err
	}

	if (reqParam.Limit*reqParam.Limit)-int32(recordCount) >= reqParam.Limit {

		validationErrors := make(map[string][]string)
		utils.AppendValuesToMap(validationErrors, consts.Page, consts.Invalid)
		return []entities.ListAllSubscriptions{}, models.MetaData{}, validationErrors, nil
	}

	//call Paginate function
	reqParam.Page, reqParam.Limit = utils.Paginate(reqParam.Page, reqParam.Limit, consts.DefaultLimit)

	//call ViewAllSubscriptions function in repo
	memberSubscriptions, err := subscription.repo.ViewAllSubscriptions(ctx, memberID, reqParam)
	if err != nil {
		return []entities.ListAllSubscriptions{}, models.MetaData{}, nil, err
	}

	metadata := &models.MetaData{
		CurrentPage: reqParam.Page,
		PerPage:     reqParam.Limit,
		Total:       recordCount,
	}

	metadata = utils.MetaDataInfo(metadata)

	return memberSubscriptions, *metadata, nil, nil
}

// GetSubscriptionRecordCount function
func (subscription *SubscriptionUseCases) GetSubscriptionRecordCount(ctx context.Context, memberID uuid.UUID) (int64, error) {

	//GetSubscriptionRecordCount function call to repo
	totalCount, err := subscription.repo.GetSubscriptionRecordCount(ctx, memberID)
	if err != nil {
		logger.Log().WithContext(ctx).Errorf("GetSubscriptionRecordCount failed, err=%s", err.Error())
		return 0, err
	}
	return totalCount, nil
}

// CreateSubscriptionPlan creates a subscription plan.
func (subscription *SubscriptionUseCases) CreateSubscriptionPlan(ctx context.Context, subscriptionPlan entities.SubscriptionPlan) error {
	log := logger.Log().WithContext(ctx)
	if err := subscription.repo.CreateSubscriptionPlanTransaction(ctx, subscriptionPlan); err != nil {
		log.Errorf("CreateSubscriptionPlan failed, CreateSubscriptionPlanTransaction failed, err=%s", err.Error())
		return err
	}
	return nil
}

// ValidateSubscriptionPlan validates a subscription plan.
func (subscription *SubscriptionUseCases) ValidateSubscriptionPlan(ctx context.Context, data interface{}, method string) (map[string][]string, error) {
	log := logger.Log().WithContext(ctx)
	validationErrors := make(map[string][]string)
	var err error
	switch strings.ToUpper(method) {
	case http.MethodPost:
		validationErrors, err = subscription.validateSubscriptionPlanForPost(ctx, data)
		if err != nil {
			log.Errorf("ValidateSubscriptionPlan failed, validateSubscriptionPlanForPost failed, err=%s", err.Error())
			return nil, err
		}
	case http.MethodPatch:
		validationErrors, err = subscription.validateSubscriptionPlanForPatch(ctx, data)
		if err != nil {
			log.Errorf("ValidateSubscriptionPlan failed, validateSubscriptionPlanForPatch failed, err=%s", err.Error())
			return nil, err
		}
	default:
		errMsg := fmt.Errorf("invalid request method")
		log.Errorf("ValidateSubscriptionPlan failed, err=%s", errMsg.Error())
		return nil, errMsg
	}

	return validationErrors, nil
}

func (subscription *SubscriptionUseCases) validateSubscriptionPlanForPost(ctx context.Context, sd interface{}) (map[string][]string, error) {
	validationErrors := make(map[string][]string)
	log := logger.Log().WithContext(ctx)
	switch d := sd.(type) {
	case entities.SubscriptionPlan:
		if !utilities.IsNotEmpty(d.Name) {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationName, consts.ValidationErrorRequired)
		}
		if d.Amount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanAmount, consts.ValidationErrorMinValue)
		}
		if len(d.CurrencyISO) != 3 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanCurrencyID, consts.ValidationErrorFormat)

		}
		if _, err := subscription.repo.GetSubscriptionDurationsByID(ctx, d.SubscriptionDurationID); err != nil {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationID, consts.ValidationErrorInvalid)
		}

		if _, err := subscription.repo.GetSubscriptionDurationsByID(ctx, d.CanRenewableWithin); err != nil {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanRenewableDuration, consts.ValidationErrorInvalid)
		}

		if len(d.PartnerID) < 1 {
			utils.AppendValuesToMap(validationErrors, consts.PartnerID, consts.ValidationErrorInvalid)
		} else {
			partnerVal := entities.Entity{
				TableName:  "partner",
				ColumnName: "id",
				Value:      d.PartnerID.String(),
			}
			if count, err := subscription.repo.GetByID(ctx, partnerVal); err != nil || count == 0 {
				utils.AppendValuesToMap(validationErrors, consts.PartnerID, consts.ValidationErrorInvalid)
			}
		}
		if d.SubscriptionPlanDetails.TrackCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanTrackCount, consts.ValidationErrorMinValue)
		}
		if d.SubscriptionPlanDetails.ArtistCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanArtistCount, consts.ValidationErrorMinValue)
		}
		if d.SubscriptionPlanDetails.ProductCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanProductCount, consts.ValidationErrorMinValue)
		}
		if d.SubscriptionPlanDetails.MaxArtistsPerProduct < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanMaxArtistsProduct, consts.ValidationErrorMinValue)
		}
		if d.SubscriptionPlanDetails.MaxTracksPerProduct < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanMaxTracksProduct, consts.ValidationErrorMinValue)
		}
		if len(d.ProductTypes) < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanProductTypes, consts.ValidationErrorMinValue)
		} else {
			if err := subscription.repo.VerifyPartnerProductType(ctx, d.PartnerID.String(), d.ProductTypes); err != nil {
				utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanProductTypes, consts.ValidationErrorInvalid)
			}
		}
		if len(d.TrackQuality) < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanTRackQuality, consts.ValidationErrorMinValue)
		} else {
			if err := subscription.repo.VerifyPartnerTrackQuality(ctx, d.PartnerID.String(), d.TrackQuality); err != nil {
				utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanTRackQuality, consts.ValidationErrorInvalid)
			}
		}
	default:
		errMsg := fmt.Errorf("invalid input data type, type=%v", d)
		log.Errorf("ValidateSubscriptionPlan failed, err=%s", errMsg.Error())
		return validationErrors, errMsg
	}

	return validationErrors, nil
}

func (subscription *SubscriptionUseCases) validateSubscriptionPlanForPatch(ctx context.Context, sd interface{}) (map[string][]string, error) {
	validationErrors := make(map[string][]string)

	switch d := sd.(type) {
	case entities.SubscriptionPlan:

		if d.Name != "" && len(strings.TrimSpace(d.Name)) == 0 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanName, "required")
		}

		// Check if the "Amount" field is provided and greater than or equal to 1
		if d.Amount != 0 && d.Amount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanAmount, "min")
		}

		// Check if the "CurrencyISO" field is provided and has a valid format
		if d.CurrencyISO != "" && len(d.CurrencyISO) != 3 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanCurrency, consts.ValidationErrorFormat)
		}

		// Check if the "SubscriptionDurationID" field is provided and valid
		if d.SubscriptionDurationID != 0 {
			_, err := subscription.repo.GetSubscriptionDurationsByID(ctx, d.SubscriptionDurationID)
			if err != nil {
				utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationID, consts.ValidationErrorInvalid)
			}
		}

		if d.PartnerID != uuid.Nil {
			partnerVal := entities.Entity{
				TableName:  "partner",
				ColumnName: "id",
				Value:      d.PartnerID.String(),
			}
			if count, err := subscription.repo.GetByID(ctx, partnerVal); err != nil || count == 0 {
				utils.AppendValuesToMap(validationErrors, consts.PartnerID, consts.ValidationErrorInvalid)
			}
		}

		// Check if the "TrackCount" field is provided and greater than or equal to 1
		if d.SubscriptionPlanDetails.TrackCount != 0 && d.SubscriptionPlanDetails.TrackCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanTrackCount, consts.ValidationErrorMinValue)
		}

		// Check if the "ArtistCount" field is provided and greater than or equal to 1
		if d.SubscriptionPlanDetails.ArtistCount != 0 && d.SubscriptionPlanDetails.ArtistCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanArtistCount, consts.ValidationErrorMinValue)
		}

		// Check if the "ProductCount" field is provided and greater than or equal to 1
		if d.SubscriptionPlanDetails.ProductCount != 0 && d.SubscriptionPlanDetails.ProductCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanProductCount, consts.ValidationErrorMinValue)
		}

		// Check if the "MaxArtistsPerProduct" field is provided and greater than or equal to 1
		if d.SubscriptionPlanDetails.MaxArtistsPerProduct != 0 && d.SubscriptionPlanDetails.MaxArtistsPerProduct < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanMaxArtistsProduct, consts.ValidationErrorMinValue)
		}

		// Check if the "MaxTracksPerProduct" field is provided and greater than or equal to 1
		if d.SubscriptionPlanDetails.MaxTracksPerProduct != 0 && d.SubscriptionPlanDetails.MaxTracksPerProduct < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanMaxTracksProduct, consts.ValidationErrorMinValue)
		}

		// Check if the "ProductTypes" field is provided and not empty
		if len(d.ProductTypes) != 0 {
			if err := subscription.repo.VerifyPartnerProductType(ctx, d.PartnerID.String(), d.ProductTypes); err != nil {
				utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanProductTypes, consts.ValidationErrorInvalid)
			}
		}

		// Check if the "TrackQuality" field is provided and not empty
		if len(d.TrackQuality) != 0 {
			if err := subscription.repo.VerifyPartnerTrackQuality(ctx, d.PartnerID.String(), d.TrackQuality); err != nil {
				utils.AppendValuesToMap(validationErrors, consts.SubscriptionPlanTRackQuality, consts.ValidationErrorInvalid)
			}
		}

	default:

		return validationErrors, fmt.Errorf("invalid input data type")
	}
	return validationErrors, nil
}

// EditSubscriptionPlanStore edits a subscription plan's stores.
func (subscription *SubscriptionUseCases) EditSubscriptionPlanStore(ctx context.Context, partnerID string, planID string, stores []string) error {
	log := logger.Log().WithContext(ctx)
	storeIds, err := subscription.repo.VerifyPartnerStores(ctx, partnerID, stores)
	if err != nil {
		log.Errorf("EditSubscriptionPlanStore failed, VerifyPartnerStores failed err=%s", err.Error())
		return err
	}
	err = subscription.repo.EditSubscriptionPlanStores(ctx, planID, storeIds)
	if err != nil {
		log.Errorf("EditSubscriptionPlanStore failed, err=%s", err.Error())
		return err
	}
	return nil
}

// EditSubscriptionPlanCountry edits a subscription plan's countries.
func (subscription *SubscriptionUseCases) EditSubscriptionPlanCountry(ctx context.Context, planID string, country []string) error {
	log := logger.Log().WithContext(ctx)
	countryIds, err := subscription.repo.GetCountriesID(ctx, country)
	if err != nil {
		log.Errorf("EditSubscriptionPlanCountry failed, GetCountriesID failed err=%s", err.Error())
		return err
	}
	err = subscription.repo.EditSubscriptionPlanCountry(ctx, planID, countryIds)
	if err != nil {
		log.Errorf("EditSubscriptionPlanCountry failed, err=%s", err.Error())
		return err
	}
	return nil
}

// GetSubscriptionPlanByID retrieves a subscription plan by ID.
func (subscription *SubscriptionUseCases) GetSubscriptionPlanByID(ctx context.Context, planID string) (*entities.SubscriptionPlan, error) {
	plan, err := subscription.repo.GetSubscriptionPlanByID(ctx, planID)
	if err != nil {
		return nil, err
	}

	// Check if the plan is deleted
	if plan.IsDeleted {
		return nil, errors.New("subscription plan is deleted")
	}

	// Check if the plan is not found
	if plan.ID == "" {
		return nil, errors.New("subscription plan not found")
	}

	return &plan, nil // Return a pointer to the plan
}

// GetAllSubscriptionPlans retrieves all subscription plans.
func (subscription *SubscriptionUseCases) GetAllSubscriptionPlans(ctx context.Context, reqData entities.ReqParams, searchFilter string, searchValue string, partnerID string) ([]entities.SubscriptionPlan, error) {
	offsetQuery := utilities.CalculateOffset(int(reqData.Page), int(reqData.Limit))
	subscriptionPlans, err := subscription.repo.GetAllSubscriptionPlans(ctx, offsetQuery, partnerID)
	if err != nil {
		return nil, err
	}
	filteredPlans := filterSubscriptionPlans(subscriptionPlans, searchFilter, searchValue)
	return filteredPlans, nil
}

func filterSubscriptionPlans(plans []entities.SubscriptionPlan, searchFilter string, searchValue string) []entities.SubscriptionPlan {
	if searchFilter == "" || searchValue == "" {
		return plans
	}

	var filteredPlans []entities.SubscriptionPlan
	for _, plan := range plans {
		switch searchFilter {
		case "partner":
			PartnerID, err := uuid.Parse(searchValue)
			if err != nil {
				fmt.Println("Error parsing UUID:", err)

			}
			if plan.PartnerID == PartnerID {
				filteredPlans = append(filteredPlans, plan)
			}
		case "name":
			if strings.Contains(strings.ToLower(plan.Name), strings.ToLower(searchValue)) {
				filteredPlans = append(filteredPlans, plan)
			}
		case "sku":
			if strings.Contains(strings.ToLower(plan.SKU), strings.ToLower(searchValue)) {
				filteredPlans = append(filteredPlans, plan)
			}
		case "status":
			status, err := strconv.ParseBool(searchValue)
			if err == nil && plan.IsActive == status {
				filteredPlans = append(filteredPlans, plan)
			}
		}
	}
	return filteredPlans
}

// EditSubscriptionPlan edits a subscription plan.
func (subscription *SubscriptionUseCases) EditSubscriptionPlan(ctx context.Context, planIDStr string, data map[string]interface{}) error {
	// Extract product types and track qualities from the data map
	productTypes, _ := data["product_type"].([]int)
	trackQualities, _ := data["track_quality"].([]int)

	// Remove existing associations
	if err := subscription.repo.RemoveExistingProductTypes(ctx, planIDStr); err != nil {
		return fmt.Errorf("failed to remove existing product type associations: %w", err)
	}
	if err := subscription.repo.RemoveExistingTrackQualities(ctx, planIDStr); err != nil {
		return fmt.Errorf("failed to remove existing track quality associations: %w", err)
	}

	// Update main subscription plan table
	if err := subscription.repo.EditSubscriptionPlan(ctx, planIDStr, data); err != nil {
		return err
	}

	// Insert new associations
	if len(productTypes) > 0 {
		if err := subscription.repo.InsertNewProductTypes(ctx, planIDStr, productTypes); err != nil {
			return fmt.Errorf("failed to insert new product type associations: %w", err)
		}
	}
	if len(trackQualities) > 0 {
		if err := subscription.repo.InsertNewTrackQualities(ctx, planIDStr, trackQualities); err != nil {
			return fmt.Errorf("failed to insert new track quality associations: %w", err)
		}
	}

	return nil
}

// GetCurrencyByID retrieves currency information by ID.
func (subscription *SubscriptionUseCases) GetCurrencyByID(ctx context.Context, currencyID int64) (entities.CurrencyResponse, error) {
	log := logger.Log().WithContext(ctx)
	currency, err := subscription.repo.GetCurrencyByID(ctx, currencyID)
	if err != nil {
		log.Errorf("GetCurrencyByID failed, err=%s", err.Error())
		return entities.CurrencyResponse{}, err
	}

	currencyResponse := entities.CurrencyResponse{
		ID:   currency.ID,
		Name: currency.Name,
		Code: currency.Code,
	}

	return currencyResponse, nil
}

// GetSubscriptionStores retrieves stores associated with a subscription plan.
func (subscription *SubscriptionUseCases) GetSubscriptionStores(ctx context.Context, planID string, page, perPage int) ([]entities.SubscriptionStoreResponse, int, error) {
	stores, totalCount, err := subscription.repo.GetSubscriptionStores(ctx, planID, page, perPage)
	if err != nil {
		return nil, 0, err
	}
	return stores, totalCount, nil
}

// GetSubscriptionCountries retrieves countries associated with a subscription plan.
func (subscription *SubscriptionUseCases) GetSubscriptionCountries(ctx context.Context, planID string, page, perPage int) ([]entities.SubscriptionCountryResponse, int, error) {
	countries, totalCount, err := subscription.repo.GetSubscriptionCountries(ctx, planID, page, perPage)
	if err != nil {
		return nil, 0, err
	}
	return countries, totalCount, nil
}

// DeleteSubscriptionPlan deletes a subscription plan.
func (subscription *SubscriptionUseCases) DeleteSubscriptionPlan(ctx context.Context, planID string, partnerID string) (string, error) {
	_, err := subscription.repo.GetSubscriptionPlanByID(ctx, planID)
	if err != nil {
		return "", fmt.Errorf("unable to find subscription plan, %s", err.Error())
	}

	operation, err := subscription.repo.DeleteSubscriptionPlan(ctx, planID, partnerID)
	if err != nil {
		return "", err
	}
	return operation, nil
}

// GetSubscription retrieves subscriptions.
func (subscription *SubscriptionUseCases) GetSubscription() []entities.Subscription {
	return subscription.repo.GetSubscription()
}

// CreateSubscriptionDuration creates a subscription duration.
func (subscription *SubscriptionUseCases) CreateSubscriptionDuration(ctx context.Context, subscriptionDuration entities.SubscriptionDuration) error {
	log := logger.Log().WithContext(ctx)
	subscriptionDuration.IntervalUnit = strings.TrimSpace(strings.ToUpper(subscriptionDuration.IntervalUnit))
	var err error
	subscriptionDuration.Value, err = utilities.CalculateIntervalValue(int(subscriptionDuration.IntervalCount), subscriptionDuration.IntervalUnit)
	if err != nil {
		log.Errorf("CreateSubscriptionDuration failed, CalculateIntervalValue failed, err=%s", err.Error())
		return err
	}
	subscriptionDuration.PaymentOptionText = fmt.Sprintf("%s%s", strings.ReplaceAll(subscriptionDuration.Name, " ", ""), consts.PaymentOptionText)
	// subscriptionDuration.PaymentOptionText = strings.ReplaceAll(subscriptionDuration.Name, " ", "") + consts.PaymentOptionText

	err = subscription.repo.CreateSubscriptionDuration(ctx, subscriptionDuration)
	if err != nil {
		log.Errorf("CreateSubscriptionDuration failed, err=%s", err.Error())
		return err
	}
	return nil
}

// GetTotalSubscriptionDurationCount gets the total count of subscription durations.
func (subscription *SubscriptionUseCases) GetTotalSubscriptionDurationCount(ctx context.Context) (int64, error) {
	// You can add the logic to query the total count from the repository here
	log := logger.Log().WithContext(ctx)
	totalCount, err := subscription.repo.GetTotalSubscriptionDurationCount(ctx)
	if err != nil {
		log.Errorf("GetTotalSubscriptionDurationCount failed, err=%s", err.Error())
		return 0, err
	}
	return totalCount, nil
}

// GetAllSubscriptionDuration retrieves all subscription durations.
func (subscription *SubscriptionUseCases) GetAllSubscriptionDuration(ctx context.Context, reqData entities.ReqParams) ([]entities.SubscriptionDuration, error) {
	log := logger.Log().WithContext(ctx)
	offsetQuery := utilities.CalculateOffset(int(reqData.Page), int(reqData.Limit))
	subscriptionDuration, err := subscription.repo.GetAllSubscriptionDuration(ctx, offsetQuery)
	if err != nil {
		log.Errorf("GetAllSubscriptionDuration failed, err=%s", err.Error())
		return nil, err
	}
	return subscriptionDuration, nil
}

// EditSubscriptionDuration edits a subscription duration.
func (subscription *SubscriptionUseCases) EditSubscriptionDuration(ctx context.Context, durationIDStr string, data map[string]interface{}) error {
	log := logger.Log().WithContext(ctx)
	durationID, err := utilities.StringToInt64(durationIDStr)
	if err != nil {
		log.Errorf("EditSubscriptionDuration failed, unable to convert %s to integer err=%s", durationIDStr, err.Error())
		return err
	}

	subscriptionDurationData, err := subscription.repo.GetSubscriptionDurationsByID(ctx, durationID)
	if err != nil {
		errMsg := fmt.Errorf("unable to find subscription duration for id =%v, error=%s", durationID, err.Error())
		log.Errorf("EditSubscriptionDuration failed, err=%s", errMsg.Error())
		return errMsg
	}

	if name, ok := data[consts.SubscriptionDurationName].(string); ok {
		data[consts.SubscriptionPaymentText] = fmt.Sprintf("%s%s", strings.ReplaceAll(name, " ", ""), consts.PaymentOptionText)
	}

	intervalCount, icOK := data[consts.SubscriptionDurationIntervalCount]
	intervalUnit, iuOK := data[consts.SubscriptionDurationIntervalUnit]

	if icOK && iuOK {
		intUnit := strings.TrimSpace(strings.ToUpper(intervalUnit.(string)))
		data[consts.SubscriptionValue], err = utilities.CalculateIntervalValue(int(intervalCount.(float64)), intUnit)
		if err != nil {
			log.Errorf("EditSubscriptionDuration failed, CalculateIntervalValue failed, data=%v err=%s", data, err.Error())
			return err
		}
		data[consts.SubscriptionDurationIntervalUnit] = intUnit
	} else if icOK && !iuOK {
		data[consts.SubscriptionValue], err = utilities.CalculateIntervalValue(int(intervalCount.(float64)), strings.TrimSpace(strings.ToUpper(subscriptionDurationData.IntervalUnit)))
		if err != nil {
			log.Errorf("EditSubscriptionDuration failed, CalculateIntervalValue failed, data=%v err=%s", data, err.Error())
			return err
		}

	} else if iuOK && !icOK {
		intUnit := strings.TrimSpace(strings.ToUpper(intervalUnit.(string)))
		data[consts.SubscriptionValue], err = utilities.CalculateIntervalValue(int(subscriptionDurationData.IntervalCount), intUnit)
		if err != nil {
			log.Errorf("EditSubscriptionDuration failed, CalculateIntervalValue failed, data=%v err=%s", data, err.Error())
			return err
		}
		data[consts.SubscriptionDurationIntervalUnit] = intUnit
	}

	err = subscription.repo.EditSubscriptionDuration(ctx, durationID, data)
	if err != nil {
		log.Errorf("EditSubscriptionDuration failed, err=%s", err.Error())
		return err
	}

	return nil
}

// DeleteSubscriptionDuration deletes a subscription duration.
func (subscription *SubscriptionUseCases) DeleteSubscriptionDuration(ctx context.Context, durationIDStr string) error {
	log := logger.Log().WithContext(ctx)
	durationID, err := utilities.StringToInt64(durationIDStr)
	if err != nil {
		log.Errorf("DeleteSubscriptionDuration failed, unable to convert %s to integer err=%s", durationIDStr, err.Error())
		return err
	}

	_, err = subscription.repo.GetSubscriptionDurationsByID(ctx, durationID)
	if err != nil {
		errMsg := fmt.Errorf("unable to find subscription duration for duration id=%v, error=%s", durationID, err.Error())
		log.Errorf("DeleteSubscriptionDuration failed, err=%s", errMsg.Error())
		return errMsg
	}

	err = subscription.repo.DeleteSubscriptionDuration(ctx, durationID)
	if err != nil {
		log.Errorf("DeleteSubscriptionDuration failed, err=%s", err.Error())
		return err
	}
	return nil

}

// ValidateSubscriptionDuration validates a subscription duration.
func (subscription *SubscriptionUseCases) ValidateSubscriptionDuration(ctx context.Context, data interface{}, method string) (map[string][]string, error) {
	log := logger.Log().WithContext(ctx)
	validationErrors := map[string][]string{}
	var err error

	switch strings.ToUpper(method) {
	case http.MethodPost:
		validationErrors, err = validateForPost(data)
		if err != nil {
			log.Errorf("ValidateSubscriptionDuration failed for POST request, err=%s", err.Error())
			return nil, err
		}
	case http.MethodPatch:
		validationErrors, err = validateForPatch(data)
		if err != nil {
			log.Errorf("ValidateSubscriptionDuration failed for PATCH request, err=%s", err.Error())
			return nil, err
		}
	default:
		errMsg := fmt.Errorf("invalid method for validation")
		log.Errorf("ValidateSubscriptionDuration failed, err=%s", errMsg.Error())
		return nil, errMsg
	}

	return validationErrors, nil
}

func validateForPost(sd interface{}) (map[string][]string, error) {
	validationErrors := make(map[string][]string)

	switch data := sd.(type) {
	case entities.SubscriptionDuration:
		if !utilities.IsNotEmpty(data.Name) {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationName, consts.ValidationErrorRequired)
		}
		if data.IntervalCount < 1 {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationIntervalCount, consts.ValidationErrorMinValue)
		}
		if valErr := utilities.ValidateIntervalUnit(strings.ToUpper(data.IntervalUnit)); valErr != nil {
			utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationIntervalUnit, consts.ValidationErrorInvalid)
		}
	default:
		return nil, fmt.Errorf("invalid input data type for data, type=%v", data)
	}

	return validationErrors, nil
}

func validateForPatch(data interface{}) (map[string][]string, error) {
	validationErrors := make(map[string][]string)

	switch d := data.(type) {
	case map[string]interface{}:
		for index, value := range d {
			switch index {
			case consts.SubscriptionDurationName:
				if !utilities.IsNotEmpty(value.(string)) {
					utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationName, consts.ValidationErrorRequired)
				}
			case consts.SubscriptionDurationIntervalCount:
				if value.(float64) < 1 {
					utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationIntervalCount, consts.ValidationErrorMinValue)
				}
			case consts.SubscriptionDurationIntervalUnit:
				if valErr := utilities.ValidateIntervalUnit(strings.ToUpper(value.(string))); valErr != nil {
					utils.AppendValuesToMap(validationErrors, consts.SubscriptionDurationIntervalUnit, consts.ValidationErrorInvalid)
				}
			}
		}
	default:
		return nil, fmt.Errorf("invalid input data type for data, type=%v", data)
	}

	return validationErrors, nil
}

// GetSubscriptionDurationByID retrieves a subscription duration by ID.
func (subscription *SubscriptionUseCases) GetSubscriptionDurationByID(ctx context.Context, durationID int64) (entities.SubscriptionDurationResponse, error) {
	log := logger.Log().WithContext(ctx)
	duration, err := subscription.repo.GetSubscriptionDurationByID(ctx, durationID)
	if err != nil {
		log.Errorf("GetSubscriptionDurationByID failed, err=%s", err.Error())
		return entities.SubscriptionDurationResponse{}, err
	}

	durationResponse := entities.SubscriptionDurationResponse{
		ID:            duration.ID,
		IntervalCount: duration.IntervalCount,
		IntervalUnit:  duration.IntervalUnit,
	}

	return durationResponse, nil
}
