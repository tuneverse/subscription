// nolint
package usecases

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"net/http"
	"subscription/internal/consts"
	"subscription/internal/entities"
	"subscription/internal/repo/mock"

	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/tuneverse/toolkit/core/logger"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"
)

func TestSubscriptionProductSwitch(t *testing.T) {

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
	}

	logger.InitLogger(clientOpt)

	id := uuid.New()

	testCases := []struct {
		name          string
		ID            uuid.UUID
		subscription  entities.SwitchSubscriptions
		buildStubs    func(subscription *mock.MockSubscriptionRepoImply)
		checkResponse func(t *testing.T, validationErr map[string][]string, err error)
	}{
		{
			name: "ok",
			ID:   id,
			subscription: entities.SwitchSubscriptions{

				CurrentSubscriptionID: "3e2b4a18-a42c-44a4-acff-a0f1f4561ee6",
				NewSubscriptionID:     "2d061b48-f075-4067-8f89-32d12249428d",
				ProductReferenceID:    "00bf6ca4-6c14-11ee-b962-0242ac120002",
			},
			buildStubs: func(subscription *mock.MockSubscriptionRepoImply) {

				subscription.EXPECT().
					SubscriptionProductSwitch(gomock.Any(), gomock.Eq(id), gomock.Any()).
					Times(1)
			},

			checkResponse: func(t *testing.T, validationErr map[string][]string, err error) {

				require.Equal(t, 0, len(validationErr))
				require.Nil(t, err)
			},
		},
		{
			name: "internal server error",
			ID:   id,
			subscription: entities.SwitchSubscriptions{

				CurrentSubscriptionID: "3e2b4a18-a42c-44a4-acff-a0f1f4561ee6",
				NewSubscriptionID:     "2d061b48-f075-4067-8f89-32d12249428d",
				ProductReferenceID:    "00bf6ca4-6c14-11ee-b962-0242ac120002",
			},
			buildStubs: func(subscription *mock.MockSubscriptionRepoImply) {

				subscription.EXPECT().
					SubscriptionProductSwitch(gomock.Any(), gomock.Eq(id), gomock.Any()).
					Times(1).
					Return(nil, sql.ErrConnDone)
			},

			checkResponse: func(t *testing.T, validationErr map[string][]string, err error) {

				require.Nil(t, validationErr)
				require.Error(t, err)
			},
		},
		{
			name: "validation error for required fields",
			ID:   id,
			subscription: entities.SwitchSubscriptions{

				CurrentSubscriptionID: "",
				NewSubscriptionID:     "",
				ProductReferenceID:    "",
			},
			buildStubs: func(subscription *mock.MockSubscriptionRepoImply) {

				subscription.EXPECT().
					SubscriptionProductSwitch(gomock.Any(), gomock.Eq(id), gomock.Any()).
					Times(0)
			},

			checkResponse: func(t *testing.T, validationErr map[string][]string, err error) {

				require.NotNil(t, validationErr)
				require.Nil(t, err)
			},
		},
	}

	for i := range testCases {

		tc := testCases[i]
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mock.NewMockSubscriptionRepoImply(ctrl)
			tc.buildStubs(store)
			subscriptionUsecase := NewSubscriptionUseCases(store)
			validationErr, err := subscriptionUsecase.SubscriptionProductSwitch(context.Background(), tc.ID, tc.subscription)

			tc.checkResponse(t, validationErr, err)
		})
	}

}

func TestViewAllSubscriptions(t *testing.T) {

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName,
		LogLevel:            "info",
		IncludeRequestDump:  false,
		IncludeResponseDump: false,
	}
	logger.InitLogger(clientOpt)

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	useCases := &SubscriptionUseCases{
		repo: mockRepo,
	}

	mockCtx := &gin.Context{}
	params := entities.ReqParams{
		Limit: 1,
		Page:  1,
	}

	memberID := uuid.New()
	expectedData := []entities.ListAllSubscriptions{
		{
			Name: "Basic Plan",
			SubscriptionDetails: entities.SubscriptionDetails{
				Status:         "Active",
				ExpirationDate: "2023-10-12T13:57:24.011426Z",
			},
			UsageDetails: entities.UsageDetails{
				ArtistsAdded: entities.ArtistsAdded{
					Added: 2,
					Total: 5,
				},
				TracksAdded: entities.TracksAdded{
					Added: 10,
					Total: 20,
				},
				ProductsAdded: entities.ProductsAdded{
					Added: 0,
					Total: 5,
				},
			},
		},
		{
			Name: "Diamond Plan",
			SubscriptionDetails: entities.SubscriptionDetails{
				Status:         "Inactive",
				ExpirationDate: "2023-10-12T13:55:57.78367Z",
			},
			UsageDetails: entities.UsageDetails{
				ArtistsAdded: entities.ArtistsAdded{
					Added: 2,
					Total: 2,
				},
				TracksAdded: entities.TracksAdded{
					Added: 5,
					Total: 5,
				},
				ProductsAdded: entities.ProductsAdded{
					Added: 0,
					Total: 1,
				},
			},
		},
		{
			Name: "Ruby Plan",
			SubscriptionDetails: entities.SubscriptionDetails{
				Status:         "Cancelled",
				ExpirationDate: "2023-10-13 11:22:36.883017",
			},
			UsageDetails: entities.UsageDetails{
				ArtistsAdded: entities.ArtistsAdded{
					Added: 2,
					Total: 5,
				},
				TracksAdded: entities.TracksAdded{
					Added: 1,
					Total: 5,
				},
				ProductsAdded: entities.ProductsAdded{
					Added: 0,
					Total: 4,
				},
			},
		},
	}

	expectedRecordCount := int64(len(expectedData))
	expectedError := error(nil)

	expectedMetaData := models.MetaData{
		Total:       expectedRecordCount,
		PerPage:     params.Limit,
		CurrentPage: params.Page,
		Next:        params.Page + 1,
		Prev:        params.Page - 1,
	}

	mockRepo.EXPECT().
		GetSubscriptionRecordCount(gomock.Any(), gomock.Eq(memberID)).Times(1).
		Return(expectedRecordCount, expectedError)

	mockRepo.EXPECT().
		ViewAllSubscriptions(gomock.Any(), gomock.Eq(memberID), params).Times(1).
		Return(expectedData, expectedError)

	data, metaData, validationErrors, err := useCases.ViewAllSubscriptions(mockCtx, memberID, params)

	assert.NoError(t, err)
	assert.Equal(t, expectedData, data)
	assert.Equal(t, 0, len(validationErrors))
	assert.Equal(t, expectedMetaData, metaData)

	// Error case- wrong page count
	params = entities.ReqParams{
		Limit: 8,
		Page:  3,
	}

	recordCount := int64(15)
	emptyMetaData := models.MetaData{}
	expectedValidationErrors := make(map[string][]string)

	utils.AppendValuesToMap(expectedValidationErrors, consts.Page, consts.Invalid)

	mockRepo.EXPECT().
		GetSubscriptionRecordCount(gomock.Any(), gomock.Eq(memberID)).Times(1).
		Return(recordCount, nil)

	data, metaData, validationErrors, err = useCases.ViewAllSubscriptions(mockCtx, memberID, params)

	assert.NoError(t, err)
	assert.Equal(t, expectedValidationErrors, validationErrors)
	assert.Equal(t, 0, len(data))
	assert.Equal(t, emptyMetaData, metaData)

	// Error No data test case
	params = entities.ReqParams{
		Limit: 10, // Set a valid limit
		Page:  1,
	}

	noDataError := errors.New("No record found")
	expectedEmptyData := []entities.ListAllSubscriptions{}
	recordCount = int64(0)
	emptyMetaData = models.MetaData{}

	utils.AppendValuesToMap(expectedValidationErrors, consts.Page, consts.Invalid)

	mockRepo.EXPECT().
		GetSubscriptionRecordCount(gomock.Any(), gomock.Any()).Times(1).
		Return(recordCount, noDataError)

	data, metaData, validationErrors, err = useCases.ViewAllSubscriptions(mockCtx, memberID, params)

	assert.EqualError(t, err, "No record found")
	assert.Equal(t, expectedEmptyData, data)
	assert.Equal(t, emptyMetaData, metaData)

	// Invalid memberId testcase
	noDataError = errors.New("Invalid member_id")
	expectedMember := false

	utils.AppendValuesToMap(expectedValidationErrors, consts.Page, consts.Invalid)

	mockRepo.EXPECT().
		GetSubscriptionRecordCount(gomock.Any(), gomock.Any()).Times(1).
		Return(recordCount, noDataError)

	mockRepo.EXPECT().
		IsMemberExists(gomock.Any(), gomock.Eq(memberID)).Times(1).
		Return(expectedMember, noDataError)

	exist, err := useCases.IsMemberExists(mockCtx, memberID)

	data, metaData, validationErrors, err = useCases.ViewAllSubscriptions(mockCtx, memberID, params)

	assert.EqualError(t, err, "Invalid member_id")
	assert.Equal(t, expectedMember, exist)

	// IsMemberExists function testcase
	var exists bool
	expectedRes := true
	expectedErr := error(nil)

	mockRepo.EXPECT().
		IsMemberExists(gomock.Any(), gomock.Eq(memberID)).Times(1).
		Return(exists, nil)

	exists, err = useCases.IsMemberExists(mockCtx, memberID)

	assert.NoError(t, err)
	assert.Equal(t, expectedRes, exists)
	assert.Equal(t, expectedErr, err)

	// GetSubscriptionRecordCount function ok testcase
	var count int64
	expectedErr = error(nil)

	mockRepo.EXPECT().
		GetSubscriptionRecordCount(gomock.Any(), gomock.Eq(memberID)).Times(1).
		Return(count, nil)

	count, err = useCases.GetSubscriptionRecordCount(mockCtx, memberID)

	assert.NoError(t, err)
	assert.Equal(t, expectedErr, err)

	//GetSubscriptionRecordCount function error case
	expectedErr = errors.New("GetSubscriptionRecordCount failed")

	mockRepo.EXPECT().
		GetSubscriptionRecordCount(gomock.Any(), gomock.Eq(memberID)).Times(1).
		Return(recordCount, expectedErr)

	count, err = useCases.GetSubscriptionRecordCount(mockCtx, memberID)

	assert.Error(t, err)
	assert.Equal(t, expectedErr, err)
	assert.EqualError(t, err, "GetSubscriptionRecordCount failed")

}
func TestCreateSubscriptionDuration(t *testing.T) {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	_ = logger.InitLogger(clientOpt)
	// Create a predefined SubscriptionDuration object for testing
	subscriptionDuration := entities.SubscriptionDuration{
		Name:              "Test Subscription",
		IntervalUnit:      "MONTH", // Ensure this is "Month" with a capital 'M'
		IntervalCount:     1,
		Value:             30,
		PaymentOptionText: "TestSubscriptionSubscriptionText",
		ID:                0,
	}

	testCases := []struct {
		name          string
		buildStubs    func(store *mock.MockSubscriptionRepoImply)
		checkResponse func(t *testing.T, err error)
	}{
		{
			name: "ok",
			buildStubs: func(store *mock.MockSubscriptionRepoImply) {
				store.EXPECT().
					CreateSubscriptionDuration(gomock.Any(), gomock.Eq(subscriptionDuration)).
					Times(1).
					Return(nil)
			},
			checkResponse: func(t *testing.T, err error) {
				require.Nil(t, err)
			},
		},
		{
			name: "internal server error",
			buildStubs: func(store *mock.MockSubscriptionRepoImply) {
				store.EXPECT().
					CreateSubscriptionDuration(gomock.Any(), gomock.Eq(subscriptionDuration)).
					Times(1).
					Return(errors.New("mocked repository error"))
			},
			checkResponse: func(t *testing.T, err error) {
				require.Error(t, err)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mock.NewMockSubscriptionRepoImply(ctrl)
			tc.buildStubs(store)
			subscriptionUseCases := NewSubscriptionUseCases(store)

			err := subscriptionUseCases.CreateSubscriptionDuration(context.Background(), subscriptionDuration)
			tc.checkResponse(t, err)
		})
	}
}

func TestGetAllSubscriptionDuration(t *testing.T) {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	_ = logger.InitLogger(clientOpt)
	// Create a predefined request data object for testing
	reqData := entities.ReqParams{
		Page:  1,
		Limit: 10,
	}

	// Create a list of SubscriptionDuration objects to be returned by the repository
	expectedSubscriptionDurations := []entities.SubscriptionDuration{
		{
			Name:              "Subscription 1",
			IntervalUnit:      "MONTH",
			IntervalCount:     1,
			Value:             30,
			PaymentOptionText: "Option 1",
			ID:                1,
		},
		{
			Name:              "Subscription 2",
			IntervalUnit:      "MONTH",
			IntervalCount:     2,
			Value:             60,
			PaymentOptionText: "Option 2",
			ID:                2,
		},
	}

	testCases := []struct {
		name          string
		buildStubs    func(store *mock.MockSubscriptionRepoImply)
		checkResponse func(t *testing.T, subscriptionDurations []entities.SubscriptionDuration, err error)
	}{
		{
			name: "ok",
			buildStubs: func(store *mock.MockSubscriptionRepoImply) {
				// Expect a call to GetAllSubscriptionDuration with the correct offset query
				store.EXPECT().
					GetAllSubscriptionDuration(gomock.Any(), gomock.Eq("LIMIT 10 OFFSET 0")).
					Times(1).
					Return(expectedSubscriptionDurations, nil)
			},
			checkResponse: func(t *testing.T, subscriptionDurations []entities.SubscriptionDuration, err error) {
				require.Nil(t, err)
				require.Equal(t, expectedSubscriptionDurations, subscriptionDurations)
			},
		},
		{
			name: "internal server error",
			buildStubs: func(store *mock.MockSubscriptionRepoImply) {
				// Expect a call to GetAllSubscriptionDuration and return an error
				store.EXPECT().
					GetAllSubscriptionDuration(gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil, errors.New("mocked repository error"))
			},
			checkResponse: func(t *testing.T, subscriptionDurations []entities.SubscriptionDuration, err error) {
				require.Error(t, err)
				require.Nil(t, subscriptionDurations)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mock.NewMockSubscriptionRepoImply(ctrl)
			tc.buildStubs(store)
			subscriptionUseCases := NewSubscriptionUseCases(store)

			subscriptionDurations, err := subscriptionUseCases.GetAllSubscriptionDuration(context.Background(), reqData)
			tc.checkResponse(t, subscriptionDurations, err)
		})
	}
}

func TestCreateSubscriptionPlanTransaction(t *testing.T) {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	_ = logger.InitLogger(clientOpt)
	// Create a predefined SubscriptionPlan object for testing
	subscriptionPlan := entities.SubscriptionPlan{
		ID:                       "1",
		Name:                     "Test Subscription Plan",
		CurrencyISO:              "USD",
		Amount:                   29.99,
		SubscriptionDurationID:   1,
		IsActive:                 true,
		PartnerID:                uuid.New(),
		TaxPercentage:            10.0,
		SKU:                      "TEST123",
		DisplayColor:             "#FFFFFF",
		IsFreeSubscription:       false,
		IsCancellationEnabled:    true,
		CanRenewableWithin:       1,
		FirstExpiryWarning:       7,
		SecondExpiryWarning:      3,
		ThirdExpiryWarning:       1,
		PlanInfoText:             "Test Subscription Plan Info",
		Position:                 1,
		IsDefault:                false,
		SubscriptionFor:          1,
		IsOneTimeSubscription:    false,
		SubscriptionLimitPerYear: 12,
		IsDeleted:                false,
		CreatedOn:                time.Now(),
		UpdatedBy:                uuid.New(),
		UpdatedOn:                time.Now(),
		DeletedBy:                uuid.New(),
		DeletedOn:                time.Now(),
		ProductTypes:             []int64{1, 2},
		TrackQuality:             []int64{1, 2},
		SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
			ArtistCount:          10,
			TrackCount:           100,
			MaxTracksPerProduct:  20,
			MaxArtistsPerProduct: 5,
			ProductCount:         5,
		},
	}

	testCases := []struct {
		name          string
		buildStubs    func(store *mock.MockSubscriptionRepoImply)
		checkResponse func(t *testing.T, err error)
	}{
		{
			name: "ok",
			buildStubs: func(store *mock.MockSubscriptionRepoImply) {
				store.EXPECT().
					CreateSubscriptionPlanTransaction(gomock.Any(), gomock.Eq(subscriptionPlan)).
					Times(1).
					Return(nil)
			},
			checkResponse: func(t *testing.T, err error) {
				require.Nil(t, err)
			},
		},
		{
			name: "internal server error",
			buildStubs: func(store *mock.MockSubscriptionRepoImply) {
				store.EXPECT().
					CreateSubscriptionPlanTransaction(gomock.Any(), gomock.Eq(subscriptionPlan)).
					Times(1).
					Return(errors.New("mocked repository error"))
			},
			checkResponse: func(t *testing.T, err error) {
				require.Error(t, err)
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			store := mock.NewMockSubscriptionRepoImply(ctrl)
			tc.buildStubs(store)
			subscriptionUseCases := NewSubscriptionUseCases(store)

			err := subscriptionUseCases.CreateSubscriptionPlan(context.Background(), subscriptionPlan)
			tc.checkResponse(t, err)
		})
	}
}

func TestEditSubscriptionPlanStore(t *testing.T) {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	_ = logger.InitLogger(clientOpt)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()
	partnerID := uuid.New().String()
	planID := uuid.New().String()
	stores := []string{"store1", "store2"}

	testCases := []struct {
		name           string
		buildStubs     func()
		expectedError  string
		expectedErrMsg string
	}{
		{
			name: "Valid Edit Subscription Plan Store",
			buildStubs: func() {
				// Mock VerifyPartnerStores to return store IDs
				mockRepo.EXPECT().
					VerifyPartnerStores(ctx, partnerID, stores).
					Times(1).
					Return([]int64{1, 2}, nil)

				// Mock EditSubscriptionPlanStores to return no error
				mockRepo.EXPECT().
					EditSubscriptionPlanStores(ctx, planID, []int64{1, 2}).
					Times(1).
					Return(nil)
			},
			expectedError:  "",
			expectedErrMsg: "",
		},
		{
			name: "VerifyPartnerStores Error",
			buildStubs: func() {
				// Mock VerifyPartnerStores to return an error
				mockRepo.EXPECT().
					VerifyPartnerStores(ctx, partnerID, stores).
					Times(1).
					Return(nil, errors.New("verification error"))
			},
			expectedError:  "verification error",
			expectedErrMsg: "verification error",
		},
		{
			name: "EditSubscriptionPlanStores Error",
			buildStubs: func() {
				// Mock VerifyPartnerStores to return store IDs
				mockRepo.EXPECT().
					VerifyPartnerStores(ctx, partnerID, stores).
					Times(1).
					Return([]int64{1, 2}, nil)

				// Mock EditSubscriptionPlanStores to return an error
				mockRepo.EXPECT().
					EditSubscriptionPlanStores(ctx, planID, []int64{1, 2}).
					Times(1).
					Return(errors.New("edit error"))
			},
			expectedError:  "edit error",
			expectedErrMsg: "edit error",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.buildStubs()
			err := subscriptionUseCases.EditSubscriptionPlanStore(ctx, partnerID, planID, stores)
			if tc.expectedError != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tc.expectedErrMsg)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestEditSubscriptionPlanCountry(t *testing.T) {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	_ = logger.InitLogger(clientOpt)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()
	planID := uuid.New().String()
	countries := []string{"USA", "IND"}

	testCases := []struct {
		name           string
		buildStubs     func()
		expectedError  string
		expectedErrMsg string
	}{
		{
			name: "Valid Edit Subscription Plan Country",
			buildStubs: func() {
				// Mock GetCountriesID to return country IDs
				mockRepo.EXPECT().
					GetCountriesID(ctx, countries).
					Times(1).
					Return([]int64{1, 2}, nil)

				// Mock EditSubscriptionPlanCountry to return no error
				mockRepo.EXPECT().
					EditSubscriptionPlanCountry(ctx, planID, []int64{1, 2}).
					Times(1).
					Return(nil)
			},
			expectedError:  "",
			expectedErrMsg: "",
		},
		{
			name: "GetCountriesID Error",
			buildStubs: func() {
				// Mock GetCountriesID to return an error
				mockRepo.EXPECT().
					GetCountriesID(ctx, countries).
					Times(1).
					Return(nil, errors.New("country ID error"))
			},
			expectedError:  "country ID error",
			expectedErrMsg: "country ID error",
		},
		{
			name: "EditSubscriptionPlanCountry Error",
			buildStubs: func() {
				// Mock GetCountriesID to return country IDs
				mockRepo.EXPECT().
					GetCountriesID(ctx, countries).
					Times(1).
					Return([]int64{1, 2}, nil)

				// Mock EditSubscriptionPlanCountry to return an error
				mockRepo.EXPECT().
					EditSubscriptionPlanCountry(ctx, planID, []int64{1, 2}).
					Times(1).
					Return(errors.New("edit error"))
			},
			expectedError:  "edit error",
			expectedErrMsg: "edit error",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.buildStubs()
			err := subscriptionUseCases.EditSubscriptionPlanCountry(ctx, planID, countries)
			if tc.expectedError != "" {
				require.Error(t, err)
				require.Contains(t, err.Error(), tc.expectedErrMsg)
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestValidateSubscriptionPlanForPost(t *testing.T) {
	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}
	_ = logger.InitLogger(clientOpt)
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	testCases := []struct {
		name           string
		buildStubs     func()
		expectedErrors map[string][]string
		expectedError  string
		data           entities.SubscriptionPlan
	}{
		{
			name: "Valid Subscription Plan",
			buildStubs: func() {
				// Mock GetSubscriptionDurationsByID and GetByID to return success
				mockRepo.EXPECT().
					GetSubscriptionDurationsByID(ctx, gomock.Eq(int64(1))).
					Times(2).
					Return(entities.SubscriptionDuration{}, nil)

				mockRepo.EXPECT().
					GetByID(ctx, gomock.Any()).
					Times(1).
					Return(int64(1), nil)

				mockRepo.EXPECT().
					VerifyPartnerProductType(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil)
				mockRepo.EXPECT().
					VerifyPartnerTrackQuality(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil)
			},
			expectedErrors: map[string][]string{},
			expectedError:  "",
			data: entities.SubscriptionPlan{
				Name:                   "Test Plan",
				Amount:                 10.0,
				CurrencyISO:            "USD",
				SubscriptionDurationID: 1,
				CanRenewableWithin:     1,
				PartnerID:              uuid.New(),
				SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
					ArtistCount:          5,
					TrackCount:           50,
					ProductCount:         5,
					MaxArtistsPerProduct: 2,
					MaxTracksPerProduct:  10,
				},
				ProductTypes: []int64{1, 2},
				TrackQuality: []int64{1, 2},
			},
		},
		{
			name: "Invalid Partner",
			buildStubs: func() {
				// Mock GetSubscriptionDurationsByID and GetByID to return success
				mockRepo.EXPECT().
					GetSubscriptionDurationsByID(ctx, gomock.Eq(int64(1))).
					Times(2).
					Return(entities.SubscriptionDuration{}, nil)

				mockRepo.EXPECT().
					GetByID(ctx, gomock.Any()).
					Times(1).
					Return(int64(0), errors.New("partner missing")) // Simulate partner ID not found

				mockRepo.EXPECT().
					VerifyPartnerProductType(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil)
				mockRepo.EXPECT().
					VerifyPartnerTrackQuality(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil)
			},
			expectedErrors: map[string][]string{
				consts.PartnerID: {consts.ValidationErrorInvalid},
			},
			expectedError: "",
			data: entities.SubscriptionPlan{
				Name:                   "Test Plan",
				Amount:                 10.0,
				CurrencyISO:            "USD",
				SubscriptionDurationID: 1,
				CanRenewableWithin:     1,
				PartnerID:              uuid.New(),
				SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
					ArtistCount:          5,
					TrackCount:           50,
					ProductCount:         5,
					MaxArtistsPerProduct: 2,
					MaxTracksPerProduct:  10,
				},
				ProductTypes: []int64{1, 2},
				TrackQuality: []int64{1, 2},
			},
		},
		{
			name: "Invalid Duration",
			buildStubs: func() {
				// Mock GetSubscriptionDurationsByID and GetByID to return success
				mockRepo.EXPECT().
					GetSubscriptionDurationsByID(ctx, gomock.Eq(int64(1))).
					Times(1).
					Return(entities.SubscriptionDuration{}, errors.New("invalid duration"))
				mockRepo.EXPECT().
					GetSubscriptionDurationsByID(ctx, gomock.Eq(int64(1))).
					Times(1).
					Return(entities.SubscriptionDuration{}, nil)

				mockRepo.EXPECT().
					GetByID(ctx, gomock.Any()).
					Times(1).
					Return(int64(1), nil) // Simulate partner ID not found

				mockRepo.EXPECT().
					VerifyPartnerProductType(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil)
				mockRepo.EXPECT().
					VerifyPartnerTrackQuality(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(nil)
			},
			expectedErrors: map[string][]string{
				consts.SubscriptionDurationID: {consts.ValidationErrorInvalid},
				//consts.SubscriptionPlanRenewableDuration: {consts.ValidationErrorInvalid},
			},
			expectedError: "",
			data: entities.SubscriptionPlan{
				Name:                   "Test Plan",
				Amount:                 10.0,
				CurrencyISO:            "USD",
				SubscriptionDurationID: 1,
				CanRenewableWithin:     1,
				PartnerID:              uuid.New(),
				SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
					ArtistCount:          5,
					TrackCount:           50,
					ProductCount:         5,
					MaxArtistsPerProduct: 2,
					MaxTracksPerProduct:  10,
				},
				ProductTypes: []int64{1, 2},
				TrackQuality: []int64{1, 2},
			},
		},
		{
			name: "Invalid Product types",
			buildStubs: func() {
				// Mock GetSubscriptionDurationsByID and GetByID to return success
				mockRepo.EXPECT().
					GetSubscriptionDurationsByID(ctx, gomock.Eq(int64(1))).
					Times(2).
					Return(entities.SubscriptionDuration{}, nil)

				mockRepo.EXPECT().
					GetByID(ctx, gomock.Any()).
					Times(1).
					Return(int64(1), nil)

				mockRepo.EXPECT().
					VerifyPartnerProductType(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(errors.New("invalid product types"))
				mockRepo.EXPECT().
					VerifyPartnerTrackQuality(ctx, gomock.Any(), gomock.Any()).
					Times(1).
					Return(errors.New("invalid product types"))
			},
			expectedErrors: map[string][]string{
				consts.SubscriptionPlanProductTypes: {consts.ValidationErrorInvalid},
				consts.SubscriptionPlanTRackQuality: {consts.ValidationErrorInvalid},
			},
			expectedError: "",
			data: entities.SubscriptionPlan{
				Name:                   "Test Plan",
				Amount:                 10.0,
				CurrencyISO:            "USD",
				SubscriptionDurationID: 1,
				CanRenewableWithin:     1,
				PartnerID:              uuid.New(),
				SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
					ArtistCount:          5,
					TrackCount:           50,
					ProductCount:         5,
					MaxArtistsPerProduct: 2,
					MaxTracksPerProduct:  10,
				},
				ProductTypes: []int64{1, 2},
				TrackQuality: []int64{1, 2},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.buildStubs()
			errors, err := subscriptionUseCases.validateSubscriptionPlanForPost(ctx, tc.data)

			if tc.expectedError != "" {
				require.NotNil(t, err)
				require.Equal(t, tc.expectedError, err.Error())
			} else {
				require.Nil(t, err)
				require.Equal(t, tc.expectedErrors, errors)
			}
		})
	}
}

func TestEditSubscriptionPlan(t *testing.T) {
	// Create a mock controller and repository
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create a test SubscriptionUseCases instance
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	// Define test data
	ctx := context.Background()
	planIDStr := "testPlanID"
	data := map[string]interface{}{
		"product_type":  []int{1, 2, 3},
		"track_quality": []int{4, 5, 6},
		// Add other fields as needed
	}

	// Define expected function calls and responses for the mock repository
	mockRepo.EXPECT().RemoveExistingProductTypes(ctx, planIDStr).Return(nil)
	mockRepo.EXPECT().RemoveExistingTrackQualities(ctx, planIDStr).Return(nil)
	mockRepo.EXPECT().EditSubscriptionPlan(ctx, planIDStr, data).Return(nil)
	mockRepo.EXPECT().InsertNewProductTypes(ctx, planIDStr, gomock.Eq([]int{1, 2, 3})).Return(nil)
	mockRepo.EXPECT().InsertNewTrackQualities(ctx, planIDStr, gomock.Eq([]int{4, 5, 6})).Return(nil)

	// Call the method being tested
	err := subscriptionUseCases.EditSubscriptionPlan(ctx, planIDStr, data)

	// Check for errors
	require.NoError(t, err)
}

func TestGetCurrencyByID(t *testing.T) {
	// Create a mock controller and repository
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create a test SubscriptionUseCases instance
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	// Define test data
	ctx := context.Background()
	currencyID := int64(123) // Replace with your desired currency ID

	// Define a mock Currency object to return
	mockCurrency := entities.CurrencyResponse{
		ID:   currencyID,
		Name: "TestCurrency",
		Code: "TEST",
	}

	// Specify the expected function call and return value for the mock repository
	mockRepo.EXPECT().GetCurrencyByID(ctx, currencyID).Return(mockCurrency, nil)

	// Call the method being tested
	currencyResponse, err := subscriptionUseCases.GetCurrencyByID(ctx, currencyID)

	// Check for errors
	require.NoError(t, err)

	// Check the returned currency response
	expectedResponse := entities.CurrencyResponse{
		ID:   mockCurrency.ID,
		Name: mockCurrency.Name,
		Code: mockCurrency.Code,
	}
	assert.Equal(t, expectedResponse, currencyResponse)
}

func TestGetSubscriptionStores(t *testing.T) {
	// Create a mock controller and repository
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create a test SubscriptionUseCases instance
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	// Define test data
	ctx := context.Background()
	planID := "testPlan"
	page := 1
	perPage := 10

	// Define a mock list of stores to return
	mockStores := []entities.SubscriptionStoreResponse{
		{
			ID:     "store1",
			Name:   "Store 1",
			Logo:   "logo1.png",
			Status: true,
		},
		{
			ID:     "store2",
			Name:   "Store 2",
			Logo:   "logo2.png",
			Status: true,
		},
	}

	// Define the total count of stores
	totalCount := len(mockStores)

	// Specify the expected function call and return value for the mock repository
	mockRepo.EXPECT().GetSubscriptionStores(ctx, planID, page, perPage).Return(mockStores, totalCount, nil)

	// Call the method being tested
	stores, returnedTotalCount, err := subscriptionUseCases.GetSubscriptionStores(ctx, planID, page, perPage)

	// Check for errors
	require.NoError(t, err)

	// Check the returned stores and total count
	assert.Equal(t, totalCount, returnedTotalCount)
	assert.Equal(t, mockStores, stores)
}

func TestGetSubscriptionCountries(t *testing.T) {
	// Create a mock controller and repository
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create a test SubscriptionUseCases instance
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	// Define test data
	ctx := context.Background()
	planID := "testPlanID"
	page := 1
	perPage := 10

	// Define a mock list of countries to return
	mockCountries := []entities.SubscriptionCountryResponse{
		{
			Code:   "C1",
			Name:   "Country 1",
			Status: true,
		},
		{
			Code:   "C2",
			Name:   "Country 2",
			Status: true,
		},
	}

	// Specify the expected function call and return value for the mock repository
	mockRepo.EXPECT().GetSubscriptionCountries(ctx, planID, page, perPage).Return(mockCountries, len(mockCountries), nil)

	// Call the method being tested
	countries, totalCount, err := subscriptionUseCases.GetSubscriptionCountries(ctx, planID, page, perPage)

	// Check for errors
	require.NoError(t, err)

	// Check the returned countries and total count
	expectedTotalCount := len(mockCountries)
	assert.Equal(t, expectedTotalCount, totalCount)
	assert.Equal(t, mockCountries, countries)
}

func TestDeleteSubscriptionPlan(t *testing.T) {
	// Create a mock controller and repository
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create a test SubscriptionUseCases instance
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	// Define test data
	ctx := context.Background()
	planID := "4524fa2c-9551-46f7-aa9b-a028fcbd0d56"
	userID := "123e4567-e89b-12d3-a456-426655440000"

	// Define a mock SubscriptionPlan for GetSubscriptionPlanByID
	mockSubscriptionPlan := entities.SubscriptionPlan{
		// Fill in the fields as per your SubscriptionPlans structure
		ID:                     planID,
		Name:                   "Test Plan",
		SubscriptionDurationID: 1,
		CanRenewableWithin:     1,
	}

	// Define the expected behavior of the repository mock
	mockRepo.EXPECT().GetSubscriptionPlanByID(ctx, planID).Return(mockSubscriptionPlan, nil) // Simulate a successful plan retrieval
	mockRepo.EXPECT().DeleteSubscriptionPlan(ctx, planID, userID).Return("deleted", nil)     // Simulate a successful deletion

	// Call the method being tested
	operation, err := subscriptionUseCases.DeleteSubscriptionPlan(ctx, planID, userID)

	// Check for errors
	require.NoError(t, err)

	// Check the returned operation
	expectedOperation := "deleted"
	assert.Equal(t, expectedOperation, operation)
}

func TestGetSubscriptionPlanByID(t *testing.T) {
	// Create a mock controller and repository
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	// Create a test SubscriptionUseCases instance
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	// Define test data
	ctx := context.Background()
	planID := "4524fa2c-9551-46f7-aa9b-a028fcbd0d56"

	// Define a mock SubscriptionPlan for GetSubscriptionPlanByID
	mockSubscriptionPlan := entities.SubscriptionPlan{
		// Fill in the fields as per your SubscriptionPlans structure
		ID:                     planID,
		Name:                   "Test Plan",
		SubscriptionDurationID: 1,
		CanRenewableWithin:     1,
	}

	// Specify the expected function call and return value for the mock repository
	mockRepo.EXPECT().GetSubscriptionPlanByID(ctx, planID).Return(mockSubscriptionPlan, nil)

	// Call the method being tested
	plan, err := subscriptionUseCases.GetSubscriptionPlanByID(ctx, planID)

	// Check for errors
	require.NoError(t, err)

	// Check the returned SubscriptionPlan
	expectedPlan := &mockSubscriptionPlan // Make sure to return a pointer to the plan
	assert.Equal(t, expectedPlan, plan)
}

func TestGetAllSubscriptionPlans(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)

	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()
	reqData := entities.ReqParams{Page: 1, Limit: 10}
	searchFilter := "name"
	searchValue := "example"
	partnerID := "partner123"

	// Define a mock list of subscription plans to return
	mockPlans := []entities.SubscriptionPlan{
		{
			ID:   "1",
			Name: "Plan 1",
			// Add other fields as needed
		},
		{
			ID:   "2",
			Name: "Plan 2",
			// Add other fields as needed
		},
	}

	mockRepo.EXPECT().GetAllSubscriptionPlans(
		gomock.Any(),
		gomock.Any(),
		gomock.Eq(partnerID),
	).Return(mockPlans, nil) // Returning mockPlans

	// Call the method being tested
	_, err := subscriptionUseCases.GetAllSubscriptionPlans(ctx, reqData, searchFilter, searchValue, partnerID)

	// Check for errors
	require.NoError(t, err)

}

func TestValidateSubscriptionPlanForPatch(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	// Define test data
	subscriptionPlan := entities.SubscriptionPlan{
		ID:                       "1",
		Name:                     "Test Subscription Plan",
		CurrencyISO:              "USD",
		Amount:                   29.99,
		SubscriptionDurationID:   1,
		IsActive:                 true,
		TaxPercentage:            10.0,
		SKU:                      "TEST123",
		DisplayColor:             "#FFFFFF",
		IsFreeSubscription:       false,
		IsCancellationEnabled:    true,
		CanRenewableWithin:       1,
		FirstExpiryWarning:       7,
		SecondExpiryWarning:      3,
		ThirdExpiryWarning:       1,
		PlanInfoText:             "Test Subscription Plan Info",
		Position:                 1,
		IsDefault:                false,
		SubscriptionFor:          1,
		IsOneTimeSubscription:    false,
		SubscriptionLimitPerYear: 12,
		IsDeleted:                false,
		CreatedOn:                time.Now(),
		UpdatedOn:                time.Now(),
		DeletedOn:                time.Now(),
		ProductTypes:             []int64{1, 2},
		TrackQuality:             []int64{1, 2},
		SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
			ArtistCount:          10,
			TrackCount:           100,
			MaxTracksPerProduct:  20,
			MaxArtistsPerProduct: 5,
			ProductCount:         5,
		},
	}

	testCases := []struct {
		name           string
		buildStubs     func()
		expectedErrors map[string][]string
		expectedError  string
		sd             interface{}
	}{
		// Test cases here
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.buildStubs()
			errors, err := subscriptionUseCases.validateSubscriptionPlanForPatch(ctx, &subscriptionPlan)

			if tc.expectedError != "" {
				require.NotNil(t, err)
				require.Equal(t, tc.expectedError, err.Error())
			} else {
				require.Nil(t, err)
				require.Equal(t, tc.expectedErrors, errors)
			}
		})
	}
}

func TestFilterSubscriptionPlans(t *testing.T) {
	partnerID, err := uuid.Parse(consts.SamplePartnerID)
	if err != nil {
		fmt.Println("Error parsing UUID:", err)
		return
	}
	plans := []entities.SubscriptionPlan{
		{
			PartnerID:             partnerID,
			Name:                  "Plan 1",
			SKU:                   "SKU123",
			IsFreeSubscription:    false,
			IsCancellationEnabled: true,
			IsOneTimeSubscription: false,
			IsActive:              true,
		},
		{
			PartnerID:             partnerID,
			Name:                  "Plan 2",
			SKU:                   "SKU456",
			IsFreeSubscription:    true,
			IsCancellationEnabled: false,
			IsOneTimeSubscription: true,
			IsActive:              false,
		},
	}

	t.Run("Filter by partner", func(t *testing.T) {
		_ = filterSubscriptionPlans(plans, "partner", "partner1")
	})

	t.Run("Filter by name", func(t *testing.T) {
		filteredPlans := filterSubscriptionPlans(plans, "name", "Plan 2")
		assert.Len(t, filteredPlans, 1)
		assert.Equal(t, plans[1], filteredPlans[0])
	})

	t.Run("No filter and value specified", func(t *testing.T) {
		filteredPlans := filterSubscriptionPlans(plans, "", "")
		assert.Len(t, filteredPlans, 2)
		assert.Equal(t, plans, filteredPlans)
	})

	t.Run("Filter by non-existent key", func(t *testing.T) {
		filteredPlans := filterSubscriptionPlans(plans, "nonexistent", "value")
		assert.Len(t, filteredPlans, 0)
	})
}

func TestValidateSubscriptionPlan(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()
	partnerIDString := "5bf922a4-775b-4177-a8f8-a43f032baf52"
	partnerID, err := uuid.Parse(partnerIDString)
	if err != nil {
		fmt.Println("Error parsing UUID:", err)
		return
	}
	InvalidsubscriptionPlan := &entities.SubscriptionPlan{}
	subscriptionPlan := &entities.SubscriptionPlan{
		Name:                     "Test Subscription Plan",
		PartnerID:                partnerID,
		CurrencyISO:              "USD",
		Amount:                   29.99,
		SubscriptionDurationID:   1,
		IsActive:                 true,
		TaxPercentage:            10.0,
		SKU:                      "TEST123",
		DisplayColor:             "#FFFFFF",
		IsFreeSubscription:       false,
		IsCancellationEnabled:    true,
		CanRenewableWithin:       1,
		FirstExpiryWarning:       7,
		SecondExpiryWarning:      3,
		ThirdExpiryWarning:       1,
		PlanInfoText:             "Test Subscription Plan Info",
		Position:                 1,
		IsDefault:                false,
		SubscriptionFor:          1,
		IsOneTimeSubscription:    false,
		SubscriptionLimitPerYear: 12,
		IsDeleted:                false,
		CreatedOn:                time.Now(),
		UpdatedOn:                time.Now(),
		SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
			ArtistCount:          10,
			TrackCount:           100,
			MaxTracksPerProduct:  20,
			MaxArtistsPerProduct: 5,
			ProductCount:         5,
		}}

	testCases := []struct {
		name           string
		buildStubs     func()
		expectedErrors map[string][]string
		expectedError  string
	}{
		{
			name: "Valid Subscription Plan",
			buildStubs: func() {
				// Populate the subscription plan and request method for the valid case
				requestMethod := "POST" // or "PATCH" or any valid request method
				// Call ValidateSubscriptionPlan with the populated subscription plan and request method
				_, err := subscriptionUseCases.ValidateSubscriptionPlan(ctx, subscriptionPlan, requestMethod)
				if err != nil {
					fmt.Println("tset", err)
				}
				// Check the expected errors and error message

			},
			expectedErrors: map[string][]string{},
			expectedError:  "",
		},
		{
			name: "Invalid Subscription Plan",
			buildStubs: func() {
				// Populate the subscription plan and request method for the valid case
				requestMethod := "POST" // or "PATCH" or any valid request method
				// Call ValidateSubscriptionPlan with the populated subscription plan and request method
				data, err := subscriptionUseCases.ValidateSubscriptionPlan(ctx, InvalidsubscriptionPlan, requestMethod)
				fmt.Println(data)
				if err != nil {
					fmt.Println("tset", err)
				}
				// Check the expected errors and error message

			},
			expectedErrors: map[string][]string{},
			expectedError:  "",
		},
		{
			name: "Valid Subscription Plan (PATCH)",
			buildStubs: func() {
				// Populate the subscription plan and request method for the valid case
				requestMethod := "PATCH"
				// Call ValidateSubscriptionPlan with the populated subscription plan and request method
				_, err := subscriptionUseCases.ValidateSubscriptionPlan(ctx, InvalidsubscriptionPlan, requestMethod)
				if err != nil {
					fmt.Println(err)
				}
				// Check the expected errors and error message
			},
			expectedErrors: map[string][]string{},
			expectedError:  "",
		},
		{
			name: "Valid Subscription Plan (PATCH)",
			buildStubs: func() {
				// Populate the subscription plan and request method for the valid case
				_, _ = subscriptionUseCases.ValidateSubscriptionPlan(ctx, InvalidsubscriptionPlan, "PATCH")

				// Call ValidateSubscriptionPlan with the populated subscription plan and request method
				_, err = subscriptionUseCases.validateSubscriptionPlanForPatch(ctx, InvalidsubscriptionPlan)
				if err != nil {
					fmt.Println(err)
				}
				// Check the expected errors and error message
			},
			expectedErrors: map[string][]string{},
			expectedError:  "",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.buildStubs()
		})
	}
}

func TestEditSubscriptionDuration(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	durationIDStr := "1"
	data := map[string]interface{}{
		// Your data for editing a subscription duration
	}

	// Mock the necessary repository calls
	mockRepo.EXPECT().GetSubscriptionDurationsByID(gomock.Any(), gomock.Any()).Return(entities.SubscriptionDuration{}, nil)
	mockRepo.EXPECT().EditSubscriptionDuration(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)

	// Call the method
	err := subscriptionUseCases.EditSubscriptionDuration(ctx, durationIDStr, data)

	// Assertions
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	// Additional assertions if needed based on the specific behavior of EditSubscriptionDuration
}

func TestsEditSubscriptionDuration(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	durationIDStr := "1"
	data := map[string]interface{}{
		// Your data for editing a subscription duration
	}

	// Mock the necessary repository calls
	mockRepo.EXPECT().GetSubscriptionDurationsByID(gomock.Any(), gomock.Any()).Return(entities.SubscriptionDuration{}, nil)
	mockRepo.EXPECT().EditSubscriptionDuration(gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)

	// Call the method
	err := subscriptionUseCases.EditSubscriptionDuration(ctx, durationIDStr, data)

	// Assertions
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	// Additional assertions if needed based on the specific behavior of EditSubscriptionDuration
}

func TestDeleteSubscriptionDuration(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	durationIDStr := "1"

	// Mock the necessary repository calls
	mockRepo.EXPECT().GetSubscriptionDurationsByID(gomock.Any(), gomock.Any()).Return(entities.SubscriptionDuration{}, nil)
	mockRepo.EXPECT().DeleteSubscriptionDuration(gomock.Any(), gomock.Any()).Return(nil)

	// Call the method
	err := subscriptionUseCases.DeleteSubscriptionDuration(ctx, durationIDStr)

	// Assertions
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	// Additional assertions if needed based on the specific behavior of DeleteSubscriptionDuration
}

func TestValidateSubscriptionDuration(t *testing.T) {
	ctx := context.Background()

	// Test case for validating a subscription duration (POST method)
	t.Run("ValidateSubscriptionDurationPOST", func(t *testing.T) {
		subscriptionUseCases := &SubscriptionUseCases{}
		data := entities.SubscriptionDuration{
			Name:          "durations week",
			IntervalCount: 7,
			IntervalUnit:  "week",

			// Populate with valid data for POST method
		}

		// Call the method
		errors, err := subscriptionUseCases.ValidateSubscriptionDuration(ctx, data, "POST")

		// Assertions
		if err != nil {
			t.Errorf("Expected no error, got %v", err)
		}
		if len(errors) != 0 {
			t.Errorf("Expected no validation errors, got %v", errors)
		}

		// Test case for validating a subscription duration (PATCH method)
		t.Run("ValidateSubscriptionDurationPATCH", func(t *testing.T) {
			subscriptionUseCases := &SubscriptionUseCases{}
			data := entities.SubscriptionDuration{
				Name:          " ",
				IntervalCount: 7,
				IntervalUnit:  "week",
			}
			data1 := entities.SubscriptionDuration{}
			_, _ = subscriptionUseCases.ValidateSubscriptionDuration(ctx, data1, "POST")
			_, _ = subscriptionUseCases.ValidateSubscriptionDuration(ctx, data1, "PATCH")

			// Call the method
			_, _ = subscriptionUseCases.ValidateSubscriptionDuration(ctx, data, "POST")
			_, _ = subscriptionUseCases.ValidateSubscriptionDuration(ctx, data, "PATCH")

		})
	})

}

func TestGetTotalSubscriptionDurationCount(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	// Expected total count from the mock repository
	expectedTotalCount := int64(42)

	// Mock the necessary repository call
	mockRepo.EXPECT().GetTotalSubscriptionDurationCount(gomock.Any()).Return(expectedTotalCount, nil)

	// Call the method
	totalCount, err := subscriptionUseCases.GetTotalSubscriptionDurationCount(ctx)

	// Assertions
	if err != nil {
		t.Errorf("Expected no error, got %v", err)
	}

	if totalCount != expectedTotalCount {
		t.Errorf("Expected total count %d, got %d", expectedTotalCount, totalCount)
	}
}

func TestEditSubscriptionDurations(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	// Mock data
	durationIDStr := "1"
	data := map[string]interface{}{
		consts.SubscriptionDurationName:         "Monthly",
		consts.SubscriptionDurationIntervalUnit: "month",
		// Add other fields as needed
	}

	// Test the case where StringToInt64 returns an error
	mockRepo.EXPECT().GetSubscriptionDurationsByID(gomock.Any(), gomock.Any()).Return(entities.SubscriptionDuration{}, errors.New("mock error"))

	// Call the method

	_ = subscriptionUseCases.EditSubscriptionDuration(ctx, durationIDStr, data)

	// Test the case where GetSubscriptionDurationsByID returns an error
	mockRepo.EXPECT().GetSubscriptionDurationsByID(gomock.Any(), gomock.Any()).Return(entities.SubscriptionDuration{}, nil)
	mockRepo.EXPECT().EditSubscriptionDuration(gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("mock error"))

	// Call the method
	_ = subscriptionUseCases.EditSubscriptionDuration(ctx, durationIDStr, data)

}

func TestGetSubscriptionDurationByID(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	// Mock data
	durationID := int64(1)
	expectedDuration := entities.SubscriptionDuration{
		ID:            durationID,
		IntervalCount: 3,
		IntervalUnit:  "Months",
		// Add other fields as needed
	}

	// Test the case where GetSubscriptionDurationByID is successful
	mockRepo.EXPECT().GetSubscriptionDurationByID(gomock.Any(), durationID).Return(expectedDuration, nil)

	// Call the method
	durationResponse, err := subscriptionUseCases.GetSubscriptionDurationByID(ctx, durationID)

	// Assertions
	assert.NoError(t, err, "Expected no error when GetSubscriptionDurationByID is successful")
	assert.Equal(t, expectedDuration.ID, durationResponse.ID, "IDs should match")
	assert.Equal(t, expectedDuration.IntervalCount, durationResponse.IntervalCount, "IntervalCounts should match")
	assert.Equal(t, expectedDuration.IntervalUnit, durationResponse.IntervalUnit, "IntervalUnits should match")

	// Test the case where GetSubscriptionDurationByID returns an error
	mockRepo.EXPECT().GetSubscriptionDurationByID(gomock.Any(), durationID).Return(entities.SubscriptionDuration{}, errors.New("mock error"))

	// Call the method
	_, err = subscriptionUseCases.GetSubscriptionDurationByID(ctx, durationID)

	// Assertions
	assert.Error(t, err, "Expected an error when GetSubscriptionDurationByID fails")
	// You can add more assertions for the specific error returned if needed
}

func TestValidateForPost(t *testing.T) {
	// Test case 1: Valid input
	t.Run("ValidInput", func(t *testing.T) {
		data := entities.SubscriptionDuration{
			Name:          "Monthly",
			IntervalCount: 1,
			IntervalUnit:  "month",
		}

		validationErrors, err := validateForPost(data)

		assert.NoError(t, err, "Expected no error for valid input")
		assert.Empty(t, validationErrors, "Expected no validation errors for valid input")
	})

	// Test case 2: Empty Name
	t.Run("EmptyName", func(t *testing.T) {
		data := entities.SubscriptionDuration{
			Name:          "",
			IntervalCount: -1,
		}

		validationErrors, err := validateForPost(data)

		assert.NoError(t, err, "Expected no error for empty Name (not validated in this function)")
		assert.Contains(t, validationErrors, consts.SubscriptionDurationName, "Expected validation error for empty Name")
	})

}

func TestGetTotalSubscriptionDurationCounts(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	// Test case 1: Successful retrieval
	t.Run("SuccessfulRetrieval", func(t *testing.T) {
		expectedCount := int64(10)

		// Mock the repository call
		mockRepo.EXPECT().GetTotalSubscriptionDurationCount(gomock.Any()).Return(expectedCount, nil)

		// Call the method
		totalCount, err := subscriptionUseCases.GetTotalSubscriptionDurationCount(ctx)

		// Assertions
		assert.NoError(t, err, "Expected no error for successful retrieval")
		assert.Equal(t, expectedCount, totalCount, "Total counts should match")
	})

}

func TestValidateSubscriptionDurationss(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := mock.NewMockSubscriptionRepoImply(ctrl)
	subscriptionUseCases := &SubscriptionUseCases{repo: mockRepo}

	ctx := context.Background()

	// Test case 1: Valid POST request
	t.Run("ValidPostRequest", func(t *testing.T) {
		testData := entities.SubscriptionDuration{
			Name:          "Annually",
			IntervalCount: 3,
			IntervalUnit:  "Year",
		}

		// Mock the validation function for POST
		validationErrors, err := subscriptionUseCases.ValidateSubscriptionDuration(ctx, testData, http.MethodPost)

		// Assertions
		assert.NoError(t, err, "Expected no error for valid POST request")
		assert.Empty(t, validationErrors, "Validation errors should be empty for valid POST request")
	})

	// Test case 2: Valid PATCH request
	t.Run("ValidPatchRequest", func(t *testing.T) {
		testData := map[string]interface{}{
			"SubscriptionDurationName":          "ValidName",
			"SubscriptionDurationIntervalCount": 3,
			"SubscriptionDurationIntervalUnit":  "Months",
		}

		// Mock the validation function for PATCH
		validationErrors, err := subscriptionUseCases.ValidateSubscriptionDuration(ctx, testData, http.MethodPatch)

		// Assertions
		assert.NoError(t, err, "Expected no error for valid PATCH request")
		assert.Empty(t, validationErrors, "Validation errors should be empty for valid PATCH request")
	})

}
