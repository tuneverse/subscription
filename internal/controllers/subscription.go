package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	constant "subscription/internal/consts"
	"subscription/internal/entities"
	"subscription/utilities"

	"subscription/internal/usecases"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/core/version"
	"gitlab.com/tuneverse/toolkit/models"
	"gitlab.com/tuneverse/toolkit/utils"

	"gitlab.com/tuneverse/toolkit/consts"
)

// SubscriptionController is responsible for handling subscription-related HTTP requests.
// It provides routing and uses SubscriptionUseCaseImply to process subscription operations.
type SubscriptionController struct {
	router   *gin.RouterGroup
	useCases usecases.SubscriptionUseCaseImply
}

// NewSubscriptionController creates a new instance of SubscriptionController.
func NewSubscriptionController(router *gin.RouterGroup, SubscriptionUseCase usecases.SubscriptionUseCaseImply) *SubscriptionController {
	return &SubscriptionController{
		router:   router,
		useCases: SubscriptionUseCase,
	}
}

// InitRoutes initializes the routes for subscription-related endpoints.
func (subscription *SubscriptionController) InitRoutes() {

	subscription.router.GET("/:version/health", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "HealthHandler")
	})
	subscription.router.POST("/:version/members/:member_id/subscriptions/checkout", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "SubscriptionCheckout")
	})
	subscription.router.PATCH("/:version/members/:member_id/subscriptions/renewal", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "SubscriptionRenewal")
	})
	subscription.router.PATCH("/:version/members/:member_id/subscriptions/cancel", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "SubscriptionCancellation")
	})
	subscription.router.PATCH("/:version/members/:member_id/subscriptions/product-switch", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "SubscriptionProductSwitch")
	})
	subscription.router.GET("/:version/members/:member_id/subscriptions", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "ViewAllSubscriptions")
	})
	subscription.router.POST("/:version/subscriptions/durations", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "CreateSubscriptionDuration")
	})
	subscription.router.GET("/:version/subscriptions/durations", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "GetAllSubscriptionDuration")
	})
	subscription.router.PATCH("/:version/subscriptions/durations/:duration_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "EditSubscriptionDuration")
	})
	subscription.router.DELETE("/:version/subscriptions/durations/:duration_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "DeleteSubscriptionDuration")
	})

	subscription.router.POST("/:version/subscriptions", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "CreateSubscriptionPlan")
	})
	subscription.router.GET("/:version/subscriptions", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "GetAllSubscriptionPlan")
	})
	subscription.router.PATCH("/:version/subscriptions/:plan_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "EditSubscriptionPlan")
	})
	subscription.router.DELETE("/:version/subscriptions/:plan_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "DeleteSubscriptionPlan")
	})
	subscription.router.GET("/:version/subscriptions/:plan_id", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "GetSingleSubscriptionPlan")
	})
	subscription.router.PATCH("/:version/subscriptions/:plan_id/stores", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "EditSubscriptionPlanStore")
	})
	subscription.router.PATCH("/:version/subscriptions/:plan_id/countries", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "EditSubscriptionPlanCountry")
	})

	subscription.router.GET("/:version/subscriptions/:plan_id/stores", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "GetSubscriptionStoreList")
	})
	subscription.router.GET("/:version/subscriptions/:plan_id/countries", func(ctx *gin.Context) {
		version.RenderHandler(ctx, subscription, "GetSubscriptionCountryList")
	})
}

// HealthHandler handles health checks for the subscription service.
func (subscription *SubscriptionController) HealthHandler(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "server run with base version",
	})
}

// SubscriptionCheckout handles the process of checking out a subscription for a member.
// This function performs the following steps:
//  1. Extracts the memberID from the URL.
//  2. Deserializes the JSON request body into a CheckoutSubscription object.
//  3. Performs any necessary validation on the checkoutData.
//  4. Calls the HandleSubscriptionCheckout use case function.
//  5. Responds with appropriate JSON results based on success or failure.
//
// Parameters:
//   - ctx (*gin.Context): The Gin context for handling the HTTP request.
func (subscription *SubscriptionController) SubscriptionCheckout(ctx *gin.Context) {

	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()

	// Check if the endpoint exists in the context
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists, if not, respond with a validation error.
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription checkout failed , endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription checkout failed,Failed to load context errors")
		return
	}

	// Extract memberID from the URL
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	// If parsing fails, log an error and return an appropriate JSON response
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription checkout  failed, invalid member_id: %s", err.Error())
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription checkout/purchase  failed")
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	// Deserialize the JSON request body into a CheckoutSubscription object
	var checkoutData entities.CheckoutSubscription
	if err := ctx.BindJSON(&checkoutData); err != nil {
		// Respond with an error message if JSON data is invalid
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription checkout  failed, invalid JSON data: %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	// Call the HandleSubscriptionCheckout use case function
	fieldsMap, err := subscription.useCases.HandleSubscriptionCheckout(ctx, memberID, checkoutData)
	if err != nil {
		// If an error occurs during the use case execution, log the error and return a 500 Internal Server Error response
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Checkout  failed, failed to checkout subscription: %s", err.Error())
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if hasVal {
			ctx.JSON(int(errorCode), val)
			return
		}
		return
	}
	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Checkout  failed, failed to checkout subscription")
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if hasVal {
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	// If successful, return a JSON response indicating success
	ctx.JSON(http.StatusOK, gin.H{
		"message": constant.SuccessfullyCheckedout,
	})
}

// SubscriptionRenewal handles the process of checking out a subscription for a member.
// This function performs the following steps:
//  1. Extracts the memberID from the URL.
//  2. Deserializes the JSON request body into a CheckoutSubscription object(same for renewal also).
//  3. Performs any necessary validation on the plan renewal
//  4. Calls the HandleSubscriptionRenewal use case function.
//  5. Responds with appropriate JSON results based on success or failure.
//
// Parameters:
//   - ctx (*gin.Context): The Gin context for handling the HTTP request.
func (subscription *SubscriptionController) SubscriptionRenewal(ctx *gin.Context) {

	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()

	// Check if the endpoint exists in the context
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists, if not, respond with a validation error.
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription renewal failed , endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription renewal failed,Failed to load context errors")
		return
	}
	// Extract memberID from the URL
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	// If parsing fails, log an error and return an appropriate JSON response
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription renewal   failed, invalid member_id: %s", err.Error())
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription checkout/purchase  failed")
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	// Deserialize the JSON request body into a CheckoutSubscription object
	var checkoutData entities.SubscriptionRenewal
	if err := ctx.BindJSON(&checkoutData); err != nil {
		// Respond with an error message if JSON data is invalid
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription renewal  failed, invalid JSON data: %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	// Call the HandleSubscriptionCheckout use case function
	fieldsMap, err := subscription.useCases.HandleSubscriptionRenewal(ctx, memberID, checkoutData)

	if err != nil {
		// If an error occurs during the use case execution, log the error and return a 500 Internal Server Error response
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Renewal  failed, failed to renew subscription: %s", err.Error())
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription checkout/purchase  failed")
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Renewal  failed, failed to renew subscription")
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription renewal failed")
			ctx.JSON(int(errorCode), val)
			return
		}
	}

	// If successful, return a JSON response indicating success
	ctx.JSON(http.StatusCreated, gin.H{
		"message": constant.SuccessfullyRenewed,
	})
}

// SubscriptionCancellation handles the process of canceling a subscription for a member.
// This function performs the following steps:
//  1. Extracts the memberID from the URL.
//  2. Deserializes the JSON request body into a CancelSubscription object.
//  3. Performs any necessary validation on the plan cancellation.
//  4. Calls the HandleSubscriptionCancellation use case function.
//  5. Responds with appropriate JSON results based on success or failure.
//
// Parameters:
//   - ctx (*gin.Context): The Gin context for handling the HTTP request.
func (subscription *SubscriptionController) SubscriptionCancellation(ctx *gin.Context) {

	// Retrieve and preprocess request details
	method := strings.ToLower(ctx.Request.Method)
	endpointUrl := ctx.FullPath()

	// Check if the endpoint exists in the context
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	// Check if the endpoint exists, if not, respond with a validation error.
	if !isEndpointExists {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription cancellation failed, endpoint does not exist in the database.")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	// Get the contextError map to handle error responses.
	contextError, errVal := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if !errVal {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription cancellation failed,Failed to load context errors")
		return
	}

	// Extract memberID from the URL
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	// If parsing fails, log an error and return an appropriate JSON response
	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription cancellation failed, invalid member_id: %s", err.Error())
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription cancellation failed")
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	// Deserialize the JSON request body into a cancellationData object
	var cancellationData entities.CancelSubscription
	if err := ctx.BindJSON(&cancellationData); err != nil {
		// Respond with an error message if JSON data is invalid
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Subscription cancellation failed, invalid JSON data: %s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	// Call the HandleSubscriptionCancellation use case function
	fieldsMap, err := subscription.useCases.HandleSubscriptionCancellation(ctx, memberID, cancellationData)

	if err != nil {
		// If an error occurs during the use case execution, log the error and return a 500 Internal Server Error response
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Cancellation failed, failed to cancel subscription: %s", err.Error())
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription cancellation failed:Internal Server Error ")
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	if len(fieldsMap) > 0 {
		fields := utils.FieldMapping(fieldsMap)
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Cancellation failed, failed to cancel subscription")
		val, hasVal, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		if hasVal {
			logger.Log().WithContext(ctx.Request.Context()).Error("Subscription cancellation failed,Validations Erros")
			ctx.JSON(int(errorCode), val)
			return
		}
	}
	// If successful, return a JSON response indicating success
	ctx.JSON(http.StatusOK, gin.H{
		"message": constant.SuccessfullyCancelled,
	})

}

// SubscriptionProductSwitch handles the product switch between subscription plans choosed by a member.
func (subscription *SubscriptionController) SubscriptionProductSwitch(ctx *gin.Context) {

	var data entities.SwitchSubscriptions
	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	if err != nil {
		logger.Log().WithContext(ctx).Errorf("Invalid member id err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest,
			gin.H{
				"message": "inavalid member id",
				"error":   err.Error(),
			})
		return
	}

	ctxt := ctx.Request.Context()
	err = ctx.BindJSON(&data)

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("Switch products between subscriptions failed err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest,
			gin.H{
				"message": "Binding failed",
				"error":   err.Error(),
			})
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("Switch product between subscriptions failed: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, isErrorExists := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	if !isErrorExists {
		logger.Log().WithContext(ctxt).Errorf("Switch products between subscriptions failed, err = %s", consts.ContextErr)
		ctx.JSON(http.StatusBadRequest, gin.H{

			"errorCode": http.StatusBadRequest,
			"message":   consts.ContextErr,
			"errors":    nil,
		})
		return
	}
	// Call the SubscriptionProductSwitch use case with the extracted memberID and data
	validationErrors, err := subscription.useCases.SubscriptionProductSwitch(ctx, memberID, data)

	if len(validationErrors) != 0 {
		fields := utils.FieldMapping(validationErrors)
		val, errVal, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)

		if errVal {
			ctx.JSON(int(errorCode), val)
		}
		return
	}

	if err != nil {
		logger.Log().WithContext(ctxt).Errorf("Switch products between subscriptions failed err=%s", err.Error())
		val, errVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if errVal {
			ctx.JSON(int(errorCode), val)
		}
		return
	}

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"message": "Switch products between subscriptions failed",
			"error":   err.Error(),
		})
		return
	}

	// Product switching between subscriptions success
	ctx.JSON(http.StatusOK, gin.H{
		"message": constant.Success,
	})

	// Log the success message
	logger.Log().WithContext(ctx.Request.Context()).Info("Switch products between subscriptions: Successfully Switched Products between Subscriptions plans")
}

// ViewAllSubscriptions handles the view of all subscription plans choosed by a member.
func (subscription *SubscriptionController) ViewAllSubscriptions(ctx *gin.Context) {

	//var metaData entities.MetaData
	var reqParam entities.ReqParams

	if err := ctx.BindQuery(&reqParam); err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointURL := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointURL, method)

	if !isEndpointExists {
		logger.Log().WithContext(ctx).Errorf("Get subscriptions for member failed: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, isErrorExists := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	if !isErrorExists {
		logger.Log().WithContext(ctx).Errorf("Switch products between subscriptions failed, err = %s", consts.ContextErr)
		ctx.JSON(http.StatusBadRequest, gin.H{

			"errorCode": http.StatusBadRequest,
			"message":   consts.ContextErr,
			"errors":    nil,
		})
		return
	}

	memberIDStr := ctx.Param("member_id")
	memberID, err := uuid.Parse(memberIDStr)

	if err != nil {
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Get subscriptions for member failed: Invalid member_id: %s", err.Error())
		val, errVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if errVal {
			ctx.JSON(int(errorCode), val)
		}
		return
	}

	// Call the ViewAllSubscriptions use case with the extracted memberID
	memberSubscriptions, metadata, validationErrors, err := subscription.useCases.ViewAllSubscriptions(ctx, memberID, reqParam)

	if len(validationErrors) != 0 {
		logger.Log().WithContext(ctx).Errorf("Get subscriptions for member failed: validation error")
		fields := utils.FieldMapping(validationErrors)
		val, errVal, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		if errVal {
			ctx.JSON(int(errorCode), val)
		}
		return
	}

	if err != nil {
		// Check if the error message contains "No record found"
		if strings.Contains(err.Error(), "No record found") {
			// Handle the "No record found" error
			logger.Log().WithContext(ctx.Request.Context()).Errorf("Get subscriptions for member: Failed to retrieve subscriptions, err=%s", err.Error())
			val, errVal, errorCode := utils.ParseFields(ctx, consts.NotFound, "", contextError, "", "")
			if errVal {
				ctx.JSON(int(errorCode), val)
			}
			return
		}
	}

	if err != nil {
		// Handle other errors, e.g., internal server error
		logger.Log().WithContext(ctx.Request.Context()).Errorf("Get subscriptions for member: Failed to retrieve subscriptions, err=%s", err.Error())
		val, errVal, errorCode := utils.ParseFields(ctx, consts.InternalServerErr, "", contextError, "", "")
		if errVal {
			ctx.JSON(int(errorCode), val)
		}
		return
	}

	successResponse := entities.SuccessResponse{
		Code:     constant.StatusOk,
		Message:  constant.SuccessfullyListed,
		Metadata: metadata,
		Data:     memberSubscriptions,
	}

	ctx.JSON(http.StatusOK, successResponse)

	// Log the success message
	logger.Log().WithContext(ctx.Request.Context()).Info("View All Subscriptions: Subscription plans listed successfully")
}

// CreateSubscriptionDuration handles the creation of a subscription duration.
func (subscription *SubscriptionController) CreateSubscriptionDuration(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	var duration entities.SubscriptionDuration
	if err := ctx.Bind(&duration); err != nil {
		log.Errorf("CreateSubscriptionDuration failed, Invalid json, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return

	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		log.Errorf("CreateSubscriptionDuration failed: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	fieldsMap, err := subscription.useCases.ValidateSubscriptionDuration(ctxt, duration, ctx.Request.Method)
	if err != nil {
		log.Errorf("CreateSubscriptionDuration failed,  ValidateSubscriptionDuration failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	if len(fieldsMap) != 0 {
		log.Errorf("CreateSubscriptionDuration failed: validation error")
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	if err := subscription.useCases.CreateSubscriptionDuration(ctxt, duration); err != nil {
		log.Errorf("CreateSubscriptionDuration failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "subscription duration created successfully",
	})
}

// GetAllSubscriptionDuration retrieves a list of all subscription durations.
func (subscription *SubscriptionController) GetAllSubscriptionDuration(ctx *gin.Context) {
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	var reqParam entities.ReqParams

	if err := ctx.BindQuery(&reqParam); err != nil {
		log.Errorf("GetAllSubscriptionDuration failed, Invalid json, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	totalCount, err := subscription.useCases.GetTotalSubscriptionDurationCount(ctxt)
	if err != nil {
		log.Errorf("GetAllSubscriptionDuration failed, GetTotalSubscriptionDurationCount failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	subscriptionDurations, err := subscription.useCases.GetAllSubscriptionDuration(ctxt, reqParam)
	if err != nil {
		log.Errorf("GetAllSubscriptionDuration failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	// Calculate the total number of subscription durations
	currentPage := int(reqParam.Page)
	nextPage := currentPage + 1
	prevPage := currentPage - 1
	// Create and populate metadata
	metaData := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(reqParam.Limit),
		CurrentPage: int32(reqParam.Page),
		Next:        int32(nextPage),
		Prev:        int32(prevPage),
	}

	// Add metadata to the response
	response := gin.H{
		"message":  "subscription duration retrieved successfully",
		"data":     subscriptionDurations,
		"metadata": utilities.MetaDataInfo(metaData),
	}

	ctx.JSON(http.StatusOK, response)
}

// EditSubscriptionDuration handles the editing of a subscription duration.
func (subscription *SubscriptionController) EditSubscriptionDuration(ctx *gin.Context) {

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	durationID := ctx.Param("duration_id")
	if len(durationID) < 1 {
		log.Errorf("EditSubscriptionDuration failed, duration_id is missing")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}

	var duration map[string]interface{}
	if err := ctx.Bind(&duration); err != nil {
		log.Errorf("EditSubscriptionDuration failed, Invalid json data, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		log.Errorf("CreateSubscriptionDuration failed: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	fieldsMap, err := subscription.useCases.ValidateSubscriptionDuration(ctxt, duration, ctx.Request.Method)
	if err != nil {
		log.Errorf("EditSubscriptionDuration failed, ValidateSubscriptionDuration failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	if len(fieldsMap) != 0 {
		log.Errorf("CreateSubscriptionDuration failed: validation error")
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	if err := subscription.useCases.EditSubscriptionDuration(ctxt, durationID, duration); err != nil {
		log.Errorf("EditSubscriptionDuration failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "subscription duration updated successfully",
	})
}

// DeleteSubscriptionDuration handles the deletion of a subscription duration.
func (subscription *SubscriptionController) DeleteSubscriptionDuration(ctx *gin.Context) {

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	durationID := ctx.Param("duration_id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if len(durationID) < 1 {
		log.Errorf("DeleteSubscriptionDuration failed, duration_id is missing")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}
	if err := subscription.useCases.DeleteSubscriptionDuration(ctxt, durationID); err != nil {
		log.Errorf("DeleteSubscriptionDuration failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "subscription duration deleted successfully",
	})
}

// CreateSubscriptionPlan handles the creation of a subscription plan.
func (subscription *SubscriptionController) CreateSubscriptionPlan(ctx *gin.Context) {

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	var subscriptionPlan entities.SubscriptionPlan

	if err := ctx.Bind(&subscriptionPlan); err != nil {
		log.Errorf("CreateSubscriptionPlan failed, invalid Json, err=%s", err.Error())
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return

	}

	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)
	if !isEndpointExists {
		logger.Log().WithContext(ctxt).Errorf("CreateSubscriptionDuration failed: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)

	fieldsMap, err := subscription.useCases.ValidateSubscriptionPlan(ctxt, subscriptionPlan, ctx.Request.Method)
	if err != nil {
		log.Errorf("CreateSubscriptionPlan failed, ValidateSubscriptionPlan failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	if len(fieldsMap) != 0 {
		log.Errorf("CreateSubscriptionDuration failed: validation error")
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, consts.ValidationErr,
			fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}
	if err := subscription.useCases.CreateSubscriptionPlan(ctxt, subscriptionPlan); err != nil {
		log.Errorf("CreateSubscriptionPlan failed,  err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{
		"message": "subscription plan created successfully",
	})
}

// GetSingleSubscriptionPlan retrieves information about a single subscription plan.
func (subscription *SubscriptionController) GetSingleSubscriptionPlan(ctx *gin.Context) {
	log := logger.Log().WithContext(ctx)
	planID := ctx.Param("plan_id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)

	if len(planID) < 1 {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "ID is missing",
		})
		return
	}

	plan, err := subscription.useCases.GetSubscriptionPlanByID(ctx, planID)
	if err != nil {
		log.Errorf("GetSubscriptionPlanByID failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		if err != nil && err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, gin.H{
				"error": "Subscription plan not found",
			})
		} else if err.Error() == "Subscription plan not found" {

		} else if err.Error() == "Subscription plan is deleted" {
			ctx.JSON(http.StatusNotFound, gin.H{
				"error": "Subscription plan is deleted",
			})
		} else {
			ctx.JSON(int(errorCode), val)
		}
		return
	}

	// If plan is still nil at this point, it means an unexpected error occurred
	if plan == nil {
		ctx.JSON(http.StatusNotFound, gin.H{
			"error": "Subscription plan not found",
		})
		return
	}

	// Convert the plan to the desired response format
	duration, err := subscription.useCases.GetSubscriptionDurationByID(ctx, plan.SubscriptionDurationID)
	if err != nil {
		log.Errorf("GetSingleSubscriptionPlan failed, err=%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"code":    500,
			"message": err.Error(),
		})
		return
	}

	currency, currencyErr := subscription.useCases.GetCurrencyByID(ctx, plan.CurrencyID)
	if currencyErr != nil {
		log.Errorf("GetSingleSubscriptionPlan failed, err=%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"code":    500,
			"message": "Internal Server Error",
		})
		return
	}

	productTypes := []entities.ProductTypeResponse{}    // Populate this array with ProductTypeResponse items
	trackQualities := []entities.TrackQualityResponse{} // Populate this array with TrackQualityResponse items

	// Populate other fields as needed using the plan data

	responsePlan := entities.SubscriptionPlan{
		ID:                       plan.ID,
		Name:                     plan.Name,
		SKU:                      plan.SKU,
		PartnerID:                plan.PartnerID,
		Amount:                   plan.Amount,
		Position:                 plan.Position,
		TaxPercentage:            plan.TaxPercentage,
		Info:                     plan.PlanInfoText,
		IsFreeSubscription:       plan.IsFreeSubscription,
		IsCancellationEnabled:    plan.IsCancellationEnabled,
		IsOneTimeSubscription:    plan.IsOneTimeSubscription,
		IsDefault:                plan.IsDefault,
		Status:                   plan.IsActive,
		SubscriptionCount:        int(plan.SubscriptionLimitPerYear),
		Duration:                 duration,
		SubscriptionLimitPerYear: plan.SubscriptionLimitPerYear,
		Currency:                 currency,
		DisplayColor:             plan.DisplayColor,
		CanRenewableWithin:       plan.CanRenewableWithin,
		FirstExpiryWarning:       plan.FirstExpiryWarning,
		SecondExpiryWarning:      plan.SecondExpiryWarning,
		ThirdExpiryWarning:       plan.ThirdExpiryWarning,
		ProductTypeResponse:      productTypes,
		TrackQualityResponse:     trackQualities,
		SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
			ArtistCount:          plan.ArtistCount,
			TrackCount:           plan.TrackCount,
			ProductCount:         int(plan.ProductCount),
			MaxTracksPerProduct:  plan.MaxTracksPerProduct,
			MaxArtistsPerProduct: plan.MaxArtistsPerProduct,
		},
	}

	ctx.JSON(http.StatusOK, gin.H{
		"code":    "200",
		"message": "Subscription plan retrieved successfully",
		"data":    responsePlan,
	})
}

// EditSubscriptionPlanStore handles editing a subscription plan's store information.
func (subscription *SubscriptionController) EditSubscriptionPlanStore(ctx *gin.Context) {
	//todo -------------need to handle dynamic once auth middleware func was ready
	partnerID := constant.SamplePartnerID
	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	planID := ctx.Param("plan_id")
	contextError, _ := utils.GetContext[map[string]any](ctx, consts.ContextErrorResponses)
	if len(planID) < 1 {
		log.Errorf("EditSubscriptionPlanStore failed, plan id missing")
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	var reqData map[string][]string
	if err := ctx.Bind(&reqData); err != nil || reqData == nil {
		log.Errorf("EditSubscriptionPlanStore failed, invalid json,request data=%v,  err=%s", reqData, err)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	stores, ok := reqData["stores"]
	if !ok {
		log.Errorf("EditSubscriptionPlanStore failed, invalid json data, stores missing in request Data")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid JSON data",
		})
		return
	}
	if err := subscription.useCases.EditSubscriptionPlanStore(ctxt, partnerID, planID, stores); err != nil {
		log.Errorf("EditSubscriptionPlanStore failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "stores updated successfully",
	})

}

// EditSubscriptionPlanCountry handles editing a subscription plan's country information.
func (subscription *SubscriptionController) EditSubscriptionPlanCountry(ctx *gin.Context) {

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	planID := ctx.Param("plan_id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if len(planID) < 1 {
		log.Errorf("EditSubscriptionPlanCountry failed, plan id is missing")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}
	var reqData map[string][]string
	if err := ctx.Bind(&reqData); err != nil || reqData == nil {
		log.Errorf("EditSubscriptionPlanCountry failed, invalid json data, request data=%v, err=%s", reqData, err)
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}
	countries, ok := reqData["countries"]
	if !ok {
		log.Errorf("EditSubscriptionPlanCountry failed, invalid json data, countries missing in request Data")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid JSON data",
		})
		return
	}
	if err := subscription.useCases.EditSubscriptionPlanCountry(ctxt, planID, countries); err != nil {
		log.Errorf("EditSubscriptionPlanCountry failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"message": "countries updated successfully",
	})
}

// GetAllSubscriptionPlan retrieves a list of all subscription plans.
func (subscription *SubscriptionController) GetAllSubscriptionPlan(ctx *gin.Context) {
	partnerID := "6f8a2d51-84df-4a1f-9d4b-e79a2c5a9c85"
	log := logger.Log().WithContext(ctx)
	var reqParam entities.ReqParams
	if err := ctx.BindQuery(&reqParam); err != nil {
		ctx.JSON(http.StatusBadRequest, err)
		return
	}

	// Extract the search filter parameters from the query
	searchFilter := ctx.DefaultQuery("search_filter", "")
	searchValue := ctx.DefaultQuery("search_value", "")

	subscriptionPlans, err := subscription.useCases.GetAllSubscriptionPlans(ctx, reqParam, searchFilter, searchValue, partnerID)
	if err != nil {
		log.Errorf("GetAllSubscriptionPlan failed, err=%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"code":        500,
			"description": "Internal Server Error",
		})
		return
	}

	currentPage := int(reqParam.Page)
	nextPage := currentPage + 1
	prevPage := currentPage - 1

	// Create and populate metadata
	meta := entities.MetaData{
		Total:       int64(len(subscriptionPlans)),
		PerPage:     int32(reqParam.Limit),
		CurrentPage: int32(reqParam.Page),
		Next:        int32(nextPage),
		Prev:        int32(prevPage),
	}

	// Restructure subscriptionPlans into the new response format
	var responseData []entities.SubscriptionPlan
	for _, plan := range subscriptionPlans {
		duration, err := subscription.useCases.GetSubscriptionDurationByID(ctx, plan.SubscriptionDurationID)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"code":    500,
				"message": err.Error(),
			})
			return
		}

		currency, currencyErr := subscription.useCases.GetCurrencyByID(ctx, plan.CurrencyID)
		if currencyErr != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"code":    500,
				"message": "Internal Server Error",
			})
			return
		}

		productTypes := []entities.ProductTypeResponse{}    // Populate this array with ProductTypeResponse items
		trackQualities := []entities.TrackQualityResponse{} // Populate this array with TrackQualityResponse items

		// Populate other fields as needed using the plan data

		responsePlan := entities.SubscriptionPlan{
			ID:                       plan.ID,
			Name:                     plan.Name,
			SKU:                      plan.SKU,
			PartnerID:                plan.PartnerID,
			Amount:                   plan.Amount,
			Position:                 plan.Position,
			TaxPercentage:            plan.TaxPercentage,
			Info:                     plan.PlanInfoText,
			IsFreeSubscription:       plan.IsFreeSubscription,
			IsCancellationEnabled:    plan.IsCancellationEnabled,
			IsOneTimeSubscription:    plan.IsOneTimeSubscription,
			IsDefault:                plan.IsDefault,
			Status:                   plan.IsActive,
			SubscriptionCount:        int(plan.SubscriptionLimitPerYear),
			Duration:                 duration,
			SubscriptionLimitPerYear: plan.SubscriptionLimitPerYear,
			Currency:                 currency,
			DisplayColor:             plan.DisplayColor,
			CanRenewableWithin:       plan.CanRenewableWithin,
			FirstExpiryWarning:       plan.FirstExpiryWarning,
			SecondExpiryWarning:      plan.SecondExpiryWarning,
			ThirdExpiryWarning:       plan.ThirdExpiryWarning,
			ProductTypeResponse:      productTypes,
			TrackQualityResponse:     trackQualities,
			SubscriptionPlanDetails: entities.SubscriptionPlanDetails{
				ArtistCount:          plan.ArtistCount,
				TrackCount:           plan.TrackCount,
				ProductCount:         int(plan.ProductCount),
				MaxTracksPerProduct:  plan.MaxTracksPerProduct,
				MaxArtistsPerProduct: plan.MaxArtistsPerProduct,
			},
		}

		responseData = append(responseData, responsePlan)
	}

	successResponse := entities.SuccessResp{
		Code:    "200",
		Message: "subscription plans retrieved successfully",
		Data:    responseData,
		Meta:    meta,
	}

	ctx.JSON(http.StatusOK, successResponse)
}

// EditSubscriptionPlan handles the editing of a subscription plan.
func (subscription *SubscriptionController) EditSubscriptionPlan(ctx *gin.Context) {
	planID := ctx.Param("plan_id")
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if len(planID) < 1 {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}

	ctxt := ctx.Request.Context()
	log := logger.Log().WithContext(ctxt)
	methods := ctx.Request.Method
	method := strings.ToLower(methods)
	endpointUrl := ctx.FullPath()
	contextEndpoints, isEndpointExists := utils.GetContext[models.ResponseData](ctx, consts.ContextEndPoints)
	endpoint := utils.GetEndPoints(contextEndpoints, endpointUrl, method)

	if !isEndpointExists {
		log.Errorf("Failed to update subscription plan: invalid endpoint")
		ctx.JSON(http.StatusBadRequest, gin.H{
			"errorCode": http.StatusBadRequest,
			"message":   consts.EndpointErr,
			"errors":    nil,
		})
		return
	}

	var plan entities.SubscriptionPlan

	// Bind JSON data to the plan variable
	if err := ctx.Bind(&plan); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "Invalid JSON data",
		})
		return
	}

	// Validate the subscription plan
	fieldsMap, err := subscription.useCases.ValidateSubscriptionPlan(ctx, plan, ctx.Request.Method)
	if err != nil {
		log.Errorf("Failed to update subscription: validation error")
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	// Check if there are validation errors
	if len(fieldsMap) != 0 {
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	// Convert the plan struct to a map
	planMap := make(map[string]interface{})
	planJSON, err := json.Marshal(plan)
	if err != nil {
		log.Errorf("EditSubscriptionPlan failed, err=%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to marshal planJSON",
		})
		return
	}

	if err := json.Unmarshal(planJSON, &planMap); err != nil {
		fmt.Println("Error converting plan to map:", err)
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	// Call the EditSubscriptionPlan method with planID and planMap
	if err := subscription.useCases.EditSubscriptionPlan(ctx, planID, planMap); err != nil {
		if strings.Contains(err.Error(), "active members are using this plan") {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "Cannot edit subscription plan, active members are using this plan",
			})
			return
		}
		log.Errorf("Failed to update subscription: validation error")
		fields := utils.FieldMapping(fieldsMap)
		val, _, errorCode := utils.ParseFields(ctx, consts.ValidationErr, fields, contextError, endpoint, method)
		ctx.JSON(int(errorCode), val)
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": "subscription plan updated successfully",
	})
}

// GetSubscriptionStoreList retrieves a list of subscription stores.
func (subscription *SubscriptionController) GetSubscriptionStoreList(ctx *gin.Context) {
	planID := ctx.Param("plan_id")
	page := ctx.DefaultQuery("page", "1")
	perPage := ctx.DefaultQuery("per_page", "10")

	pageNum, _ := strconv.Atoi(page)
	perPageNum, _ := strconv.Atoi(perPage)
	log := logger.Log().WithContext(ctx)
	stores, totalCount, err := subscription.useCases.GetSubscriptionStores(ctx, planID, pageNum, perPageNum)
	if err != nil {
		log.Errorf("GetSubscriptionStoreList failed, err=%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to fetch subscription stores",
		})
		return
	}

	meta := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(perPageNum),
		CurrentPage: int32(pageNum),
		Prev:        int32(pageNum - 1),
		Next:        int32(pageNum + 1),
	}

	successResponse := entities.SuccessResp{
		Code:    "200",
		Message: "subscription stores listed successfully",
		Meta:    meta,
		Data:    stores,
	}

	ctx.JSON(http.StatusOK, successResponse)
}

// GetSubscriptionCountryList retrieves a list of subscription countries.
func (subscription *SubscriptionController) GetSubscriptionCountryList(ctx *gin.Context) {
	planID := ctx.Param("plan_id")
	page := ctx.DefaultQuery("page", "1")
	perPage := ctx.DefaultQuery("per_page", "10")
	log := logger.Log().WithContext(ctx)

	pageNum, _ := strconv.Atoi(page)
	perPageNum, _ := strconv.Atoi(perPage)

	countries, totalCount, err := subscription.useCases.GetSubscriptionCountries(ctx, planID, pageNum, perPageNum)
	if err != nil {
		log.Errorf("GetSubscriptionCountryList failed, err=%s", err.Error())
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": "Failed to fetch subscription countries",
		})
		return
	}

	meta := entities.MetaData{
		Total:       int64(totalCount),
		PerPage:     int32(perPageNum),
		CurrentPage: int32(pageNum),
		Prev:        int32(pageNum - 1),
		Next:        int32(pageNum + 1),
	}

	successResponse := entities.SuccessResp{
		Code:    "200",
		Message: "subscription countries listed successfully",
		Meta:    meta,
		Data:    countries,
	}

	ctx.JSON(http.StatusOK, successResponse)
}

// DeleteSubscriptionPlan handles the deletion of a subscription plan.
func (subscription *SubscriptionController) DeleteSubscriptionPlan(ctx *gin.Context) {
	planID := ctx.Param("plan_id")
	//todo ------- need to change with custom id after implementing auth middleware
	partnerID := constant.PartnerID
	log := logger.Log().WithContext(ctx)
	contextError, _ := utils.GetContext[map[string]interface{}](ctx, consts.ContextErrorResponses)
	if len(planID) < 1 {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"error": "id is missing",
		})
		return
	}

	operation, err := subscription.useCases.DeleteSubscriptionPlan(ctx, planID, partnerID)
	if err != nil {
		log.Errorf("DeleteSubscriptionPlan failed, err=%s", err.Error())
		val, _, errorCode := utils.ParseFields(ctx, consts.InternalServerErr,
			"", contextError, "", "")
		ctx.JSON(int(errorCode), val)
		return
	}

	successMessage := fmt.Sprintf("subscription plan %s successfully", operation)
	ctx.JSON(http.StatusOK, gin.H{
		"message": successMessage,
	})
}
