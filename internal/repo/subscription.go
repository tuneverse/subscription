package repo

import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"subscription/internal/consts"
	"subscription/internal/entities"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/lib/pq"
	"gitlab.com/tuneverse/toolkit/core/logger"
)

// SubscriptionRepo represents a repository for subscription-related data.
type SubscriptionRepo struct {
	db *sql.DB
}

// SubscriptionRepoImply is an interface for the SubscriptionRepo.
type SubscriptionRepoImply interface {
	// HandleSubscriptionCheckout performs the necessary database operations for handling a subscription checkout.
	// It takes a context, memberID, and checkoutData as parameters, and returns an error if any.
	HandleSubscriptionCheckout(ctx context.Context, memberID uuid.UUID, checkoutData entities.CheckoutSubscription) error
	// HandleSubscriptionRenewal handles the renewal of a subscription for a member.
	HandleSubscriptionRenewal(ctx context.Context, memberID uuid.UUID, checkoutData entities.SubscriptionRenewal) error
	//HandleSubscriptionCancellation, It takes a context, memberID, and checkoutData as parameters, and returns an error if any.
	HandleSubscriptionCancellation(ctx context.Context, memberID uuid.UUID, checkoutData entities.CancelSubscription) error
	// IsFreeSubscription checks if a subscription is free or not.
	IsFreeSubscription(ctx context.Context, subscriptionID string) (bool, error)
	//IsMemberExists checks whether the member is exist or not
	IsMemberExists(context.Context, uuid.UUID) (bool, error)
	//SubscriptionProductSwitch allow member to switch products between subscriptions
	SubscriptionProductSwitch(context.Context, uuid.UUID, entities.SwitchSubscriptions) (map[string][]string, error)
	//ViewAllSubscriptions list all the subscriptions purchased by the member
	ViewAllSubscriptions(context.Context, uuid.UUID, entities.ReqParams) ([]entities.ListAllSubscriptions, error)
	//GetSubscriptionRecordCount returns the total count of subscriptions purchased by the member
	GetSubscriptionRecordCount(context.Context, uuid.UUID) (int64, error)

	GetSubscription() []entities.Subscription
	CreateSubscriptionDuration(context.Context, entities.SubscriptionDuration) error
	GetAllSubscriptionDuration(context.Context, string) ([]entities.SubscriptionDuration, error)
	EditSubscriptionDuration(context.Context, int64, map[string]interface{}) error
	GetSubscriptionDurationsByID(context.Context, int64) (entities.SubscriptionDuration, error)
	DeleteSubscriptionDuration(context.Context, int64) error
	GetSubscriptionDurationByID(ctx context.Context, durationID int64) (entities.SubscriptionDuration, error)
	CreateSubscriptionPlanTransaction(context.Context, entities.SubscriptionPlan) error
	VerifyPartnerProductType(context.Context, string, []int64) error
	VerifyPartnerTrackQuality(context.Context, string, []int64) error
	EditSubscriptionPlanStores(context.Context, string, []int64) error
	EditSubscriptionPlanCountry(context.Context, string, []int64) error
	VerifyPartnerStores(context.Context, string, []string) ([]int64, error)
	GetCountriesID(ctx context.Context, countryIsoCodes []string) ([]int64, error)
	GetByID(ctx context.Context, entity entities.Entity) (int64, error)
	GetCurrencyID(ctx context.Context, iso string) (int64, error)
	GetAllSubscriptionPlans(ctx context.Context, subQuery string, partnerID string) ([]entities.SubscriptionPlan, error)
	GetSubscriptionPlanByID(ctx context.Context, planID string) (entities.SubscriptionPlan, error)
	EditSubscriptionPlan(ctx context.Context, planID string, data map[string]interface{}) error
	RemoveExistingProductTypes(ctx context.Context, subscriptionID string) error
	RemoveExistingTrackQualities(ctx context.Context, subscriptionID string) error
	InsertNewProductTypes(ctx context.Context, subscriptionID string, productTypes []int) error
	InsertNewTrackQualities(ctx context.Context, subscriptionID string, trackQualities []int) error
	GetCurrencyByID(ctx context.Context, currencyID int64) (entities.CurrencyResponse, error)
	GetSubscriptionStores(ctx context.Context, subscriptionPlanID string, page, perPage int) ([]entities.SubscriptionStoreResponse, int, error)
	GetSubscriptionCountries(ctx context.Context, subscriptionPlanID string, page, perPage int) ([]entities.SubscriptionCountryResponse, int, error)
	GetTotalSubscriptionDurationCount(ctx context.Context) (int64, error)
	DeleteSubscriptionPlan(ctx context.Context, subscriptionPlanID string, userID string) (string, error)
	CheckSubscriptionExistenceAndStatus(ctx *gin.Context, subscriptionID string) (exists bool, isActive bool, err error)
	CheckIfMemberSubscribedToFreePlan(ctx *gin.Context, memberID uuid.UUID, subscriptionID string) (bool, error)
	CheckIfPayoutGatewayExists(ctx *gin.Context, paymentGatewayID int) (bool, error)
	HasSubscribedToOneTimePlan(ctx *gin.Context, memberID uuid.UUID, subscriptionID string) (bool, error)
	GetSubscriptionStatusName(ctx *gin.Context, subscriptionID string) (string, error)
	IsMemberSubscribedToPlan(ctx *gin.Context, memberID uuid.UUID, subscriptionID string) (bool, error)
	CheckCancellationEnabled(ctx *gin.Context, subscriptionID string) (bool, error)
}

// NewSubscriptionRepo creates a new SubscriptionRepoImply.
func NewSubscriptionRepo(db *sql.DB) SubscriptionRepoImply {
	return &SubscriptionRepo{db: db}
}

// IsFreeSubscription checks if a subscription is free.
// Parameters:
//   - ctx (context.Context): The context for the database operation.
//   - subscriptionID (string): The unique identifier of the subscription to check.
//
// Returns:
//   - (bool): True if the subscription is free, false otherwise.
//   - (error): An error if the operation fails.
func (subscription *SubscriptionRepo) IsFreeSubscription(ctx context.Context, subscriptionID string) (bool, error) {
	var isFreeSubscription bool
	var subscriptionExists bool

	// Query to check if the subscription exists and if it's free.
	err := subscription.db.QueryRowContext(ctx, `
				SELECT EXISTS (SELECT 1 FROM subscription_plan WHERE id = $1) AS subscription_exists,
				is_free_subscription
				FROM subscription_plan
				WHERE id = $1
			`, subscriptionID).Scan(&subscriptionExists, &isFreeSubscription)

	if !subscriptionExists {
		return false, fmt.Errorf("subscription does not exist")
	}

	if err != nil {
		return false, err
	}

	return isFreeSubscription, nil
}

// HandleSubscriptionCheckout handles the checkout process for a subscription.
// It performs the following steps:
//  1. Checks if the provided SubscriptionID and PaymentGatewayID exist in their respective tables.
//  2. Queries the currency_id associated with the subscription plan.
//  3. Fetches subscription duration details.
//  4. Calculates the expiration date based on the subscription duration.
//  5. Inserts a new member_subscription record.
//  6. Inserts data into the member_payout_gateway table.
//
// Parameters:
//   - ctx (context.Context): The context for the database operations.
//   - memberID (uuid.UUID): The unique identifier of the member.
//   - checkoutData (entities.CheckoutSubscription): The checkout data, including SubscriptionID and PaymentGatewayID.
//
// Returns:
//   - error: An error if any database operation fails.
func (subscription *SubscriptionRepo) HandleSubscriptionCheckout(ctx context.Context, memberID uuid.UUID, checkoutData entities.CheckoutSubscription) error {
	// Start a transaction.
	tx, err := subscription.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		}
	}()

	var currencyID, subscriptionDurationValue int

	// Query to fetch currency_id
	row := subscription.db.QueryRowContext(ctx, `
				SELECT currency_id
				FROM subscription_plan
				WHERE id = $1
			`, checkoutData.SubscriptionID)

	if err := row.Scan(&currencyID); err != nil {
		return err
	}
	// Fetch the subscription duration value from the database
	query := `
			SELECT sd.value
			FROM public.subscription_duration AS sd
			WHERE sd.id = (
				SELECT sp.subscription_duration_id
				FROM public.subscription_plan AS sp
				WHERE sp.id = $1
			);	`
	err = subscription.db.QueryRowContext(ctx, query, checkoutData.SubscriptionID).Scan(&subscriptionDurationValue)
	if err != nil {
		return err
	}

	// Calculate the expiration date
	expirationDate := time.Now().Add(time.Duration(subscriptionDurationValue) * 24 * time.Hour)
	combinedQuery := `
    		INSERT INTO member_subscription 
			(member_id, subscription_id, expiration_date, member_subscription_status_id, custom_name)
    		VALUES 
			($1, $2, $3, (SELECT id FROM member_subscription_status WHERE name = 'active'), $4)
    		RETURNING member_subscription_status_id
		`

	// Execute the combined query within the transaction.
	_, err = tx.ExecContext(ctx, combinedQuery, memberID, checkoutData.SubscriptionID, expirationDate, checkoutData.CustomName)
	if err != nil {
		return err
	}

	_, err = subscription.db.ExecContext(ctx, `
					INSERT INTO member_payout_gateway (member_id, payment_gateway_id, currency_id, payment_details)
					SELECT $1, $2, sp.currency_id, 
					('{"payment_amount": ' || sp.amount || ', "tax_percentage": ' || sp.tax_percentage || ' }')::jsonb
					FROM subscription_plan sp
					WHERE sp.id = $3
				`, memberID, checkoutData.PaymentGatewayID, checkoutData.SubscriptionID)

	if err != nil {
		return err
	}

	// If all the queries succeed, commit the transaction.
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

// IsMemberExists function to check whether the member exists or not
func (subscription *SubscriptionRepo) IsMemberExists(ctx context.Context, memberID uuid.UUID) (bool, error) {

	var exists int
	isMemberExistsQ := `SELECT 1 FROM member WHERE id = $1`
	row := subscription.db.QueryRowContext(ctx, isMemberExistsQ, memberID)
	err := row.Scan(&exists)

	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// SubscriptionProductSwitch function to switch products between subscriptions
func (subscription *SubscriptionRepo) SubscriptionProductSwitch(ctx context.Context, memberID uuid.UUID, data entities.SwitchSubscriptions) (map[string][]string, error) {

	var (
		productCount, maxProductCount, newArtistCount, newTrackCount, newMaxTracksPerProduct, newMaxArtistsPerProduct,
		currentArtistCount, currentTrackCount, currentMaxTracksPerProduct, currentMaxArtistsPerProduct int
		newSubscriptionStatus        string
		newIsActive, currentIsActive bool
	)

	// Check if given memberID exists
	memberExists, err := subscription.IsMemberExists(ctx, memberID)
	if err != nil {
		return nil, err
	}
	if !memberExists {
		return nil, errors.New("member not found") // Return custom error message
	}

	// Start a transaction.
	tx, err := subscription.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			err := tx.Rollback()
			if err != nil {
				return
			}
		}
	}()

	// Step 1: Check if the product_count is full for the new subscription plan.
	err = subscription.db.QueryRowContext(ctx, `
		SELECT max(product_count)as maxProductCount FROM subscription_plan WHERE id = $1
		`, data.NewSubscriptionID).Scan(&maxProductCount)

	if err != nil {
		return nil, err
	}

	err = subscription.db.QueryRowContext(ctx, `
		SELECT count(id)as productCount FROM product WHERE member_subscription_id = $1
		`, data.NewSubscriptionID).Scan(&productCount)

	if err != nil {
		return nil, err
	}
	if productCount == maxProductCount {
		return nil, fmt.Errorf("no more products can be added to this plan")
	}

	// Step 2: Check if the new subscription plan is active.
	err = subscription.db.QueryRowContext(ctx, `
		SELECT name
		FROM member_subscription_status
		WHERE id = (
			SELECT member_subscription_status_id
			FROM member_subscription
			WHERE subscription_id = $1
		)
	`, data.NewSubscriptionID).Scan(&newSubscriptionStatus)

	if err != nil {
		return nil, err
	}
	if newSubscriptionStatus != "active" {
		return nil, fmt.Errorf("new subscription plan is not active")
	}

	// Step 3: Compare fields between the new and current subscription plans.
	err = subscription.db.QueryRowContext(ctx, `
		SELECT sp2.artist_count, sp2.track_count, sp2.max_tracks_per_product, sp2.max_artists_per_product, sp2.is_active
		FROM subscription_plan AS sp2
		WHERE sp2.id = $1
		`, data.CurrentSubscriptionID).Scan(&currentArtistCount, &currentTrackCount, &currentMaxTracksPerProduct, &currentMaxArtistsPerProduct, &currentIsActive)

	if err != nil {
		return nil, err
	}

	err = subscription.db.QueryRowContext(ctx, `
        SELECT sp1.artist_count, sp1.track_count, sp1.max_tracks_per_product, sp1.max_artists_per_product, sp1.is_active
        FROM subscription_plan AS sp1
        WHERE sp1.id = $1
		`, data.NewSubscriptionID).Scan(&newArtistCount, &newTrackCount, &newMaxTracksPerProduct, &newMaxArtistsPerProduct, &newIsActive)

	if err != nil {
		return nil, err
	}
	if !newIsActive {
		return nil, fmt.Errorf("plan you are trying to switch to is not active")
	}

	if (newArtistCount >= currentArtistCount) && (newTrackCount >= currentTrackCount) &&
		(newMaxTracksPerProduct >= currentMaxTracksPerProduct) && (newMaxArtistsPerProduct >= currentMaxArtistsPerProduct) {

		// Step 4: Update the member_subscription_id in the product table.
		_, err = subscription.db.ExecContext(ctx, `
		UPDATE product
		SET member_subscription_id = (SELECT id FROM member_subscription WHERE subscription_id = $2)
		WHERE id = $1
		`, data.ProductReferenceID, data.NewSubscriptionID)

		if err != nil {
			return nil, err
		}

		// Commit the transaction if everything succeeds.
		if err = tx.Commit(); err != nil {
			return nil, err
		}
		return nil, nil
	}
	return nil, fmt.Errorf("product criteria do not fit the new plan")
}

// ViewAllSubscriptions returns list of all member subscriptions
func (subscription *SubscriptionRepo) ViewAllSubscriptions(ctx context.Context, memberID uuid.UUID, reqParam entities.ReqParams) ([]entities.ListAllSubscriptions, error) {

	// Check if given memberID exists
	memberExists, err := subscription.IsMemberExists(ctx, memberID)
	if err != nil {
		return nil, err
	}
	if !memberExists {
		return nil, errors.New("member not found") // Return custom error message
	}

	var (
		data          entities.ListAllSubscriptions
		subscriptions []entities.ListAllSubscriptions
	)

	query := `
		SELECT
    		sp.name,
    		mss.name,
    		ms.expiration_date,
    		sp.artist_count,
    		sp.max_artists_per_product,
    		sp.track_count,
    		sp.max_tracks_per_product,
    		COUNT(p.id) AS product_count_for_member,
			sp.product_count
		FROM
    		subscription_plan AS sp
		INNER JOIN
    		member_subscription AS ms ON sp.id = ms.subscription_id
		LEFT JOIN
			member_subscription_status AS mss ON ms.member_subscription_status_id = mss.id
		LEFT JOIN
    		product AS p ON ms.id = p.member_subscription_id
		WHERE
			ms.member_id = $1
		GROUP BY
    		sp.name,
    		mss.name,
    		ms.expiration_date,
    		sp.artist_count,
    		sp.max_artists_per_product,
    		sp.track_count,
    		sp.max_tracks_per_product,
    		sp.product_count 
	`
	//Checking page and limit not equal to zero and calculate offset
	if reqParam.Page != 0 && reqParam.Limit != 0 {
		offset := (reqParam.Page - 1) * reqParam.Limit
		query += fmt.Sprintf(" LIMIT %d OFFSET %d", reqParam.Limit, offset)
	}

	rows, err := subscription.db.QueryContext(ctx, query, memberID)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		err = rows.Scan(
			&data.Name,
			&data.SubscriptionDetails.Status,
			&data.SubscriptionDetails.ExpirationDate,
			&data.UsageDetails.ArtistsAdded.Added,
			&data.UsageDetails.ArtistsAdded.Total,
			&data.UsageDetails.TracksAdded.Added,
			&data.UsageDetails.TracksAdded.Total,
			&data.UsageDetails.ProductsAdded.Added,
			&data.UsageDetails.ProductsAdded.Total,
		)

		if err != nil {
			return nil, err
		}
		//append scanned values into subscriptions
		subscriptions = append(subscriptions, data)
	}
	return subscriptions, nil
}

// GetSubscriptionRecordCount function is used to calculate and return total count of subscriptions
func (subscription *SubscriptionRepo) GetSubscriptionRecordCount(ctx context.Context, memberID uuid.UUID) (int64, error) {

	var totalCount int64

	query := `
		SELECT COUNT(subscription_id) AS total_count
		FROM member_subscription 
		WHERE member_subscription.member_id = member_id;
	`
	row := subscription.db.QueryRowContext(ctx, query)

	if err := row.Scan(&totalCount); err != nil {
		logger.Log().WithContext(ctx).Errorf("GetSubscriptionRecordCount failed, QueryRowContext failed, err=%s", err.Error())
		return 0, err
	}
	return totalCount, nil
}

// CreateSubscriptionPlanTransaction creates a subscription plan transaction.
func (subscription *SubscriptionRepo) CreateSubscriptionPlanTransaction(ctx context.Context, subscriptionPlan entities.SubscriptionPlan) error {
	log := logger.Log().WithContext(ctx)
	// Start a transaction
	tx, err := subscription.db.BeginTx(ctx, nil)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed,tansaction BeginTx() failed, err=%s", err.Error())
		return err
	}
	// Ensure the transaction is rolled back if any error occurs
	defer func() error {
		if err != nil {
			log.Errorf("CreateSubscriptionPlanTransaction failed, err=%s", err.Error())
			err = tx.Rollback()
			if err != nil {
				log.Errorf("CreateSubscriptionPlanTransaction failed, transaction rollback() failed, err=%s", err.Error())
				return err
			}
			return err
		}
		err = tx.Commit()
		if err != nil {
			log.Errorf("CreateSubscriptionPlanTransaction failed, transaction commit() failed, err=%s", err.Error())
			return err
		}
		return nil
	}()
	var subscriptionPLanID string
	// Use the same transaction for all your queries
	currentTime := time.Now()

	err = tx.QueryRowContext(ctx, `INSERT INTO subscription_plan (
		name, currency_id, amount, subscription_duration_id, artist_count,
		track_count, max_tracks_per_product, max_artists_per_product, is_active,
		partner_id, tax_percentage, sku, display_color, is_free_subscription,
		is_cancellation_enabled, can_renewable_within, first_expiry_warning,
		second_expiry_warning, third_expiry_warning, product_count, plan_info_text,
		position, is_default, subscription_for, is_one_time_subscription,
		subscription_limit_per_year, is_deleted, created_on, updated_on
	) WITH c AS (
		SELECT id
		FROM currency
		WHERE iso=$2
	) SELECT
		$1, c.id, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16,
		$17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28,$29
	FROM c RETURNING id`,
		subscriptionPlan.Name,
		subscriptionPlan.CurrencyISO,
		subscriptionPlan.Amount,
		subscriptionPlan.SubscriptionDurationID,
		subscriptionPlan.SubscriptionPlanDetails.ArtistCount,
		subscriptionPlan.SubscriptionPlanDetails.TrackCount,
		subscriptionPlan.SubscriptionPlanDetails.MaxTracksPerProduct,
		subscriptionPlan.SubscriptionPlanDetails.MaxArtistsPerProduct,
		subscriptionPlan.IsActive,
		subscriptionPlan.PartnerID,
		subscriptionPlan.TaxPercentage,
		subscriptionPlan.SKU,
		subscriptionPlan.DisplayColor,
		subscriptionPlan.IsFreeSubscription,
		subscriptionPlan.IsCancellationEnabled,
		subscriptionPlan.CanRenewableWithin,
		subscriptionPlan.FirstExpiryWarning,
		subscriptionPlan.SecondExpiryWarning,
		subscriptionPlan.ThirdExpiryWarning,
		subscriptionPlan.SubscriptionPlanDetails.ProductCount,
		subscriptionPlan.PlanInfoText,
		subscriptionPlan.Position,
		subscriptionPlan.IsDefault,
		subscriptionPlan.SubscriptionFor,
		subscriptionPlan.IsOneTimeSubscription,
		subscriptionPlan.SubscriptionLimitPerYear,
		subscriptionPlan.IsDeleted,
		currentTime,
		currentTime).Scan(&subscriptionPLanID)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, QueryRowContext() failed, err=%s", err.Error())
		return err
	}
	err = CreateSubscriptionPlanProductType(ctx, tx, subscriptionPLanID, subscriptionPlan.ProductTypes)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, CreateSubscriptionPlanProductType failed, err=%s", err.Error())
		return err
	}

	err = CreateSubscriptionPlanTrackQuality(ctx, tx, subscriptionPLanID, subscriptionPlan.TrackQuality)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, CreateSubscriptionPlanTrackQuality failed, err=%s", err.Error())
		return err
	}

	partnerStores, err := GetPartnerStores(ctx, tx, subscriptionPlan.PartnerID)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, GetPartnerStores failed, err=%s", err.Error())
		return err
	}
	countryIDs, err := GetAllCountries(ctx, tx)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, GetAllCountries failed, err=%s", err.Error())
		return err
	}
	err = CreateSubscriptionPlanStores(ctx, tx, subscriptionPLanID, partnerStores)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, CreateSubscriptionPlanStores failed, err=%s", err.Error())
		return err
	}

	err = CreateSubscriptionPlanCountries(ctx, tx, subscriptionPLanID, countryIDs)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTransaction failed, CreateSubscriptionPlanCountries failed, err=%s", err.Error())
		return err
	}

	return nil
}

// CreateSubscriptionPlanProductType creates a subscription plan product type.
func CreateSubscriptionPlanProductType(ctx context.Context, tx *sql.Tx, subscriptionPLanID string, subscriptionProductTypes []int64) error {
	log := logger.Log().WithContext(ctx)
	if len(subscriptionProductTypes) == 0 {
		return nil
	}

	query := `INSERT INTO subscription_plan_product_type (subscription_id, product_type_id) VALUES `
	values := []interface{}{}

	for i, st := range subscriptionProductTypes {
		query += fmt.Sprintf("($%d, $%d),", i*2+1, i*2+2)
		values = append(values, subscriptionPLanID, st)
	}

	query = strings.TrimSuffix(query, ",") // Remove the trailing comma

	_, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanProductType failed,  ExecContext() failed, err=%s", err.Error())
		return err
	}
	return nil
}

// CreateSubscriptionPlanTrackQuality creates a subscription plan track quality.
func CreateSubscriptionPlanTrackQuality(ctx context.Context, tx *sql.Tx, subscriptionPLanID string, subscriptionTrackQuality []int64) error {
	log := logger.Log().WithContext(ctx)
	if len(subscriptionTrackQuality) == 0 {
		return nil
	}

	query := `INSERT INTO subscription_plan_track_quality (subscription_id, track_file_quality_id) VALUES `
	values := []interface{}{}

	for i, st := range subscriptionTrackQuality {
		query += fmt.Sprintf("($%d, $%d),", i*2+1, i*2+2)
		values = append(values, subscriptionPLanID, st)
	}

	query = strings.TrimSuffix(query, ",") // Remove the trailing comma

	_, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanTrackQuality failed,  ExecContext() failed, err=%s", err.Error())
		return err
	}
	return nil
}

// VerifyPartnerProductType verifies the partner product type.
func (subscription *SubscriptionRepo) VerifyPartnerProductType(ctx context.Context, partnerID string, subscriptionPlanProductTypes []int64) error {
	log := logger.Log().WithContext(ctx)
	placeholders := make([]string, len(subscriptionPlanProductTypes))
	for i, productType := range subscriptionPlanProductTypes {
		placeholders[i] = fmt.Sprintf("%d", productType)
	}

	query := fmt.Sprintf(`
        SELECT
            EXISTS (
                SELECT 1
                FROM (VALUES %s) AS input(product_type_id)
                WHERE NOT EXISTS (
                    SELECT 1
                    FROM partner_product_type
                    WHERE partner_id = $1
                    AND product_type_id = input.product_type_id
                )
            ) AS are_missing_product_types;
    `, fmt.Sprintf("(%s)", strings.Join(placeholders, "), (")))

	// Prepare the query arguments
	args := append([]interface{}{partnerID})

	var areMissingProductTypes bool
	err := subscription.db.QueryRowContext(ctx, query, args...).Scan(&areMissingProductTypes)
	if err != nil {
		log.Errorf("VerifyPartnerProductType failed,  QueryRowContext() failed, err=%s", err.Error())
		return err
	}

	if areMissingProductTypes {
		errMsg := fmt.Errorf("some specified product types are missing for the partner")
		log.Errorf("VerifyPartnerProductType failed, err=%s", errMsg.Error())
		return errMsg
	}

	return nil
}

// VerifyPartnerTrackQuality verifies the partner track quality.
func (subscription *SubscriptionRepo) VerifyPartnerTrackQuality(ctx context.Context, partnerID string, subscriptionPlanTrackQuality []int64) error {
	log := logger.Log().WithContext(ctx)
	placeholders := make([]string, len(subscriptionPlanTrackQuality))
	for i, trackFile := range subscriptionPlanTrackQuality {
		placeholders[i] = fmt.Sprintf("%d", trackFile)
	}

	query := fmt.Sprintf(`
        SELECT
            EXISTS (
                SELECT 1
                FROM (VALUES %s) AS input(track_file_quality_id)
                WHERE NOT EXISTS (
                    SELECT 1
                    FROM partner_track_file_quality
                    WHERE partner_id = $1
                    AND track_file_quality_id = input.track_file_quality_id
                )
            ) AS are_missing_track_file_quality;
    `, fmt.Sprintf("(%s)", strings.Join(placeholders, "), (")))

	// Prepare the query arguments
	args := append([]interface{}{partnerID})

	var areMissingTrackFileQuality bool
	err := subscription.db.QueryRowContext(ctx, query, args...).Scan(&areMissingTrackFileQuality)
	if err != nil {
		log.Errorf("VerifyPartnerTrackQuality failed, QueryRowContext() failed, err=%s", err.Error())
		return err
	}

	if areMissingTrackFileQuality {
		errMsg := fmt.Errorf("some specified track file qualities are missing for the partner")
		log.Errorf("VerifyPartnerTrackQuality failed, err=%s", errMsg.Error())
		return errMsg
	}

	return nil
}

// GetPartnerStores gets partner stores.
func GetPartnerStores(ctx context.Context, tx *sql.Tx, partnerID uuid.UUID) ([]int64, error) {
	log := logger.Log().WithContext(ctx)
	var storeIDs []int64
	query := `SELECT id FROM partner_store WHERE partner_id=$1 AND is_active=$2;`

	rows, err := tx.QueryContext(ctx, query, partnerID, true)
	if err != nil {
		log.Errorf("GetPartnerStores failed, QueryContext() failed, err=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var storeID int64
		if err := rows.Scan(&storeID); err != nil {
			log.Errorf("GetPartnerStores failed, Scan() failed, err=%s", err.Error())
			return nil, err
		}
		storeIDs = append(storeIDs, storeID)
	}

	if err := rows.Err(); err != nil {
		log.Errorf("GetPartnerStores failed, rows.Err() failed, err=%s", err.Error())
		return nil, err
	}
	return storeIDs, nil
}

// CreateSubscriptionPlanStores creates subscription plan stores.
func CreateSubscriptionPlanStores(ctx context.Context, tx *sql.Tx, subscriptionPLanID string, partnerStores []int64) error {
	log := logger.Log().WithContext(ctx)
	if len(partnerStores) == 0 {
		return nil
	}

	query := `INSERT INTO subscription_store (subscription_id, partner_store_id) VALUES `
	values := []interface{}{}

	for i, st := range partnerStores {
		query += fmt.Sprintf("($%d, $%d),", i*2+1, i*2+2)
		values = append(values, subscriptionPLanID, st)
	}

	query = strings.TrimSuffix(query, ",") // Remove the trailing comma

	_, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanStores failed, ExecContext() failed, err=%s", err.Error())
		return err
	}
	return nil
}

// GetAllCountries gets all countries.
func GetAllCountries(ctx context.Context, tx *sql.Tx) ([]int64, error) {
	log := logger.Log().WithContext(ctx)
	var countryIDs []int64
	query := `SELECT id FROM country`

	rows, err := tx.QueryContext(ctx, query)
	if err != nil {
		log.Errorf("GetAllCountries failed, QueryContext() failed, err=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var countryID int64
		if err := rows.Scan(&countryID); err != nil {
			log.Errorf("GetAllCountries failed, rows.Scan() failed, err=%s", err.Error())
			return nil, err
		}
		countryIDs = append(countryIDs, countryID)
	}

	if err := rows.Err(); err != nil {
		log.Errorf("GetAllCountries failed, rows.Err() failed, err=%s", err.Error())
		return nil, err
	}
	return countryIDs, nil
}

// CreateSubscriptionPlanCountries creates subscription plan countries.
func CreateSubscriptionPlanCountries(ctx context.Context, tx *sql.Tx, subscriptionPLanID string, countryIDs []int64) error {
	log := logger.Log().WithContext(ctx)
	if len(countryIDs) == 0 {
		return nil
	}

	query := `INSERT INTO subscription_country (subscription_id, country_id) VALUES `
	values := []interface{}{}

	for i, st := range countryIDs {
		query += fmt.Sprintf("($%d, $%d),", i*2+1, i*2+2)
		values = append(values, subscriptionPLanID, st)
	}

	query = strings.TrimSuffix(query, ",") // Remove the trailing comma

	_, err := tx.ExecContext(ctx, query, values...)
	if err != nil {
		log.Errorf("CreateSubscriptionPlanCountries failed, ExecContext() failed, err=%s", err.Error())
		return err
	}
	return nil
}

// VerifyPartnerStores verifies partner stores.
func (subscription *SubscriptionRepo) VerifyPartnerStores(ctx context.Context, partnerID string, stores []string) ([]int64, error) {
	log := logger.Log().WithContext(ctx)
	query := "SELECT id,store_id FROM partner_store WHERE store_id = ANY($1) AND partner_id = $2"

	// Convert the storeIDs slice to a PostgreSQL array
	storeIDArray := pq.Array(stores)

	rows, err := subscription.db.Query(query, storeIDArray, partnerID)
	if err != nil {
		log.Errorf("VerifyPartnerStores failed, Query() failed, err=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	var result []int64
	foundStoreIDs := make(map[string]bool) // To track which store_ids were found

	// Initialize the foundStoreIDs map
	for _, storeID := range stores {
		foundStoreIDs[storeID] = false
	}

	for rows.Next() {
		var id int64
		var storeID string
		if err := rows.Scan(&id, &storeID); err != nil {
			log.Errorf("VerifyPartnerStores failed, rows.Scan() failed, err=%s", err.Error())
			return nil, err
		}
		result = append(result, id)
		// Mark the store_id as found
		foundStoreIDs[storeID] = true
	}

	if err := rows.Err(); err != nil {
		log.Errorf("VerifyPartnerStores failed, rows.Err() failed, err=%s", err.Error())
		return nil, err
	}

	// Check if any store_id was not found
	for _, storeID := range stores {
		if !foundStoreIDs[storeID] {
			errMsg := fmt.Errorf("store_id %s not found for the partner", storeID)
			log.Errorf("VerifyPartnerStores failed, err=%s", errMsg.Error())
			return nil, errMsg
		}
	}

	return result, nil
}

// EditSubscriptionPlanStores edits subscription plan stores.
func (subscription *SubscriptionRepo) EditSubscriptionPlanStores(ctx context.Context, subscriptionPlanID string, partnerStores []int64) error {
	tx, err := subscription.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	// Ensure the transaction is rolled back if any error occurs
	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}
		_ = tx.Commit()
	}()

	err = DeleteSubscriptionPlanStores(ctx, tx, subscriptionPlanID)
	if err != nil {
		return err
	}
	err = CreateSubscriptionPlanStores(ctx, tx, subscriptionPlanID, partnerStores)
	if err != nil {
		return err
	}
	return nil
}

// DeleteSubscriptionPlanStores deletes subscription plan stores.
func DeleteSubscriptionPlanStores(ctx context.Context, tx *sql.Tx, subscriptionPLanID string) error {
	query := `DELETE FROM subscription_store WHERE subscription_id=$1 `
	_, err := tx.ExecContext(ctx, query, subscriptionPLanID)
	if err != nil {
		return err
	}
	return nil
}

// EditSubscriptionPlanCountry edits subscription plan country.
func (subscription *SubscriptionRepo) EditSubscriptionPlanCountry(ctx context.Context, subscriptionPLanID string, countries []int64) error {
	tx, err := subscription.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	// Ensure the transaction is rolled back if any error occurs
	defer func() {
		if err != nil {
			_ = tx.Rollback()
			return
		}
		_ = tx.Commit()
	}()

	err = DeleteSubscriptionPlanCountry(ctx, tx, subscriptionPLanID)
	if err != nil {
		return err
	}
	err = CreateSubscriptionPlanCountries(ctx, tx, subscriptionPLanID, countries)
	if err != nil {
		return err
	}
	return nil
}

// DeleteSubscriptionPlanCountry deletes subscription plan country.
func DeleteSubscriptionPlanCountry(ctx context.Context, tx *sql.Tx, subscriptionPLanID string) error {
	query := `DELETE FROM subscription_country WHERE subscription_id=$1 `
	_, err := tx.ExecContext(ctx, query, subscriptionPLanID)
	if err != nil {
		return err
	}
	return nil
}

// GetCountriesID gets country IDs.
func (subscription *SubscriptionRepo) GetCountriesID(ctx context.Context, countryIsoCodes []string) ([]int64, error) {
	log := logger.Log().WithContext(ctx)
	query := "SELECT id,iso_three_code FROM country WHERE iso_three_code = ANY($1)"

	// Convert the countryISO slice to a PostgreSQL array
	countryIsoArray := pq.Array(countryIsoCodes)

	rows, err := subscription.db.Query(query, countryIsoArray)
	if err != nil {
		log.Errorf("GetCountriesID failed, Query() failed, err=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	var result []int64
	foundCountryISO := make(map[string]bool)

	// Initialize the foundCountryISO map
	for _, iso := range countryIsoCodes {
		foundCountryISO[iso] = false
	}

	for rows.Next() {
		var id int64
		var iso string
		if err := rows.Scan(&id, &iso); err != nil {
			log.Errorf("GetCountriesID failed, rows.Scan() failed, err=%s", err.Error())
			return nil, err
		}
		result = append(result, id)
		// Mark the iso as found
		foundCountryISO[iso] = true
	}

	if err := rows.Err(); err != nil {
		log.Errorf("GetCountriesID failed, rows.Err() failed, err=%s", err.Error())
		return nil, err
	}

	// Check if any iso was not found
	for _, iso := range countryIsoCodes {
		if !foundCountryISO[iso] {
			errMsg := fmt.Errorf("country iso %s not found", iso)
			log.Errorf("GetCountriesID failed, err=%s", errMsg.Error())
			return nil, errMsg
		}
	}
	return result, nil
}

// GetByID gets data by ID.
func (subscription *SubscriptionRepo) GetByID(ctx context.Context, entity entities.Entity) (int64, error) {
	log := logger.Log().WithContext(ctx)
	var count int64
	sqlst := `
		SELECT COUNT(*)
		FROM %s
		WHERE %s=$1;
	`
	sqlst = fmt.Sprintf(sqlst, entity.TableName, entity.ColumnName)
	err := subscription.db.QueryRowContext(ctx, sqlst, entity.Value).Scan(&count)
	if err != nil {
		log.Errorf("GetByID failed, QueryRowContext() failed err=%s", err.Error())
		return count, err
	}
	return count, err
}

// GetCurrencyID gets currency ID.
func (subscription *SubscriptionRepo) GetCurrencyID(ctx context.Context, iso string) (int64, error) {
	log := logger.Log().WithContext(ctx)
	var id int64
	sqlst := `
		SELECT id
		FROM currency
		WHERE iso=$1;
	`
	err := subscription.db.QueryRowContext(ctx, sqlst, iso).Scan(&id)
	if err != nil {
		log.Errorf("GetCurrencyID failed, QueryRowContext() failed err=%s", err.Error())
		return id, err
	}
	return id, err
}

// GetAllSubscriptionPlans gets all subscription plans.
func (subscription *SubscriptionRepo) GetAllSubscriptionPlans(ctx context.Context, subQuery string, partnerID string) ([]entities.SubscriptionPlan, error) {
	var plans []entities.SubscriptionPlan

	selectQuery := `
        SELECT DISTINCT ON (sp.id)
            sp.id, sp.name, sp.currency_id, sp.amount, sp.subscription_duration_id, 
            sp.artist_count, sp.track_count, sp.max_tracks_per_product, sp.max_artists_per_product, 
            sp.is_active, sp.partner_id, sp.tax_percentage, sp.sku, sp.display_color, 
            sp.is_free_subscription, sp.is_cancellation_enabled, sp.can_renewable_within, 
            sp.first_expiry_warning, sp.second_expiry_warning, sp.third_expiry_warning, 
            sp.product_count, sp.plan_info_text, sp.position, sp.is_default, 
            sp.subscription_for, sp.is_one_time_subscription, sp.subscription_limit_per_year, 
            sp.is_deleted, sp.created_on, sp.updated_by, sp.updated_on
        FROM 
            subscription_plan sp
        WHERE 
            sp.is_deleted = false AND sp.partner_id = $1 ` + subQuery

	rows, err := subscription.db.QueryContext(ctx, selectQuery, partnerID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var plan entities.SubscriptionPlan
		var updatedBy sql.NullString

		if err := rows.Scan(
			&plan.ID, &plan.Name, &plan.CurrencyID, &plan.Amount, &plan.SubscriptionDurationID,
			&plan.ArtistCount, &plan.TrackCount, &plan.MaxTracksPerProduct, &plan.MaxArtistsPerProduct,
			&plan.IsActive, &plan.PartnerID, &plan.TaxPercentage, &plan.SKU, &plan.DisplayColor,
			&plan.IsFreeSubscription, &plan.IsCancellationEnabled, &plan.CanRenewableWithin,
			&plan.FirstExpiryWarning, &plan.SecondExpiryWarning, &plan.ThirdExpiryWarning,
			&plan.ProductCount, &plan.PlanInfoText, &plan.Position, &plan.IsDefault,
			&plan.SubscriptionFor, &plan.IsOneTimeSubscription, &plan.SubscriptionLimitPerYear,
			&plan.IsDeleted, &plan.CreatedOn, &updatedBy, &plan.UpdatedOn,
		); err != nil {
			return nil, err
		}
		resultUUID, err := uuid.Parse(updatedBy.String)
		if err != nil {
			fmt.Println("Error parsing UUID:", err)
		}
		plan.UpdatedBy = resultUUID

		plans = append(plans, plan)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return plans, nil
}

// GetSubscriptionPlanByID gets a subscription plan by ID.
func (subscription *SubscriptionRepo) GetSubscriptionPlanByID(ctx context.Context, planID string) (entities.SubscriptionPlan, error) {
	var plan entities.SubscriptionPlan
	var updatedBy sql.NullString // Use sql.NullString for nullable string columns

	err := subscription.db.QueryRowContext(ctx,
		`SELECT 
            id, name, currency_id, amount, subscription_duration_id,
            artist_count, track_count, max_tracks_per_product, max_artists_per_product,
            is_active, partner_id, tax_percentage, sku, display_color,
            is_free_subscription, is_cancellation_enabled, can_renewable_within,
            first_expiry_warning, second_expiry_warning, third_expiry_warning,
            product_count, plan_info_text, position, is_default,
            subscription_for, is_one_time_subscription, subscription_limit_per_year,
            is_deleted, created_on, updated_by, updated_on
        FROM subscription_plan
        WHERE id = $1 AND is_deleted = false`,
		planID,
	).Scan(
		&plan.ID, &plan.Name, &plan.CurrencyID, &plan.Amount, &plan.SubscriptionDurationID,
		&plan.ArtistCount, &plan.TrackCount, &plan.MaxTracksPerProduct, &plan.MaxArtistsPerProduct,
		&plan.IsActive, &plan.PartnerID, &plan.TaxPercentage, &plan.SKU, &plan.DisplayColor,
		&plan.IsFreeSubscription, &plan.IsCancellationEnabled, &plan.CanRenewableWithin,
		&plan.FirstExpiryWarning, &plan.SecondExpiryWarning, &plan.ThirdExpiryWarning,
		&plan.ProductCount, &plan.PlanInfoText, &plan.Position, &plan.IsDefault,
		&plan.SubscriptionFor, &plan.IsOneTimeSubscription, &plan.SubscriptionLimitPerYear,
		&plan.IsDeleted, &plan.CreatedOn, &updatedBy, &plan.UpdatedOn,
	)

	if err != nil {
		return plan, err
	}

	// Convert sql.NullString to string
	resultUUID, err := uuid.Parse(updatedBy.String)
	if err != nil {
		fmt.Println("Error parsing UUID:", err)
	}
	plan.UpdatedBy = resultUUID

	return plan, nil
}

func processValue(value interface{}) interface{} {
	switch v := value.(type) {
	case map[string]interface{}:
		nestedJSON, _ := json.Marshal(v)
		return string(nestedJSON)
	case []interface{}:
		// Flatten nested slices to a comma-separated string
		var flattened []string
		for _, item := range v {
			flattened = append(flattened, fmt.Sprintf("%v", item))
		}
		return strings.Join(flattened, ", ")
	default:
		return v
	}
}

func isValidColumn(columnName string) bool {
	// Define a list of valid column names for subscription_plan table
	validColumns := []string{
		"name", "currency_id", "amount", "subscription_duration_id",
	}

	for _, validColumn := range validColumns {
		if columnName == validColumn {
			return true
		}
	}
	return false
}

// EditSubscriptionPlan edits a subscription plan.
func (subscription *SubscriptionRepo) EditSubscriptionPlan(ctx context.Context, planID string, data map[string]interface{}) error {
	if len(data) == 0 {
		return nil
	}

	// Check if there are active members using this plan
	var activeMembersCount int
	err := subscription.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM member_subscription WHERE subscription_id = $1 AND expiration_date > NOW()", planID).Scan(&activeMembersCount)
	if err != nil {
		return fmt.Errorf("failed to check active members: %w", err)
	}

	// If there are active members, return an error
	if activeMembersCount > 0 {
		return fmt.Errorf("active members are using this plan: cannot edit subscription plan")
	}

	// Extract product_type and track_quality arrays from data
	productTypeStrings, productTypeExists := data["product_type"].([]interface{})
	trackQualityStrings, trackQualityExists := data["track_quality"].([]interface{})

	// Convert product_type strings to integers
	var productTypes []int
	if productTypeExists {
		for _, str := range productTypeStrings {
			num, err := strconv.Atoi(fmt.Sprintf("%v", str))
			if err != nil {
				return fmt.Errorf("failed to convert product_type to int: %w", err)
			}
			productTypes = append(productTypes, num)
		}
	}

	// Convert track_quality strings to integers
	var trackQualities []int
	if trackQualityExists {
		for _, str := range trackQualityStrings {
			num, err := strconv.Atoi(fmt.Sprintf("%v", str))
			if err != nil {
				return fmt.Errorf("failed to convert track_quality to int: %w", err)
			}
			trackQualities = append(trackQualities, num)
		}
	}

	// Remove existing associations for product_type and track_quality
	if productTypeExists {
		err := subscription.RemoveExistingProductTypes(ctx, planID)
		if err != nil {
			return err
		}

		insertErr := subscription.InsertNewProductTypes(ctx, planID, productTypes)
		if insertErr != nil {
			return insertErr
		}
	}

	if trackQualityExists {
		err := subscription.RemoveExistingTrackQualities(ctx, planID)
		if err != nil {
			return err
		}

		insertErr := subscription.InsertNewTrackQualities(ctx, planID, trackQualities)
		if insertErr != nil {
			fmt.Println(insertErr)
			return insertErr
		}
	}

	// Construct the SQL update statement dynamically
	var updates []string
	var values []interface{}
	index := 1
	for field, value := range data {
		// Process value to handle nested structures or slices
		processedValue := processValue(value)

		// Escape reserved keywords in column names with double quotes
		if field == "default" {
			field = `"` + field + `"`
		}

		// Check if the column name is valid for the subscription_plan table
		if isValidColumn(field) {
			updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
			values = append(values, processedValue)
			index++
		}
	}

	if len(updates) == 0 {
		// No valid columns to update
		return nil
	}

	values = append(values, planID)

	stmt := fmt.Sprintf("UPDATE subscription_plan SET %s WHERE id = $%d",
		strings.Join(updates, ", "), index)

	_, err = subscription.db.ExecContext(ctx, stmt, values...)
	if err != nil {
		return fmt.Errorf("failed to update subscription plan fields: %w", err)
	}

	return nil
}

// RemoveExistingProductTypes removes existing product types.
func (subscription *SubscriptionRepo) RemoveExistingProductTypes(ctx context.Context, subscriptionID string) error {
	_, err := subscription.db.ExecContext(ctx,
		"DELETE FROM subscription_plan_product_type WHERE subscription_id = $1",
		subscriptionID,
	)
	if err != nil {
		return fmt.Errorf("failed to remove existing product type associations: %w", err)
	}
	return nil
}

// RemoveExistingTrackQualities removes existing track qualities.
func (subscription *SubscriptionRepo) RemoveExistingTrackQualities(ctx context.Context, subscriptionID string) error {
	_, err := subscription.db.ExecContext(ctx,
		"DELETE FROM subscription_plan_track_quality WHERE subscription_id = $1",
		subscriptionID,
	)
	if err != nil {
		return fmt.Errorf("failed to remove existing track quality associations: %w", err)
	}
	return nil
}

// InsertNewProductTypes inserts new product types.
func (subscription *SubscriptionRepo) InsertNewProductTypes(ctx context.Context, subscriptionID string, productTypes []int) error {
	for _, productTypeID := range productTypes {
		_, err := subscription.db.ExecContext(ctx,
			"INSERT INTO subscription_plan_product_type (subscription_id, product_type_id) VALUES ($1, $2)",
			subscriptionID, productTypeID,
		)
		if err != nil {
			return fmt.Errorf("failed to insert new product type association: %w", err)
		}
		fmt.Printf("Inserted product type association for subscription %s and product type ID %d\n", subscriptionID, productTypeID)
	}
	return nil
}

// InsertNewTrackQualities inserts new track qualities.
func (subscription *SubscriptionRepo) InsertNewTrackQualities(ctx context.Context, subscriptionID string, trackQualities []int) error {
	for _, trackQualityID := range trackQualities {
		_, err := subscription.db.ExecContext(ctx,
			"INSERT INTO subscription_plan_track_quality (subscription_id, track_file_quality_id) VALUES ($1, $2)",
			subscriptionID, trackQualityID,
		)
		if err != nil {
			return fmt.Errorf("failed to insert new track quality association: %w", err)
		}
		fmt.Printf("Inserted track quality association for subscription %s and track quality ID %d\n", subscriptionID, trackQualityID)
	}
	return nil
}

// GetCurrencyByID gets currency by ID.
func (subscription *SubscriptionRepo) GetCurrencyByID(ctx context.Context, currencyID int64) (entities.CurrencyResponse, error) {
	var currency entities.CurrencyResponse
	query := "SELECT id, name, iso FROM currency WHERE id = $1"
	err := subscription.db.QueryRowContext(ctx, query, currencyID).Scan(&currency.ID, &currency.Name, &currency.Code)
	if err != nil {
		return entities.CurrencyResponse{}, err
	}

	return currency, nil
}

// GetSubscriptionStores gets subscription stores.
func (subscription *SubscriptionRepo) GetSubscriptionStores(ctx context.Context, subscriptionPlanID string, page, perPage int) ([]entities.SubscriptionStoreResponse, int, error) {
	offset := (page - 1) * perPage

	query := `
		SELECT s.id, s.name, s.logo, COALESCE(ss.id IS NOT NULL, false) AS status, ss.id AS subscription_store_id
		FROM store s
		LEFT JOIN partner_store ps ON s.id = ps.store_id
		LEFT JOIN subscription_store ss ON ps.id = ss.partner_store_id AND ss.subscription_id = $1
		LIMIT $2 OFFSET $3
	`

	countQuery := `
		SELECT COUNT(s.id)
		FROM store s
		LEFT JOIN partner_store ps ON s.id = ps.store_id
		LEFT JOIN subscription_store ss ON ps.id = ss.partner_store_id AND ss.subscription_id = $1
	`

	var totalCount int
	err := subscription.db.QueryRowContext(ctx, countQuery, subscriptionPlanID).Scan(&totalCount)
	if err != nil {
		return nil, 0, err
	}

	rows, err := subscription.db.QueryContext(ctx, query, subscriptionPlanID, perPage, offset)
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	var stores []entities.SubscriptionStoreResponse

	for rows.Next() {
		var store entities.SubscriptionStoreResponse
		var subscriptionStoreID entities.NullableInt64

		err := rows.Scan(
			&store.ID,
			&store.Name,
			&store.Logo,
			&store.Status,
			&subscriptionStoreID.NullInt64,
		)
		if err != nil {
			return nil, 0, err
		}
		store.SubscriptionStoreID = subscriptionStoreID
		stores = append(stores, store)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return stores, totalCount, nil
}

// GetSubscriptionCountries gets subscription countries.
func (subscription *SubscriptionRepo) GetSubscriptionCountries(ctx context.Context, subscriptionPlanID string, page, perPage int) ([]entities.SubscriptionCountryResponse, int, error) {
	offset := (page - 1) * perPage

	query := `
		SELECT c.id, c.name, COALESCE(sc.id IS NOT NULL, false) AS status
		FROM country c
		LEFT JOIN subscription_country sc ON c.id = sc.country_id AND sc.subscription_id = $1
		LIMIT $2 OFFSET $3
	`
	countQuery := `
		SELECT COUNT(c.id)
		FROM country c
		LEFT JOIN subscription_country sc ON c.id = sc.country_id AND sc.subscription_id = $1
	`
	var totalCount int
	err := subscription.db.QueryRowContext(ctx, countQuery, subscriptionPlanID).Scan(&totalCount)
	if err != nil {
		return nil, 0, err
	}

	rows, err := subscription.db.QueryContext(ctx, query, subscriptionPlanID, perPage, offset)
	if err != nil {
		return nil, 0, err
	}
	defer rows.Close()

	var countries []entities.SubscriptionCountryResponse

	for rows.Next() {
		var country entities.SubscriptionCountryResponse
		err := rows.Scan(
			&country.Code,
			&country.Name,
			&country.Status,
		)
		if err != nil {
			return nil, 0, err
		}
		countries = append(countries, country)
	}

	if err := rows.Err(); err != nil {
		return nil, 0, err
	}

	return countries, totalCount, nil
}

// DeleteSubscriptionPlan deletes a subscription plan.
func (subscription *SubscriptionRepo) DeleteSubscriptionPlan(ctx context.Context, subscriptionPlanID string, partnerID string) (string, error) {
	// Check if there are any active members using the current plan
	var activeMembersCount int
	err := subscription.db.QueryRowContext(ctx, "SELECT COUNT(*) FROM member_subscription WHERE subscription_id = $1 AND expiration_date > NOW()", subscriptionPlanID).Scan(&activeMembersCount)
	if err != nil {
		return "", err
	}

	// Determine the operation based on activeMembersCount
	var operation string
	if activeMembersCount > 0 {
		// Deactivate the subscription plan
		_, err := subscription.db.ExecContext(ctx, "UPDATE subscription_plan SET is_active = false WHERE id = $1", subscriptionPlanID)
		if err != nil {
			return "", err
		}
		operation = "deactivated"
	} else {
		// Delete the subscription plan
		_, err := subscription.db.ExecContext(ctx, "UPDATE subscription_plan SET is_deleted = true, deleted_by = $1, deleted_on = NOW(), is_active = false WHERE id = $2", partnerID, subscriptionPlanID)
		if err != nil {
			return "", err
		}
		operation = "deleted"
	}

	return operation, nil
}

// GetSubscription returns an empty slice of Subscription.
func (subscription *SubscriptionRepo) GetSubscription() []entities.Subscription {
	return []entities.Subscription{}
}

// CreateSubscriptionDuration inserts a subscription duration into the database.
func (subscription *SubscriptionRepo) CreateSubscriptionDuration(ctx context.Context, subscriptionDuration entities.SubscriptionDuration) error {
	log := logger.Log().WithContext(ctx)
	_, err := subscription.db.ExecContext(ctx, `INSERT INTO subscription_duration (name, value, payment_option_text, interval_unit, interval_count)
	VALUES ($1,$2,$3,$4,$5)`, subscriptionDuration.Name, subscriptionDuration.Value, subscriptionDuration.PaymentOptionText, subscriptionDuration.IntervalUnit, subscriptionDuration.IntervalCount)
	if err != nil {
		log.Errorf("CreateSubscriptionDuration failed, ExecContext failed, err=%s", err.Error())
		return err
	}
	return nil
}

// GetTotalSubscriptionDurationCount returns the total count of subscription durations in the database.
func (subscription *SubscriptionRepo) GetTotalSubscriptionDurationCount(ctx context.Context) (int64, error) {
	log := logger.Log().WithContext(ctx)
	var totalCount int64
	countQuery := "SELECT COUNT(*) FROM subscription_duration"
	err := subscription.db.QueryRowContext(ctx, countQuery).Scan(&totalCount)
	if err != nil {
		log.Errorf("GetTotalSubscriptionDurationCount failed, QueryRowContext failed, err=%s", err.Error())
		return 0, err
	}
	return totalCount, nil
}

// GetAllSubscriptionDuration retrieves all subscription durations based on a subquery.
func (subscription *SubscriptionRepo) GetAllSubscriptionDuration(ctx context.Context, subQuery string) ([]entities.SubscriptionDuration, error) {
	log := logger.Log().WithContext(ctx)
	var durations []entities.SubscriptionDuration
	selectQuery := fmt.Sprintf("SELECT id, name, interval_count, interval_unit FROM subscription_duration ORDER BY value ASC %s", subQuery)

	rows, err := subscription.db.QueryContext(ctx, selectQuery)
	if err != nil {
		log.Errorf("GetAllSubscriptionDuration failed, QueryContext failed, err=%s", err.Error())
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var duration entities.SubscriptionDuration
		if err := rows.Scan(&duration.ID, &duration.Name, &duration.IntervalCount, &duration.IntervalUnit); err != nil {
			log.Errorf("GetAllSubscriptionDuration failed, rows.Scan() failed, err=%s", err.Error())
			return nil, err
		}
		durations = append(durations, duration)
	}

	if err := rows.Err(); err != nil {
		log.Errorf("GetAllSubscriptionDuration failed, rows.Err() failed, err=%s", err.Error())
		return nil, err
	}
	return durations, nil
}

// EditSubscriptionDuration updates a subscription duration in the database.
func (subscription *SubscriptionRepo) EditSubscriptionDuration(ctx context.Context, durationID int64, data map[string]interface{}) error {
	log := logger.Log().WithContext(ctx)
	if len(data) == 0 {
		return nil // No fields to update
	}

	// Construct the SQL update statement dynamically
	var updates []string
	var values []interface{}
	index := 1
	for field, value := range data {
		updates = append(updates, fmt.Sprintf("%s = $%d", field, index))
		values = append(values, value)
		index++
	}

	values = append(values, durationID)

	stmt := fmt.Sprintf("UPDATE subscription_duration SET %s WHERE id = $%d",
		strings.Join(updates, ", "), index)

	_, err := subscription.db.ExecContext(ctx, stmt, values...)
	if err != nil {
		errMsg := fmt.Errorf("failed to update subscription duration fields: %w", err)
		log.Errorf("EditSubscriptionDuration failed, ExecContext() failed, err=%s", errMsg.Error())
		return errMsg
	}

	return nil
}

// GetSubscriptionDurationsByID retrieves a subscription duration by its ID from the database.
func (subscription *SubscriptionRepo) GetSubscriptionDurationsByID(ctx context.Context, durationID int64) (entities.SubscriptionDuration, error) {
	log := logger.Log().WithContext(ctx)
	var duration entities.SubscriptionDuration

	err := subscription.db.QueryRowContext(ctx, `
	SELECT name, value, payment_option_text,
	interval_unit, interval_count FROM subscription_duration 
	WHERE id  = $1`, durationID).Scan(
		&duration.Name, &duration.Value,
		&duration.PaymentOptionText,
		&duration.IntervalUnit,
		&duration.IntervalCount)

	if err != nil {
		log.Errorf("GetSubscriptionDurationsByID failed, QueryRowContext() failed, err=%s", err.Error())
		return duration, err
	}
	return duration, nil
}

// DeleteSubscriptionDuration deletes a subscription duration from the database.
func (subscription *SubscriptionRepo) DeleteSubscriptionDuration(ctx context.Context, durationID int64) error {
	log := logger.Log().WithContext(ctx)
	_, err := subscription.db.Exec("DELETE FROM subscription_duration WHERE id = $1", durationID)
	if err != nil {
		log.Errorf("DeleteSubscriptionDuration failed, Exec() failed, err=%s", err.Error())
		return err
	}
	return nil
}

// GetSubscriptionDurationByID retrieves a subscription duration by its ID from the database.
func (subscription *SubscriptionRepo) GetSubscriptionDurationByID(ctx context.Context, durationID int64) (entities.SubscriptionDuration, error) {
	log := logger.Log().WithContext(ctx)
	var duration entities.SubscriptionDuration
	query := "SELECT id, name, value, payment_option_text, interval_unit, interval_count FROM subscription_duration WHERE id = $1"
	err := subscription.db.QueryRowContext(ctx, query, durationID).Scan(
		&duration.ID, &duration.Name, &duration.Value, &duration.PaymentOptionText, &duration.IntervalUnit, &duration.IntervalCount,
	)
	if err != nil {
		log.Errorf("GetSubscriptionDurationByID failed, QueryRowContext() failed, err=%s", err.Error())
		return entities.SubscriptionDuration{}, err
	}

	return duration, nil
}

// HandleSubscriptionRenewal handles the renewal of a subscription for a member.
// Parameters:
//   - ctx (context.Context): The context for the database operation.
//   - memberID (uuid.UUID): The unique identifier of the member renewing the subscription.
//   - checkoutData (entities.SubscriptionRenewal): The data related to subscription renewal.
// Returns:
//   - (error): An error if the renewal process fails.

// HandleSubscriptionRenewal handles the renewal of a subscription for a member.
func (subscription *SubscriptionRepo) HandleSubscriptionRenewal(ctx context.Context, memberID uuid.UUID, checkoutData entities.SubscriptionRenewal) error {

	// Query to check if the member is subscribed to the specified plan.
	checkMemberSubscriptionQuery := `
        SELECT EXISTS (SELECT 1 FROM member_subscription WHERE member_id = $1 AND subscription_id = $2) AS subscribed;
    `

	var subscribed bool

	err := subscription.db.QueryRowContext(ctx, checkMemberSubscriptionQuery, memberID, checkoutData.SubscriptionID).Scan(&subscribed)

	if err != nil {
		return err
	}

	if !subscribed {
		return fmt.Errorf("you have not subscribed to this plan")
	}

	// Query to check renewal conditions such as expiration date and status.
	checkRenewalConditionsQuery := `
        SELECT expiration_date, mss.name
        FROM member_subscription ms
        INNER JOIN member_subscription_status mss ON ms.member_subscription_status_id = mss.id
        WHERE ms.member_id = $1 AND ms.subscription_id = $2;
    `

	var expirationDate time.Time
	var statusName string

	err = subscription.db.QueryRowContext(ctx, checkRenewalConditionsQuery, memberID, checkoutData.SubscriptionID).Scan(&expirationDate, &statusName)

	if err != nil {
		return err
	}

	// Calculate days until subscription expiration.
	daysUntilExpiration := int(expirationDate.Sub(time.Now()).Hours() / 24)

	var firstWarning, secondWarning, thirdWarning int

	// Query to fetch first, second, and third expiry warnings from the subscription plan.
	err = subscription.db.QueryRowContext(ctx, "SELECT first_expiry_warning, second_expiry_warning, third_expiry_warning FROM subscription_plan WHERE id = $1", checkoutData.SubscriptionID).Scan(&firstWarning, &secondWarning, &thirdWarning)

	if err != nil {
		return err
	}

	// Check if renewal is required and issue warnings if applicable.
	if daysUntilExpiration > 0 && (daysUntilExpiration == firstWarning || daysUntilExpiration == secondWarning || daysUntilExpiration == thirdWarning) && statusName == "active" {
		return fmt.Errorf("please renew your subscription before expiration. You can renew it by making a payment <a href=\"%s\">here</a>", consts.DummyPaymentGatewayURL)
	}

	//*** if payment is successfully made , the status is changed to renewed, any further notifications wont be made  ***//
	// if payment is not done even after 3 warnings, the subscription is moved to grace peroid (varies upon each partner)

	// Query to check grace period conditions.
	checkGracePeriodQuery := `
        SELECT expiration_date, mss.name, sp.can_renewable_within
        FROM member_subscription ms
        INNER JOIN member_subscription_status mss ON ms.member_subscription_status_id = mss.id
        INNER JOIN subscription_plan sp ON ms.subscription_id = sp.id
        WHERE ms.member_id = $1 AND ms.subscription_id = $2;
    `

	var canRenewableWithin int

	err = subscription.db.QueryRowContext(ctx, checkGracePeriodQuery, memberID, checkoutData.SubscriptionID).Scan(&expirationDate, &statusName, &canRenewableWithin)

	if err != nil {
		return err
	}

	// Check if the subscription status is "cancelled."
	if statusName == "cancelled" {
		return fmt.Errorf("your plan is already cancelled")
	}

	// Using time.Until to calculate the duration until expirationDate from now.
	daysUntilGracePeriodToEnd := int(time.Until(expirationDate).Hours()/24) - canRenewableWithin

	// Check if renewal is required during the grace period.
	if daysUntilGracePeriodToEnd > 0 && statusName == "On Hold" {

		return fmt.Errorf("you are currently on grae peroid ,  You can renew the subscription by making a payment <a href=\"%s\">here</a>", consts.DummyPaymentGatewayURL)

	} else if daysUntilGracePeriodToEnd <= 0 && statusName == "on hold" {
		// Query to update the subscription status to "expired" if grace period has ended.
		updateStatusQuery := `
		UPDATE member_subscription AS ms
		SET 
		member_subscription_status_id = mss.id
		FROM 
		member_subscription_status AS mss
		WHERE 
		ms.member_subscription_status_id = mss.id
		  AND ms.subscription_id = $1
		  AND ms.expiration_date <= NOW() - INTERVAL '1 day' * (ms.can_renewable_within + 1)
		  AND mss.name = 'expired';
	`

		_, updateErr := subscription.db.ExecContext(ctx, updateStatusQuery, checkoutData.SubscriptionID)

		if updateErr != nil {
			return updateErr
		}

		return fmt.Errorf("sorry, your subscription has expired")
	}

	// Query to fetch the subscription duration value from the database.
	var subscriptionDurationValue int
	query := `
		SELECT sd.value
		FROM public.subscription_duration AS sd
		WHERE sd.id = (
			SELECT sp.subscription_duration_id
			FROM public.subscription_plan AS sp
			WHERE sp.id = $1
		);	`

	err = subscription.db.QueryRowContext(ctx, query, checkoutData.SubscriptionID).Scan(&subscriptionDurationValue)

	if err != nil {
		return err
	}

	// Calculate the new expiration date.
	expirationDate = time.Now().Add(time.Duration(subscriptionDurationValue) * 24 * time.Hour)

	// Query to update the expiration date for the specific subscription.
	updateExpirationDateQuery := `
        UPDATE member_subscription
        SET expiration_date = $1
        WHERE subscription_id = $2;
    `

	// Execute the update query to set the new expiration date.
	_, updateErr := subscription.db.ExecContext(ctx, updateExpirationDateQuery, expirationDate, checkoutData.SubscriptionID)

	if updateErr != nil {
		return updateErr
	}

	// The expiration_date has been updated successfully.
	return nil
}

// HandleSubscriptionCancellation handles the cancellation process for a subscription.
// It performs the following steps:
//  1. Checks if the member has subscribed to the plan.
//  2. Checks if the associated subscription plan is active/on hold.
//  3. Verifies if the subscription is cancellation enabled.
//
// Parameters:
//   - ctx (context.Context): The context for the database operations.
//   - memberID (uuid.UUID): The unique identifier of the member.
//   - checkoutData (entities.CancelSubscription): The cancellation data, including SubscriptionID.
//
// Returns:
//   - error: An error if any database operation fails.

func (subscription *SubscriptionRepo) HandleSubscriptionCancellation(ctx context.Context, memberID uuid.UUID, checkoutData entities.CancelSubscription) error {

	// If all conditions are met, update the member_subscription_status to "cancelled".
	updateStatusQuery := `
   			 UPDATE member_subscription
    		 SET member_subscription_status_id = (SELECT id FROM member_subscription_status WHERE name = 'cancelled') 
			 WHERE subscription_id = $1 ;
`

	_, err := subscription.db.ExecContext(ctx, updateStatusQuery, checkoutData.SubscriptionID)

	if err != nil {
		return err
	}

	return nil
}

// CheckSubscriptionExistenceAndStatus checks if the given subscription ID exists in the database
// and if the subscription plan associated with it is currently active.
// It returns a boolean indicating existence and activation status, along with an error if any.
func (subscription *SubscriptionRepo) CheckSubscriptionExistenceAndStatus(ctx *gin.Context, subscriptionID string) (exists bool, isActive bool, err error) {
	// Query to check if the subscription exists and its active status.
	query := `
        SELECT EXISTS (
            SELECT 1 FROM subscription_plan WHERE id = $1
        ), is_active
        FROM subscription_plan
        WHERE id = $1;
    `
	err = subscription.db.QueryRowContext(ctx, query, subscriptionID).Scan(&exists, &isActive)
	if err != nil {
		return false, false, fmt.Errorf("error checking subscription existence and status: %s", err)
	}

	return exists, isActive, nil
}

// IsSubscriptionFree checks if the provided subscription ID corresponds to a free subscription.
// Returns a boolean indicating if it's a free subscription and an error if any.
func (subscription *SubscriptionRepo) IsSubscriptionFree(ctx context.Context, subscriptionID string) (bool, error) {
	var isFree bool
	err := subscription.db.QueryRowContext(ctx, `
        SELECT is_free
        FROM subscription_plan
        WHERE id = $1
    `, subscriptionID).Scan(&isFree)

	if err != nil {
		return false, fmt.Errorf("error checking if subscription is free: %s", err)
	}

	return isFree, nil
}

// CheckIfMemberSubscribedToFreePlan checks if a member has already subscribed to a specific free subscription.
// It takes the context, memberID, and subscriptionID as parameters.
// Returns a boolean indicating whether the member has subscribed to the free plan and an error if any.
func (subscription *SubscriptionRepo) CheckIfMemberSubscribedToFreePlan(ctx *gin.Context, memberID uuid.UUID, subscriptionID string) (bool, error) {
	// Check if the subscription is free
	isFree, err := subscription.IsSubscriptionFree(ctx, subscriptionID)
	if err != nil {
		return false, err
	}

	// If the subscription is not free, return false
	if !isFree {
		return false, nil
	}

	// Check if the member is subscribed to this free subscription
	var hasSubscribed bool
	err = subscription.db.QueryRowContext(ctx, `
        SELECT EXISTS (
            SELECT 1
            FROM public.member_subscription
            WHERE member_id = $1
            AND subscription_id = $2
        );
    `, memberID, subscriptionID).Scan(&hasSubscribed)

	if err != nil {
		return false, fmt.Errorf("error checking member subscription: %s", err)
	}

	return hasSubscribed, nil
}

// IsOneTimeSubscription checks if the provided subscription ID corresponds to a one-time subscription.
// Returns a boolean indicating if it's a one-time subscription and an error if any.
func (subscription *SubscriptionRepo) IsOneTimeSubscription(ctx context.Context, subscriptionID string) (bool, error) {
	var isOneTime bool
	err := subscription.db.QueryRowContext(ctx, `
        SELECT is_one_time_subscription
        FROM public.subscription_plan
        WHERE id = $1;
    `, subscriptionID).Scan(&isOneTime)

	if err != nil {
		return false, fmt.Errorf("error checking if it's a one-time subscription: %s", err)
	}

	return isOneTime, nil
}

// HasSubscribedToOneTimePlan checks if a member has already subscribed to a specific one-time subscription plan.
// It takes the context, memberID, and subscriptionID as parameters.
// Returns a boolean indicating whether the member has subscribed to the one-time plan and an error if any.
func (subscription *SubscriptionRepo) HasSubscribedToOneTimePlan(ctx *gin.Context, memberID uuid.UUID, subscriptionID string) (bool, error) {
	// Check if it's a one-time subscription
	isOneTime, err := subscription.IsOneTimeSubscription(ctx, subscriptionID)
	if err != nil {
		return false, err
	}

	// If it's not a one-time subscription, return false
	if !isOneTime {
		return false, nil
	}

	// Check if the member is subscribed to this one-time subscription
	var hasSubscribed bool
	err = subscription.db.QueryRowContext(ctx, `
        SELECT EXISTS (
            SELECT 1
            FROM public.member_subscription
            WHERE member_id = $1
            AND subscription_id = $2
        );
    `, memberID, subscriptionID).Scan(&hasSubscribed)

	if err != nil {
		return false, fmt.Errorf("error checking member subscription: %s", err)
	}

	return hasSubscribed, nil
}

// CheckIfPayoutGatewayExists checks if the given PaymentGatewayID exists in the database.
// Returns an error if the PaymentGateway does not exist.
func (subscription *SubscriptionRepo) CheckIfPayoutGatewayExists(ctx *gin.Context, paymentGatewayID int) (bool, error) {
	var payoutGatewayExists bool

	// Check if PaymentGatewayID exists
	err := subscription.db.QueryRowContext(ctx, `
        SELECT EXISTS (SELECT 1 FROM payment_gateway WHERE id = $1) AS payout_gateway_exists
    `, paymentGatewayID).Scan(&payoutGatewayExists)

	if err != nil {
		return false, fmt.Errorf("error checking payout gateway existence: %s", err)
	}

	// If PaymentGatewayID does not exist, return an error
	if !payoutGatewayExists {
		return false, fmt.Errorf("PaymentGateway does not exist")
	}

	return true, nil
}

// CheckMemberSubscriptionLimitAndYearlyLimit checks if a member has reached the yearly subscription limit for a specific plan.
// It also returns whether the subscription plan has a yearly limit. Returns a boolean indicating if the member has reached the limit
// and an error if any.
func (subscription *SubscriptionRepo) CheckMemberSubscriptionLimitAndYearlyLimit(ctx context.Context, memberID uuid.UUID, subscriptionID string) (bool, bool, error) {
	// Check if the subscription plan has a yearly limit
	var subscriptionLimitPerYear int
	err := subscription.db.QueryRowContext(ctx, `
        SELECT subscription_limit_per_year
        FROM public.subscription_plan
        WHERE id = $1;
    `, subscriptionID).Scan(&subscriptionLimitPerYear)

	if err != nil {
		return false, false, fmt.Errorf("error fetching subscription limit per year: %s", err)
	}

	hasLimit := subscriptionLimitPerYear > 0

	if !hasLimit {
		return false, false, nil
	}

	// Calculate the date range for the past year
	oneYearAgo := time.Now().AddDate(-1, 0, 0)

	// Check how many subscriptions the member has for this plan in the past year
	var subscriptionCount int
	err = subscription.db.QueryRowContext(ctx, `
        SELECT COUNT(*)
        FROM public.member_subscription
        WHERE member_id = $1 AND subscription_id = $2 AND created_on >= $3;
    `, memberID, subscriptionID, oneYearAgo).Scan(&subscriptionCount)

	if err != nil {
		return false, false, fmt.Errorf("error fetching member subscription count: %s", err)
	}

	return subscriptionCount >= subscriptionLimitPerYear, hasLimit, nil
}

// Function to get the subscription status name by subscription_id
func (subscription *SubscriptionRepo) GetSubscriptionStatusName(ctx *gin.Context, subscriptionID string) (string, error) {
	var statusName string

	// Query to fetch the subscription status name based on subscription_id
	err := subscription.db.QueryRowContext(ctx, `
        SELECT ms.name
        FROM public.member_subscription_status ms
        JOIN public.member_subscription msub ON ms.id = msub.member_subscription_status_id
        WHERE msub.subscription_id = $1
    `, subscriptionID).Scan(&statusName)

	if err != nil {
		return "", fmt.Errorf("error fetching subscription status name: %s", err)
	}

	// If no error and statusName is empty, it means no matching record found.
	if statusName == "" {
		return "", fmt.Errorf("no matching subscription status found for the given subscription ID")
	}

	return statusName, nil
}

// Function to check if a member is subscribed to a specified plan.
func (subscription *SubscriptionRepo) IsMemberSubscribedToPlan(ctx *gin.Context, memberID uuid.UUID, subscriptionID string) (bool, error) {
	// Query to check if the member is subscribed to the specified plan.
	checkMemberSubscriptionQuery := `
        SELECT EXISTS (SELECT 1 FROM member_subscription WHERE member_id = $1 AND subscription_id = $2) AS subscribed;
    `

	var subscribed bool

	// Execute the query and scan the result into the subscribed variable.
	err := subscription.db.QueryRowContext(ctx, checkMemberSubscriptionQuery, memberID, subscriptionID).Scan(&subscribed)

	if err != nil {
		return false, fmt.Errorf("error checking subscription status: %w", err)
	}

	return subscribed, nil
}

// CheckCancellationEnabled checks if a subscription plan allows cancellations.
// It returns true if cancellation is enabled, otherwise false.
func (subscription *SubscriptionRepo) CheckCancellationEnabled(ctx *gin.Context, subscriptionID string) (bool, error) {
	// SQL query to fetch the is_cancellation_enabled column for the given SubscriptionID.
	query := `
		SELECT is_cancellation_enabled FROM public.subscription_plan WHERE id = $1;
	`

	var isCancellationEnabled bool

	// Execute the SQL query and scan the result into the isCancellationEnabled variable.
	err := subscription.db.QueryRowContext(ctx, query, subscriptionID).Scan(&isCancellationEnabled)
	if err != nil {
		return false, fmt.Errorf("failed to fetch cancellation status: %v", err)
	}

	return isCancellationEnabled, nil
}
