// Code generated by MockGen. DO NOT EDIT.
// Source: subscription/internal/repo (interfaces: SubscriptionRepoImply)

// Package mock is a generated GoMock package.
package mock

import (
	context "context"
	reflect "reflect"
	entities "subscription/internal/entities"

	gin "github.com/gin-gonic/gin"
	gomock "github.com/golang/mock/gomock"
	uuid "github.com/google/uuid"
)

// MockSubscriptionRepoImply is a mock of SubscriptionRepoImply interface.
type MockSubscriptionRepoImply struct {
	ctrl     *gomock.Controller
	recorder *MockSubscriptionRepoImplyMockRecorder
}

// MockSubscriptionRepoImplyMockRecorder is the mock recorder for MockSubscriptionRepoImply.
type MockSubscriptionRepoImplyMockRecorder struct {
	mock *MockSubscriptionRepoImply
}

// NewMockSubscriptionRepoImply creates a new mock instance.
func NewMockSubscriptionRepoImply(ctrl *gomock.Controller) *MockSubscriptionRepoImply {
	mock := &MockSubscriptionRepoImply{ctrl: ctrl}
	mock.recorder = &MockSubscriptionRepoImplyMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockSubscriptionRepoImply) EXPECT() *MockSubscriptionRepoImplyMockRecorder {
	return m.recorder
}

// CheckCancellationEnabled mocks base method.
func (m *MockSubscriptionRepoImply) CheckCancellationEnabled(arg0 *gin.Context, arg1 string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckCancellationEnabled", arg0, arg1)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CheckCancellationEnabled indicates an expected call of CheckCancellationEnabled.
func (mr *MockSubscriptionRepoImplyMockRecorder) CheckCancellationEnabled(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckCancellationEnabled", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).CheckCancellationEnabled), arg0, arg1)
}

// CheckIfMemberSubscribedToFreePlan mocks base method.
func (m *MockSubscriptionRepoImply) CheckIfMemberSubscribedToFreePlan(arg0 *gin.Context, arg1 uuid.UUID, arg2 string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckIfMemberSubscribedToFreePlan", arg0, arg1, arg2)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CheckIfMemberSubscribedToFreePlan indicates an expected call of CheckIfMemberSubscribedToFreePlan.
func (mr *MockSubscriptionRepoImplyMockRecorder) CheckIfMemberSubscribedToFreePlan(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckIfMemberSubscribedToFreePlan", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).CheckIfMemberSubscribedToFreePlan), arg0, arg1, arg2)
}

// CheckIfPayoutGatewayExists mocks base method.
func (m *MockSubscriptionRepoImply) CheckIfPayoutGatewayExists(arg0 *gin.Context, arg1 int) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckIfPayoutGatewayExists", arg0, arg1)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CheckIfPayoutGatewayExists indicates an expected call of CheckIfPayoutGatewayExists.
func (mr *MockSubscriptionRepoImplyMockRecorder) CheckIfPayoutGatewayExists(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckIfPayoutGatewayExists", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).CheckIfPayoutGatewayExists), arg0, arg1)
}

// CheckSubscriptionExistenceAndStatus mocks base method.
func (m *MockSubscriptionRepoImply) CheckSubscriptionExistenceAndStatus(arg0 *gin.Context, arg1 string) (bool, bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckSubscriptionExistenceAndStatus", arg0, arg1)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(bool)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// CheckSubscriptionExistenceAndStatus indicates an expected call of CheckSubscriptionExistenceAndStatus.
func (mr *MockSubscriptionRepoImplyMockRecorder) CheckSubscriptionExistenceAndStatus(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckSubscriptionExistenceAndStatus", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).CheckSubscriptionExistenceAndStatus), arg0, arg1)
}

// CreateSubscriptionDuration mocks base method.
func (m *MockSubscriptionRepoImply) CreateSubscriptionDuration(arg0 context.Context, arg1 entities.SubscriptionDuration) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateSubscriptionDuration", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateSubscriptionDuration indicates an expected call of CreateSubscriptionDuration.
func (mr *MockSubscriptionRepoImplyMockRecorder) CreateSubscriptionDuration(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateSubscriptionDuration", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).CreateSubscriptionDuration), arg0, arg1)
}

// CreateSubscriptionPlanTransaction mocks base method.
func (m *MockSubscriptionRepoImply) CreateSubscriptionPlanTransaction(arg0 context.Context, arg1 entities.SubscriptionPlan) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateSubscriptionPlanTransaction", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// CreateSubscriptionPlanTransaction indicates an expected call of CreateSubscriptionPlanTransaction.
func (mr *MockSubscriptionRepoImplyMockRecorder) CreateSubscriptionPlanTransaction(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateSubscriptionPlanTransaction", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).CreateSubscriptionPlanTransaction), arg0, arg1)
}

// DeleteSubscriptionDuration mocks base method.
func (m *MockSubscriptionRepoImply) DeleteSubscriptionDuration(arg0 context.Context, arg1 int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteSubscriptionDuration", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// DeleteSubscriptionDuration indicates an expected call of DeleteSubscriptionDuration.
func (mr *MockSubscriptionRepoImplyMockRecorder) DeleteSubscriptionDuration(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteSubscriptionDuration", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).DeleteSubscriptionDuration), arg0, arg1)
}

// DeleteSubscriptionPlan mocks base method.
func (m *MockSubscriptionRepoImply) DeleteSubscriptionPlan(arg0 context.Context, arg1, arg2 string) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DeleteSubscriptionPlan", arg0, arg1, arg2)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// DeleteSubscriptionPlan indicates an expected call of DeleteSubscriptionPlan.
func (mr *MockSubscriptionRepoImplyMockRecorder) DeleteSubscriptionPlan(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DeleteSubscriptionPlan", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).DeleteSubscriptionPlan), arg0, arg1, arg2)
}

// EditSubscriptionDuration mocks base method.
func (m *MockSubscriptionRepoImply) EditSubscriptionDuration(arg0 context.Context, arg1 int64, arg2 map[string]interface{}) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EditSubscriptionDuration", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// EditSubscriptionDuration indicates an expected call of EditSubscriptionDuration.
func (mr *MockSubscriptionRepoImplyMockRecorder) EditSubscriptionDuration(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EditSubscriptionDuration", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).EditSubscriptionDuration), arg0, arg1, arg2)
}

// EditSubscriptionPlan mocks base method.
func (m *MockSubscriptionRepoImply) EditSubscriptionPlan(arg0 context.Context, arg1 string, arg2 map[string]interface{}) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EditSubscriptionPlan", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// EditSubscriptionPlan indicates an expected call of EditSubscriptionPlan.
func (mr *MockSubscriptionRepoImplyMockRecorder) EditSubscriptionPlan(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EditSubscriptionPlan", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).EditSubscriptionPlan), arg0, arg1, arg2)
}

// EditSubscriptionPlanCountry mocks base method.
func (m *MockSubscriptionRepoImply) EditSubscriptionPlanCountry(arg0 context.Context, arg1 string, arg2 []int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EditSubscriptionPlanCountry", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// EditSubscriptionPlanCountry indicates an expected call of EditSubscriptionPlanCountry.
func (mr *MockSubscriptionRepoImplyMockRecorder) EditSubscriptionPlanCountry(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EditSubscriptionPlanCountry", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).EditSubscriptionPlanCountry), arg0, arg1, arg2)
}

// EditSubscriptionPlanStores mocks base method.
func (m *MockSubscriptionRepoImply) EditSubscriptionPlanStores(arg0 context.Context, arg1 string, arg2 []int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EditSubscriptionPlanStores", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// EditSubscriptionPlanStores indicates an expected call of EditSubscriptionPlanStores.
func (mr *MockSubscriptionRepoImplyMockRecorder) EditSubscriptionPlanStores(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EditSubscriptionPlanStores", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).EditSubscriptionPlanStores), arg0, arg1, arg2)
}

// GetAllSubscriptionDuration mocks base method.
func (m *MockSubscriptionRepoImply) GetAllSubscriptionDuration(arg0 context.Context, arg1 string) ([]entities.SubscriptionDuration, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAllSubscriptionDuration", arg0, arg1)
	ret0, _ := ret[0].([]entities.SubscriptionDuration)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAllSubscriptionDuration indicates an expected call of GetAllSubscriptionDuration.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetAllSubscriptionDuration(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAllSubscriptionDuration", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetAllSubscriptionDuration), arg0, arg1)
}

// GetAllSubscriptionPlans mocks base method.
func (m *MockSubscriptionRepoImply) GetAllSubscriptionPlans(arg0 context.Context, arg1, arg2 string) ([]entities.SubscriptionPlan, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAllSubscriptionPlans", arg0, arg1, arg2)
	ret0, _ := ret[0].([]entities.SubscriptionPlan)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAllSubscriptionPlans indicates an expected call of GetAllSubscriptionPlans.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetAllSubscriptionPlans(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAllSubscriptionPlans", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetAllSubscriptionPlans), arg0, arg1, arg2)
}

// GetByID mocks base method.
func (m *MockSubscriptionRepoImply) GetByID(arg0 context.Context, arg1 entities.Entity) (int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByID", arg0, arg1)
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetByID indicates an expected call of GetByID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetByID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetByID), arg0, arg1)
}

// GetCountriesID mocks base method.
func (m *MockSubscriptionRepoImply) GetCountriesID(arg0 context.Context, arg1 []string) ([]int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetCountriesID", arg0, arg1)
	ret0, _ := ret[0].([]int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCountriesID indicates an expected call of GetCountriesID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetCountriesID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetCountriesID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetCountriesID), arg0, arg1)
}

// GetCurrencyByID mocks base method.
func (m *MockSubscriptionRepoImply) GetCurrencyByID(arg0 context.Context, arg1 int64) (entities.CurrencyResponse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetCurrencyByID", arg0, arg1)
	ret0, _ := ret[0].(entities.CurrencyResponse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCurrencyByID indicates an expected call of GetCurrencyByID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetCurrencyByID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetCurrencyByID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetCurrencyByID), arg0, arg1)
}

// GetCurrencyID mocks base method.
func (m *MockSubscriptionRepoImply) GetCurrencyID(arg0 context.Context, arg1 string) (int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetCurrencyID", arg0, arg1)
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetCurrencyID indicates an expected call of GetCurrencyID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetCurrencyID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetCurrencyID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetCurrencyID), arg0, arg1)
}

// GetSubscription mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscription() []entities.Subscription {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscription")
	ret0, _ := ret[0].([]entities.Subscription)
	return ret0
}

// GetSubscription indicates an expected call of GetSubscription.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscription() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscription", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscription))
}

// GetSubscriptionCountries mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionCountries(arg0 context.Context, arg1 string, arg2, arg3 int) ([]entities.SubscriptionCountryResponse, int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionCountries", arg0, arg1, arg2, arg3)
	ret0, _ := ret[0].([]entities.SubscriptionCountryResponse)
	ret1, _ := ret[1].(int)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// GetSubscriptionCountries indicates an expected call of GetSubscriptionCountries.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionCountries(arg0, arg1, arg2, arg3 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionCountries", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionCountries), arg0, arg1, arg2, arg3)
}

// GetSubscriptionDurationByID mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionDurationByID(arg0 context.Context, arg1 int64) (entities.SubscriptionDuration, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionDurationByID", arg0, arg1)
	ret0, _ := ret[0].(entities.SubscriptionDuration)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSubscriptionDurationByID indicates an expected call of GetSubscriptionDurationByID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionDurationByID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionDurationByID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionDurationByID), arg0, arg1)
}

// GetSubscriptionDurationsByID mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionDurationsByID(arg0 context.Context, arg1 int64) (entities.SubscriptionDuration, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionDurationsByID", arg0, arg1)
	ret0, _ := ret[0].(entities.SubscriptionDuration)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSubscriptionDurationsByID indicates an expected call of GetSubscriptionDurationsByID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionDurationsByID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionDurationsByID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionDurationsByID), arg0, arg1)
}

// GetSubscriptionPlanByID mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionPlanByID(arg0 context.Context, arg1 string) (entities.SubscriptionPlan, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionPlanByID", arg0, arg1)
	ret0, _ := ret[0].(entities.SubscriptionPlan)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSubscriptionPlanByID indicates an expected call of GetSubscriptionPlanByID.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionPlanByID(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionPlanByID", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionPlanByID), arg0, arg1)
}

// GetSubscriptionRecordCount mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionRecordCount(arg0 context.Context, arg1 uuid.UUID) (int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionRecordCount", arg0, arg1)
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSubscriptionRecordCount indicates an expected call of GetSubscriptionRecordCount.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionRecordCount(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionRecordCount", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionRecordCount), arg0, arg1)
}

// GetSubscriptionStatusName mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionStatusName(arg0 *gin.Context, arg1 string) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionStatusName", arg0, arg1)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSubscriptionStatusName indicates an expected call of GetSubscriptionStatusName.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionStatusName(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionStatusName", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionStatusName), arg0, arg1)
}

// GetSubscriptionStores mocks base method.
func (m *MockSubscriptionRepoImply) GetSubscriptionStores(arg0 context.Context, arg1 string, arg2, arg3 int) ([]entities.SubscriptionStoreResponse, int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSubscriptionStores", arg0, arg1, arg2, arg3)
	ret0, _ := ret[0].([]entities.SubscriptionStoreResponse)
	ret1, _ := ret[1].(int)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// GetSubscriptionStores indicates an expected call of GetSubscriptionStores.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetSubscriptionStores(arg0, arg1, arg2, arg3 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSubscriptionStores", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetSubscriptionStores), arg0, arg1, arg2, arg3)
}

// GetTotalSubscriptionDurationCount mocks base method.
func (m *MockSubscriptionRepoImply) GetTotalSubscriptionDurationCount(arg0 context.Context) (int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetTotalSubscriptionDurationCount", arg0)
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetTotalSubscriptionDurationCount indicates an expected call of GetTotalSubscriptionDurationCount.
func (mr *MockSubscriptionRepoImplyMockRecorder) GetTotalSubscriptionDurationCount(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetTotalSubscriptionDurationCount", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).GetTotalSubscriptionDurationCount), arg0)
}

// HandleSubscriptionCancellation mocks base method.
func (m *MockSubscriptionRepoImply) HandleSubscriptionCancellation(arg0 context.Context, arg1 uuid.UUID, arg2 entities.CancelSubscription) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HandleSubscriptionCancellation", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// HandleSubscriptionCancellation indicates an expected call of HandleSubscriptionCancellation.
func (mr *MockSubscriptionRepoImplyMockRecorder) HandleSubscriptionCancellation(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HandleSubscriptionCancellation", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).HandleSubscriptionCancellation), arg0, arg1, arg2)
}

// HandleSubscriptionCheckout mocks base method.
func (m *MockSubscriptionRepoImply) HandleSubscriptionCheckout(arg0 context.Context, arg1 uuid.UUID, arg2 entities.CheckoutSubscription) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HandleSubscriptionCheckout", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// HandleSubscriptionCheckout indicates an expected call of HandleSubscriptionCheckout.
func (mr *MockSubscriptionRepoImplyMockRecorder) HandleSubscriptionCheckout(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HandleSubscriptionCheckout", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).HandleSubscriptionCheckout), arg0, arg1, arg2)
}

// HandleSubscriptionRenewal mocks base method.
func (m *MockSubscriptionRepoImply) HandleSubscriptionRenewal(arg0 context.Context, arg1 uuid.UUID, arg2 entities.SubscriptionRenewal) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HandleSubscriptionRenewal", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// HandleSubscriptionRenewal indicates an expected call of HandleSubscriptionRenewal.
func (mr *MockSubscriptionRepoImplyMockRecorder) HandleSubscriptionRenewal(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HandleSubscriptionRenewal", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).HandleSubscriptionRenewal), arg0, arg1, arg2)
}

// HasSubscribedToOneTimePlan mocks base method.
func (m *MockSubscriptionRepoImply) HasSubscribedToOneTimePlan(arg0 *gin.Context, arg1 uuid.UUID, arg2 string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "HasSubscribedToOneTimePlan", arg0, arg1, arg2)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// HasSubscribedToOneTimePlan indicates an expected call of HasSubscribedToOneTimePlan.
func (mr *MockSubscriptionRepoImplyMockRecorder) HasSubscribedToOneTimePlan(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "HasSubscribedToOneTimePlan", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).HasSubscribedToOneTimePlan), arg0, arg1, arg2)
}

// InsertNewProductTypes mocks base method.
func (m *MockSubscriptionRepoImply) InsertNewProductTypes(arg0 context.Context, arg1 string, arg2 []int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InsertNewProductTypes", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// InsertNewProductTypes indicates an expected call of InsertNewProductTypes.
func (mr *MockSubscriptionRepoImplyMockRecorder) InsertNewProductTypes(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InsertNewProductTypes", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).InsertNewProductTypes), arg0, arg1, arg2)
}

// InsertNewTrackQualities mocks base method.
func (m *MockSubscriptionRepoImply) InsertNewTrackQualities(arg0 context.Context, arg1 string, arg2 []int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InsertNewTrackQualities", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// InsertNewTrackQualities indicates an expected call of InsertNewTrackQualities.
func (mr *MockSubscriptionRepoImplyMockRecorder) InsertNewTrackQualities(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InsertNewTrackQualities", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).InsertNewTrackQualities), arg0, arg1, arg2)
}

// IsFreeSubscription mocks base method.
func (m *MockSubscriptionRepoImply) IsFreeSubscription(arg0 context.Context, arg1 string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsFreeSubscription", arg0, arg1)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IsFreeSubscription indicates an expected call of IsFreeSubscription.
func (mr *MockSubscriptionRepoImplyMockRecorder) IsFreeSubscription(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsFreeSubscription", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).IsFreeSubscription), arg0, arg1)
}

// IsMemberExists mocks base method.
func (m *MockSubscriptionRepoImply) IsMemberExists(arg0 context.Context, arg1 uuid.UUID) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsMemberExists", arg0, arg1)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IsMemberExists indicates an expected call of IsMemberExists.
func (mr *MockSubscriptionRepoImplyMockRecorder) IsMemberExists(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsMemberExists", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).IsMemberExists), arg0, arg1)
}

// IsMemberSubscribedToPlan mocks base method.
func (m *MockSubscriptionRepoImply) IsMemberSubscribedToPlan(arg0 *gin.Context, arg1 uuid.UUID, arg2 string) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "IsMemberSubscribedToPlan", arg0, arg1, arg2)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// IsMemberSubscribedToPlan indicates an expected call of IsMemberSubscribedToPlan.
func (mr *MockSubscriptionRepoImplyMockRecorder) IsMemberSubscribedToPlan(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "IsMemberSubscribedToPlan", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).IsMemberSubscribedToPlan), arg0, arg1, arg2)
}

// RemoveExistingProductTypes mocks base method.
func (m *MockSubscriptionRepoImply) RemoveExistingProductTypes(arg0 context.Context, arg1 string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RemoveExistingProductTypes", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// RemoveExistingProductTypes indicates an expected call of RemoveExistingProductTypes.
func (mr *MockSubscriptionRepoImplyMockRecorder) RemoveExistingProductTypes(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemoveExistingProductTypes", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).RemoveExistingProductTypes), arg0, arg1)
}

// RemoveExistingTrackQualities mocks base method.
func (m *MockSubscriptionRepoImply) RemoveExistingTrackQualities(arg0 context.Context, arg1 string) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RemoveExistingTrackQualities", arg0, arg1)
	ret0, _ := ret[0].(error)
	return ret0
}

// RemoveExistingTrackQualities indicates an expected call of RemoveExistingTrackQualities.
func (mr *MockSubscriptionRepoImplyMockRecorder) RemoveExistingTrackQualities(arg0, arg1 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RemoveExistingTrackQualities", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).RemoveExistingTrackQualities), arg0, arg1)
}

// SubscriptionProductSwitch mocks base method.
func (m *MockSubscriptionRepoImply) SubscriptionProductSwitch(arg0 context.Context, arg1 uuid.UUID, arg2 entities.SwitchSubscriptions) (map[string][]string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SubscriptionProductSwitch", arg0, arg1, arg2)
	ret0, _ := ret[0].(map[string][]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// SubscriptionProductSwitch indicates an expected call of SubscriptionProductSwitch.
func (mr *MockSubscriptionRepoImplyMockRecorder) SubscriptionProductSwitch(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SubscriptionProductSwitch", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).SubscriptionProductSwitch), arg0, arg1, arg2)
}

// VerifyPartnerProductType mocks base method.
func (m *MockSubscriptionRepoImply) VerifyPartnerProductType(arg0 context.Context, arg1 string, arg2 []int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "VerifyPartnerProductType", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// VerifyPartnerProductType indicates an expected call of VerifyPartnerProductType.
func (mr *MockSubscriptionRepoImplyMockRecorder) VerifyPartnerProductType(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "VerifyPartnerProductType", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).VerifyPartnerProductType), arg0, arg1, arg2)
}

// VerifyPartnerStores mocks base method.
func (m *MockSubscriptionRepoImply) VerifyPartnerStores(arg0 context.Context, arg1 string, arg2 []string) ([]int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "VerifyPartnerStores", arg0, arg1, arg2)
	ret0, _ := ret[0].([]int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// VerifyPartnerStores indicates an expected call of VerifyPartnerStores.
func (mr *MockSubscriptionRepoImplyMockRecorder) VerifyPartnerStores(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "VerifyPartnerStores", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).VerifyPartnerStores), arg0, arg1, arg2)
}

// VerifyPartnerTrackQuality mocks base method.
func (m *MockSubscriptionRepoImply) VerifyPartnerTrackQuality(arg0 context.Context, arg1 string, arg2 []int64) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "VerifyPartnerTrackQuality", arg0, arg1, arg2)
	ret0, _ := ret[0].(error)
	return ret0
}

// VerifyPartnerTrackQuality indicates an expected call of VerifyPartnerTrackQuality.
func (mr *MockSubscriptionRepoImplyMockRecorder) VerifyPartnerTrackQuality(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "VerifyPartnerTrackQuality", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).VerifyPartnerTrackQuality), arg0, arg1, arg2)
}

// ViewAllSubscriptions mocks base method.
func (m *MockSubscriptionRepoImply) ViewAllSubscriptions(arg0 context.Context, arg1 uuid.UUID, arg2 entities.ReqParams) ([]entities.ListAllSubscriptions, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ViewAllSubscriptions", arg0, arg1, arg2)
	ret0, _ := ret[0].([]entities.ListAllSubscriptions)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ViewAllSubscriptions indicates an expected call of ViewAllSubscriptions.
func (mr *MockSubscriptionRepoImplyMockRecorder) ViewAllSubscriptions(arg0, arg1, arg2 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ViewAllSubscriptions", reflect.TypeOf((*MockSubscriptionRepoImply)(nil).ViewAllSubscriptions), arg0, arg1, arg2)
}
