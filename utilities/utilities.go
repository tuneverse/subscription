package utilities

import (
	"fmt"
	"strconv"
	"strings"
	"subscription/internal/consts"
	"subscription/internal/entities"
)

// PreparePlaceholders generates unique placeholders for the given number of dates.
func PreparePlaceholders(n int) string {
	var b strings.Builder
	for i := 0; i < n; i++ {
		_, err := b.WriteString(fmt.Sprintf("$%d,", i+1))
		if err != nil {
			return ""
		}
	}
	return strings.TrimSuffix(b.String(), ",")
}

// CalculateOffset builds a pagination query offset based on the given page and limit.
func CalculateOffset(page, limit int) string {

	if page <= 0 {
		page = consts.DefaultPage
	}
	if limit <= 0 || limit > consts.LimitVal {
		limit = consts.DefaultLimit
	}
	offset := fmt.Sprintf("%s %d %s %d", "LIMIT", limit, "OFFSET", (page-1)*limit)
	return offset
}

// IsEmpty checks whether the given string is empty
func IsEmpty(str string) bool {

	return strings.TrimSpace(str) == ""
}

// StringToInt64 converts a string to int64.
func StringToInt64(input string) (int64, error) {
	num, err := strconv.ParseInt(input, consts.InputOne, consts.InputTwo)
	if err != nil {
		return 0, err
	}
	return num, nil
}

// SetPaginationValues sets valid pagination values for the given page and limit.
func SetPaginationValues(page, limit int32) (int32, int32) {
	if page <= 0 {
		page = consts.DefaultPage
	}
	if limit <= 0 || limit > consts.MaxAllowedLimit {
		limit = consts.DefaultLimit
	}
	return page, limit
}

// MetaDataInfo calculates values for pagination.
func MetaDataInfo(metaData entities.MetaData) entities.MetaData {
	if int64(metaData.CurrentPage)*int64(metaData.PerPage) < metaData.Total {
		metaData.Next = metaData.CurrentPage + 1
	}
	if metaData.CurrentPage > 1 {
		metaData.Prev = metaData.CurrentPage - 1
	}
	return metaData
}

// CalculateIntervalValue calculates the total number of days based on interval count and unit.
func CalculateIntervalValue(intervalCount int, intervalUnit string) (int, error) {
	switch intervalUnit {
	case consts.IntervalUnitDay:
		return intervalCount, nil
	case consts.IntervalUnitWeek:
		return intervalCount * consts.DaysInWeek, nil
	case consts.IntervalUnitMonth:
		return intervalCount * consts.DaysInMonth, nil
	case consts.IntervalUnitYear:
		return intervalCount * consts.DaysInYear, nil
	default:
		return 0, fmt.Errorf("invalid interval unit")
	}
}

// IsNotEmpty checks whether the given string is not empty.
func IsNotEmpty(str string) bool {
	return strings.TrimSpace(str) != ""
}

// ValidateIntervalUnit validates the given interval unit.
func ValidateIntervalUnit(intervalUnit string) error {
	switch intervalUnit {
	case consts.IntervalUnitDay, consts.IntervalUnitWeek, consts.IntervalUnitMonth, consts.IntervalUnitYear:
		return nil
	default:
		return fmt.Errorf("interval unit must be day, week, month or year")
	}
}
