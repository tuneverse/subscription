package app

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"subscription/config"
	"subscription/internal/consts"
	"subscription/internal/controllers"
	"subscription/internal/entities"

	"subscription/internal/repo"
	"subscription/internal/repo/driver"
	"subscription/internal/usecases"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/patrickmn/go-cache"
	"gitlab.com/tuneverse/toolkit/core/logger"
	"gitlab.com/tuneverse/toolkit/middleware"
)

// method run
// env configuration
// logrus, zap
// use case intia
// repo initalization
// controller init
var log *logger.Logger

// Run is the main function to initialize and run the application.
func Run() {
	// init the env config
	cfg, err := config.LoadConfig(consts.AppName)
	if err != nil {
		panic(err)
	}

	file := &logger.FileMode{
		LogfileName:  consts.AppName + ".log", // Log file name.
		LogMaxAge:    consts.LogMaxAge,        // Maximum log file age in days.
		LogMaxSize:   consts.LogMaxSize,       // Maximum log file size (10 MB).
		LogMaxBackup: consts.LogMaxBackup,     // Maximum number of log file backups to keep.
	}

	clientOpt := &logger.ClientOptions{
		Service:             consts.AppName, // Service name.
		LogLevel:            "info",         // Log level.
		IncludeRequestDump:  false,          // Include request data in logs.
		IncludeResponseDump: false,          // Include response data in logs.
	}

	if cfg.Debug {
		log = logger.InitLogger(clientOpt, file)
	} else {
		db := &logger.CloudMode{

			URL:    cfg.LoggerServiceURL,
			Secret: cfg.LoggerSecret,
		}
		log = logger.InitLogger(clientOpt, db, file)
	}
	// database connection
	pgsqlDB, err := driver.ConnectDB(cfg.Db)
	if err != nil {
		log.Fatalf("unable to connect the database:error=%s", err.Error())
		//log.Fatalf("unable to connect the database")
		return
	}

	// here initalizing the router
	router := initRouter()
	if !cfg.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	api := router.Group("/api")
	api.Use(middleware.LogMiddleware(map[string]interface{}{}))
	api.Use(middleware.APIVersionGuard(middleware.VersionOptions{
		AcceptedVersions: cfg.AcceptedVersions,
	}))
	api.Use(middleware.Localize())
	api.Use(middleware.ErrorLocalization(
		middleware.ErrorLocaleOptions{
			Cache:                  cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:        time.Duration(time.Hour * 24),
			CacheKeyLabel:          consts.CacheErrorKey,
			LocalisationServiceURL: fmt.Sprintf("%s/localization/error", cfg.LocalisationServiceURL),
		},
	))
	api.Use(middleware.EndpointExtraction(
		middleware.EndPointOptions{
			Cache:            cache.New(5*time.Minute, 10*time.Minute),
			CacheExpiration:  time.Duration(time.Hour * 24),
			CacheKeyLabel:    consts.CacheEndpointsKey,
			ContextEndPoints: consts.ContextEndPoints,
			EndPointsURL:     fmt.Sprintf("%s/localization/endpointname", cfg.EndpointURL),
		},
	))
	// complete user related initialization
	{

		// repo initialization
		subscriptionRepo := repo.NewSubscriptionRepo(pgsqlDB)

		// initilizing usecases
		subscriptionUseCases := usecases.NewSubscriptionUseCases(subscriptionRepo)

		// initalizing controllers
		subscriptionController := controllers.NewSubscriptionController(api, subscriptionUseCases)
		// init the routes
		subscriptionController.InitRoutes()
	}

	// run the app
	launch(cfg, router)
}

func initRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.DebugMode)

	// CORS
	// - PUT and PATCH methods
	// - Origin header
	// - Credentials share
	// - Preflight requests cached for 12 hours
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "DELETE", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		// AllowOriginFunc: func(origin string) bool {
		// },
		MaxAge: 12 * time.Hour,
	}))

	// common middlewares should be added here

	return router
}

// launch
func launch(cfg *entities.EnvConfig, router *gin.Engine) {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%v", cfg.Port),
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	_, err := fmt.Println("Server listening in...", cfg.Port)
	if err != nil {
		return
	}
	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal, 1)
	// kill (no param) default send syscanll.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall. SIGKILL but can"t be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Printf("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("Server Shutdown:%s", err.Error())
	}
	// catching ctx.Done(). timeout of 5 seconds.

	<-ctx.Done()
	log.Printf("timeout of 5 seconds.")

	log.Printf("Server exiting")
}
